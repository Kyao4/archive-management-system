package dao.user;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.MD5;
import dao.MySQLDAOFactory;
import pojo.User;

public class MySQLUserDAO implements UserDAO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Override
	public User getUserByName(String user_name) {
		User user = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_user where user_name like ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, user_name);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				user = new User();
				user.setUser_id(rs.getInt(1));
				user.setUser_name(rs.getString(2));
				user.setUser_password(rs.getString(3));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return user;
	}

	//@Override
	public boolean updatePasswordByName(String user_name, String user_password) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "update hust_user set user_password = ? where user_name like ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, MD5.GetMD5Code(user_password));
			stmt.setString(2, user_name);
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return flag;
	}

	//@Override
	public User getUserByNameAndPassword(String user_name, String user_password) {
		User user = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_user where user_name = ? and user_password = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, user_name);
			stmt.setString(2, MD5.GetMD5Code(user_password));
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				user = new User();
				user.setUser_id(rs.getInt(1));
				user.setUser_name(rs.getString(2));
				user.setUser_password(rs.getString(3));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return user;
	}

}
