package dao.user;

import pojo.User;

public interface UserDAO {

	
	public User getUserByName(String user_name);
	
	public User getUserByNameAndPassword(String user_name, String user_password);
	
	public boolean updatePasswordByName(String user_name, String user_password);

}
