package dao.archive_inner_box;

import java.util.List;

import pojo.Archive_inner_box;

public interface Archive_inner_boxDAO {

	
	public boolean createInnerBox(Archive_inner_box box);
	
	public Archive_inner_box getInnerBoxInfoByAb_idAib_num(int ab_id, int aib_num);

	public boolean deleteAllArchive_inner_box();
	
	public List<Integer> getAib_idByAb_yearAb_semesterAb_enrollAib_dept(String ab_year, 
			String ab_semester, String ab_enroll, String aib_dept);
	
	public Archive_inner_box getInnerBoxInfoByAib_id(int aib_id);
	
}
