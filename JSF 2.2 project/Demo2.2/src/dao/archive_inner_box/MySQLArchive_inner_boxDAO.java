package dao.archive_inner_box;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.MySQLDAOFactory;
import pojo.Archive_inner_box;

public class MySQLArchive_inner_boxDAO implements Archive_inner_boxDAO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Override
	public boolean createInnerBox(Archive_inner_box box) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "insert into hust_archive_inner_box values(?,?,?,?,?,?)";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, box.getAib_id());
			stmt.setInt(2, box.getAib_num());
			stmt.setInt(3, box.getAib_length());
			stmt.setString(4, box.getAib_dept());
			stmt.setString(5, box.getAib_side());
			stmt.setInt(6, box.getAib_ab_id());
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return flag;
	}

	
	//@Override
	public Archive_inner_box getInnerBoxInfoByAb_idAib_num(int ab_id,
			int aib_num) {
		Archive_inner_box innerBox = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_archive_inner_box where aib_ab_id = ? and aib_num = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, ab_id);
			stmt.setInt(2, aib_num);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				innerBox = new Archive_inner_box();
				innerBox.setAib_id(rs.getInt(1));
				innerBox.setAib_num(rs.getInt(2));
				innerBox.setAib_length(rs.getInt(3));
				innerBox.setAib_dept(rs.getString(4));
				innerBox.setAib_side(rs.getString(5));
				innerBox.setAib_ab_id(rs.getInt(6));
			}
		
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return innerBox;
	}

	//@Override
	public boolean deleteAllArchive_inner_box() {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "truncate table hust_archive_inner_box";
			stmt = conn.prepareStatement(sql);
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return flag;
	}

	//@Override
	public List<Integer> getAib_idByAb_yearAb_semesterAb_enrollAib_dept(
			String ab_year, String ab_semester, String ab_enroll,
			String aib_dept) {
		List<Integer> aib_idList = new ArrayList<Integer>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select aib_id from hust_archive_box, hust_archive_inner_box where "
					+ "hust_archive_box.ab_id = hust_archive_inner_box.aib_ab_id and "
					+ "ab_year = ? and ab_semester = ? and ab_enroll = ? and aib_dept = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, ab_year);
			stmt.setString(2, ab_semester);
			stmt.setString(3, ab_enroll);
			stmt.setString(4, aib_dept);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				int aib_id = rs.getInt(1);
				aib_idList.add(aib_id);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return aib_idList;
	}

	//@Override
	public Archive_inner_box getInnerBoxInfoByAib_id(int aib_id) {
		Archive_inner_box aib = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_archive_inner_box where aib_id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, aib_id);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				aib = new Archive_inner_box();
				aib.setAib_id(rs.getInt(1));
				aib.setAib_num(rs.getInt(2));
				aib.setAib_length(rs.getInt(3));
				aib.setAib_dept(rs.getString(4));
				aib.setAib_side(rs.getString(5));
				aib.setAib_ab_id(rs.getInt(6));
			}
		
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return aib;
	}

}
