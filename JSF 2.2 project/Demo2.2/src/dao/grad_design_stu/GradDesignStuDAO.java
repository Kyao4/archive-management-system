package dao.grad_design_stu;

import java.util.List;

import pojo.Gds_year_major;
import pojo.GradDesignStatistics;
import pojo.Grad_design_stu;

public interface GradDesignStuDAO {

	public boolean createStu(Grad_design_stu stu);
	
	public List<Gds_year_major> getGds_year_marjorList();
	
	public GradDesignStatistics getGradDesignStatisticsByYearMajor(String gds_year, String gds_major);

	public boolean deleteGradDesignByYearSemester(String year, String semester);
	
	public List<Gds_year_major> getGds_Year();
	
	public List<String> getAllMajor();
	
	public List<String> getAllYear();
}
