package dao.transcript;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.MySQLDAOFactory;
import pojo.Transcript;

public class MySQLTranscriptDAO implements TranscriptDAO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Override
	public boolean createTrancript(Transcript transcript) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "insert into hust_transcript(transcript_file,transcript_type,transcript_archive_id) values(?,?,?)";
			stmt = conn.prepareStatement(sql);
			stmt.setBlob(1, new ByteArrayInputStream(transcript.getTranscript_file()));
			stmt.setString(2, transcript.getTranscript_type());
			stmt.setInt(3, transcript.getTranscript_archive_id());
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		
		return flag;
	}

	//@Override
	public boolean deleteTranscriptById(int archive_id) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "delete from hust_transcript where transcript_archive_id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, archive_id);
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return flag;
	}

	//@Override
	public List<Transcript> getTranscriptByArchive_id(int archive_id) {
		List<Transcript> transcriptList = new ArrayList<Transcript>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_transcript "
					+ "where transcript_archive_id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, archive_id);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Transcript transcript = new Transcript();
				transcript.setTranscript_id(rs.getInt(1));
				transcript.setTranscript_file(rs.getBlob(2).getBytes(1, (int)rs.getBlob(2).length()));
				transcript.setTranscript_type(rs.getString(3));
				transcript.setTranscript_archive_id(rs.getInt(4));
				transcriptList.add(transcript);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		
		
		return transcriptList;
	}

}
