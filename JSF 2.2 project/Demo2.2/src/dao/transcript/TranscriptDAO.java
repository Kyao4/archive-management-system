package dao.transcript;

import java.util.List;

import pojo.Transcript;

public interface TranscriptDAO {

	public boolean createTrancript(Transcript transcript);
	
	public boolean deleteTranscriptById(int archive_id);
	
	public List<Transcript> getTranscriptByArchive_id(int archive_id);

}
