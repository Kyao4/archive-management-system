package dao.archive_box;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import pojo.Archive_box;
import dao.MySQLDAOFactory;

public class MySQLArchive_boxDAO implements Archive_boxDAO, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Override
	public boolean updateArchive_box(Archive_box box) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "update hust_archive_box set ab_year = ?, "
					+ "ab_semester = ?, "
					+ "ab_enroll = ? "
					+ "where ab_id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, box.getAb_year());
			stmt.setString(2, box.getAb_semester());
			stmt.setString(3, box.getAb_enroll());
			stmt.setInt(4, box.getAb_id());
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return flag;
	}

	//@Override
	public List<Archive_box> getAllBox() {
		List<Archive_box> boxList = new ArrayList<Archive_box>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_archive_box";
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Archive_box box = new Archive_box();
				box.setAb_id(rs.getInt(1));
				box.setAb_num(rs.getInt(2));
				box.setAb_side(rs.getString(3));
				box.setAb_year(rs.getString(4));
				box.setAb_semester(rs.getString(5));
				box.setAb_enroll(rs.getString(6));
				boxList.add(box);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return boxList;
	}

	//@Override
	public Archive_box getArchive_boxByAb_id(int ab_id) {
		Archive_box box = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_archive_box where "
					+ "ab_id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, ab_id);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				box = new Archive_box();
				box.setAb_id(rs.getInt(1));
				box.setAb_num(rs.getInt(2));
				box.setAb_side(rs.getString(3));
				box.setAb_year(rs.getString(4));
				box.setAb_semester(rs.getString(5));
				box.setAb_enroll(rs.getString(6));
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return box;
	}

	//@Override
	public Archive_box getArchive_boxByAb_numAb_side(int ab_num, String ab_side) {
		Archive_box box = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_archive_box where ab_num = ? and ab_side = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, ab_num);
			stmt.setString(2, ab_side);
			ResultSet rs  = stmt.executeQuery();
			while(rs.next()) {
				box = new Archive_box();
				box.setAb_id(rs.getInt(1));
				box.setAb_num(rs.getInt(2));
				box.setAb_side(rs.getString(3));
				box.setAb_year(rs.getString(4));
				box.setAb_semester(rs.getString(5));
				box.setAb_enroll(rs.getString(6));
				
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return box;
	}

	//@Override
	public int getAb_idByYearSemesterEnroll(String year, String semester, String enroll) {
		int result = -1;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "SELECT ab_id FROM hust_archive_box WHERE ab_year = ? AND ab_semester= ? AND ab_enroll = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, year);
			stmt.setString(2, semester);
			stmt.setString(3, enroll);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				result = rs.getInt(1);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		
		return result;
	}

}
