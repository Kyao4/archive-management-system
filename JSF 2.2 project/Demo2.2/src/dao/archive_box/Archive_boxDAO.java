package dao.archive_box;

import java.util.List;

import pojo.Archive_box;

public interface Archive_boxDAO {

	
	public boolean updateArchive_box(Archive_box box);
	
	public List<Archive_box> getAllBox();
	
	public Archive_box getArchive_boxByAb_id(int ab_id);
	
	public Archive_box getArchive_boxByAb_numAb_side(int ab_num, String ab_side);
	
	public int getAb_idByYearSemesterEnroll(String year, String semester, String enroll);
}
