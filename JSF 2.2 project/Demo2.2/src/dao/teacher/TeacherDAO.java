package dao.teacher;

import java.util.List;

import pojo.Teacher;

public interface TeacherDAO {
	
	public Teacher getTeacherById(String teacher_id);
	
	public Teacher getTeacherByIdAndPassword(String teacher_id, String teacher_password);
	
	public boolean updatePasswordById(String teacher_id, String newPassword);
	
	public boolean createTeacher(Teacher teacher);
	
	public String getTeacher_idByName(String teacher_name);
	
	public boolean deleteTeacherById(String teacher_id);
	
	public List<Teacher> getAllTeacher();
	
	public boolean deleteAllTeacher();
	
	public List<String> getAllDept();
}
