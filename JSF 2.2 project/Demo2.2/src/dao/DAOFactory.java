package dao;

import dao.archive.ArchiveDAO;
import dao.archive_box.Archive_boxDAO;
import dao.archive_inner_box.Archive_inner_boxDAO;
import dao.course.CourseDAO;
import dao.grad_design_stu.GradDesignStuDAO;
import dao.regular_grade.RegularGradeDAO;
import dao.secretary.SecretaryDAO;
import dao.student.StudentDAO;
import dao.teacher.TeacherDAO;
import dao.transcript.TranscriptDAO;
import dao.user.UserDAO;



public abstract class DAOFactory {
	public static final int MYSQL = 1;
	public static final int ORACLE = 2;
	public static final int SQLSERVER = 3;
	
	public abstract TeacherDAO getTeacherDAO();
	
	public abstract UserDAO getUserDAO();
	
	public abstract SecretaryDAO getSecretaryDAO();
	
	public abstract CourseDAO getCourseDAO();
	
	public abstract ArchiveDAO getArchiveDAO();
	
	public abstract StudentDAO getStudentDAO();

	public abstract TranscriptDAO getTranscriptDAO();
	
	public abstract Archive_boxDAO getArchive_boxDAO();

	public abstract Archive_inner_boxDAO getArchive_inner_boxDAO();
	
	public abstract RegularGradeDAO getRegularGradeDAO();
	
	public abstract GradDesignStuDAO getGradDesignStuDAO();
	
	public static DAOFactory getDAOFactory(int dbType) {
		switch(dbType) {
			case MYSQL: return new MySQLDAOFactory();
			default: return null;
		}
	}
}
