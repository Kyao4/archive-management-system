package dao.secretary;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.MD5;
import dao.MySQLDAOFactory;
import pojo.Secretary;

public class MySQLSecretaryDAO implements SecretaryDAO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Override
	public Secretary getSecretaryByName(String secretary_name) {
		Secretary secretary = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_secretary where secretary_name like ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, secretary_name);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				secretary = new Secretary();
				secretary.setSecretary_name(rs.getString(2));
				secretary.setSecretary_password(rs.getString(3));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return secretary;
	}

	//@Override
	public boolean updatePasswordByName(String secretary_name,
			String secretary_password) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "update hust_secretary set secretary_password = ? where secretary_name = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, MD5.GetMD5Code(secretary_password));
			stmt.setString(2, secretary_name);
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return flag;
	}

	//@Override
	public Secretary getSecretaryByNameAndPassword(String secretary_name,
			String secretary_password) {
		Secretary secretary = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_secretary where secretary_name = ? and secretary_password = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, secretary_name);
			stmt.setString(2, MD5.GetMD5Code(secretary_password));
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				secretary = new Secretary();
				secretary.setSecretary_name(rs.getString(2));
				secretary.setSecretary_password(rs.getString(3));
			}
		
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return secretary;
	}

}
