package dao.secretary;

import pojo.Secretary;

public interface SecretaryDAO {
	
	public Secretary getSecretaryByName(String secretary_name);
	
	public boolean updatePasswordByName(String secretary_name, String secretary_password);

	public Secretary getSecretaryByNameAndPassword(String secretary_name, String secretary_password);
	
}
