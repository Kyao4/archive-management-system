package dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
























import dao.archive.ArchiveDAO;
import dao.archive.MySQLArchiveDAO;
import dao.archive_box.Archive_boxDAO;
import dao.archive_box.MySQLArchive_boxDAO;
import dao.archive_inner_box.Archive_inner_boxDAO;
import dao.archive_inner_box.MySQLArchive_inner_boxDAO;
import dao.course.CourseDAO;
import dao.course.MySQLCourseDAO;
import dao.grad_design_stu.GradDesignStuDAO;
import dao.grad_design_stu.MySQLGradDesignStuDAO;
import dao.regular_grade.MySQLRegularGradeDAO;
import dao.regular_grade.RegularGradeDAO;
import dao.secretary.MySQLSecretaryDAO;
import dao.secretary.SecretaryDAO;
import dao.student.MySQLStudentDAO;
import dao.student.StudentDAO;
import dao.teacher.MySQLTeacherDAO;
import dao.teacher.TeacherDAO;
import dao.transcript.MySQLTranscriptDAO;
import dao.transcript.TranscriptDAO;
import dao.user.MySQLUserDAO;
import dao.user.UserDAO;


public class MySQLDAOFactory extends DAOFactory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String DBDRIVER = "com.mysql.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/hustarchive";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "admin";
	
	public static Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName(DBDRIVER);
			conn = DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	
	//@Override
	public TeacherDAO getTeacherDAO() {
		return new MySQLTeacherDAO();
	}
	


	public static void closeResutlSet(ResultSet rs) {
		if(rs != null) {
			try {
				rs.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static void closeStatement(PreparedStatement stmt) {
		if(stmt != null) {
			try {
				stmt.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static void closeConnection(Connection conn) {
		if(conn != null) {
			try {
				conn.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}


	//@Override
	public UserDAO getUserDAO() {
		return new MySQLUserDAO();
	}


	//@Override
	public SecretaryDAO getSecretaryDAO() {
		return new MySQLSecretaryDAO();
	}


	//@Override
	public CourseDAO getCourseDAO() {
		return new MySQLCourseDAO();
	}


	//@Override
	public ArchiveDAO getArchiveDAO() {
		return new MySQLArchiveDAO();
	}


	//@Override
	public StudentDAO getStudentDAO() {
		return new MySQLStudentDAO();
	}


	//@Override
	public TranscriptDAO getTranscriptDAO() {
		return new MySQLTranscriptDAO();
	}


	//@Override
	public RegularGradeDAO getRegularGradeDAO() {
		return new MySQLRegularGradeDAO();
	}


	//@Override
	public Archive_boxDAO getArchive_boxDAO() {
		return new MySQLArchive_boxDAO();
	}


	//@Override
	public Archive_inner_boxDAO getArchive_inner_boxDAO() {
		return new MySQLArchive_inner_boxDAO();
	}


	//@Override
	public GradDesignStuDAO getGradDesignStuDAO() {
		return new MySQLGradDesignStuDAO();
	}


}
