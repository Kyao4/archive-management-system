package dao.student;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.MySQLDAOFactory;
import pojo.Archive;
import pojo.Student;
import pojo.Student_info;

public class MySQLStudentDAO implements StudentDAO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Override
	public boolean createStudent(Student stu) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "insert into hust_student values(?,?,?,?,?)";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, stu.getStu_id());
			stmt.setString(2, stu.getStu_class());
			stmt.setString(3, stu.getStu_name());
			stmt.setDouble(4, stu.getStu_grade());
			stmt.setInt(5, stu.getStu_archive_id());
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	//@Override
	public List<Student_info> getALlStudentGradeInfoByArchive_yearArchive_semester(Archive archive) {
		List<Student_info> stuInfoList = new ArrayList<Student_info>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "SELECT archive_year, archive_semester, "
						+ "archive_course_name, archive_teacher, stu_name, stu_id, stu_class, stu_grade " 
						+ "from hust_student LEFT JOIN hust_archive on hust_student.stu_archive_id = hust_archive.archive_id "
						+ "WHERE archive_year = ? and archive_semester = ? "
						+ "order by archive_year, archive_semester, archive_course_name";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, archive.getArchive_year());
			stmt.setString(2, archive.getArchive_semester());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Student_info stu_info = new Student_info();
				stu_info.setArchive_year(rs.getString(1));
				stu_info.setArchive_semester(rs.getString(2));
				stu_info.setArchive_course_name(rs.getString(3));
				stu_info.setArchive_teacher(rs.getString(4));
				stu_info.setStu_name(rs.getString(5));
				stu_info.setStu_id(rs.getString(6));
				stu_info.setStu_class(rs.getString(7));
				stu_info.setStu_grade(rs.getDouble(8));
				stuInfoList.add(stu_info);
			} 
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return stuInfoList;
	}

	//@Override
	public List<Student> getStudentByArchive_id(int archive_id) {
		List<Student> stuList = new ArrayList<Student>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_student where stu_archive_id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, archive_id);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Student stu = new Student();
				stu.setStu_id(rs.getString(1));
				stu.setStu_class(rs.getString(2));
				stu.setStu_name(rs.getString(3));
				stu.setStu_grade(rs.getDouble(4));
				stu.setStu_archive_id(rs.getInt(5));
				stuList.add(stu);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return stuList;
	}

	//@Override
	public boolean deleteStudentByStudent_idAndArchive_id(Student stu) {
		boolean flag = true;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "delete from hust_student where stu_id = ? "
					+ "and stu_archive_id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, stu.getStu_id());
			stmt.setInt(2, stu.getStu_archive_id());
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

}
