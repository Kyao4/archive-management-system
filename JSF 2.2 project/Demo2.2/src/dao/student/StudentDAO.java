package dao.student;

import java.util.List;

import pojo.Archive;
import pojo.Student;
import pojo.Student_info;

public interface StudentDAO {

	
	public boolean createStudent(Student stu);
	
	public List<Student_info> getALlStudentGradeInfoByArchive_yearArchive_semester(Archive archive);
	
	public List<Student> getStudentByArchive_id(int archive_id);
	
	public boolean deleteStudentByStudent_idAndArchive_id(Student stu);
	
}
