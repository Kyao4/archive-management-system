package dao.regular_grade;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.DAOFactory;
import dao.MySQLDAOFactory;
import pojo.RegularGrade;

public class MySQLRegularGradeDAO implements RegularGradeDAO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Override
	public boolean createRegularGrade(RegularGrade rg) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "insert into hust_regular_grade(rg_file, rg_type, rg_archive_id) values(?,?,?)";
			stmt = conn.prepareStatement(sql);
			stmt.setBlob(1, new ByteArrayInputStream(rg.getRg_file()));
			stmt.setString(2, rg.getRg_type());
			stmt.setInt(3, rg.getRg_archive_id());
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		
		return flag;
	}

	//@Override
	public boolean deleteRegularGradeByArchive_id(int archive_id) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "delete from hust_regular_grade where rg_archive_id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, archive_id);
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return flag;
	}

	//@Override
	public List<RegularGrade> getRegularGradeByArchive_id(int archive_id) {
		List<RegularGrade> rgList = new ArrayList<RegularGrade>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_regular_grade "
					+ "where rg_archive_id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, archive_id);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				RegularGrade rg = new RegularGrade();
				rg.setRg_id(rs.getInt(1));
				rg.setRg_file(rs.getBlob(1).getBytes(1, (int)rs.getBlob(1).length()));
				rg.setRg_type(rs.getString(3));
				rg.setRg_archive_id(rs.getInt(4));
				rgList.add(rg);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return rgList;
	}

	

}
