package dao.regular_grade;

import java.util.List;

import pojo.RegularGrade;

public interface RegularGradeDAO {

	
	public boolean createRegularGrade(RegularGrade rg);
	
	public boolean deleteRegularGradeByArchive_id(int archive_id);

	public List<RegularGrade> getRegularGradeByArchive_id(int archive_id);
	
	
}
