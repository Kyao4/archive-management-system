package dao.course;

import java.util.List;

import pojo.Course;
import pojo.Course_info;

public interface CourseDAO {
	
	//管理员用操作
	public boolean createCourse(Course course);
	//教师用(给档案找出对应的课程)如果没找到返回-1
	public Course getCourse(Course course);
	
	//教学秘书用（统计报表）
	public List<Course_info> getAllCourseWithStatusByCourse_yearCourse_semster(Course course);
	//删除课程
	public boolean deleteCourse(Course course);
	
	public List<String> getYearList();
	
	
	public boolean deleteAllCourse();
	
	public List<Course>  getCourse_yearCourse_semester();
	
	public List<Course>  getAllCourseNameClassTeacherYearSemesterByYearSemester(String year, String semester);

	public boolean deleteByYearSemester(String year, String semester);
}
