package dao.course;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Constants;
import dao.MySQLDAOFactory;
import pojo.Course;
import pojo.Course_info;

public class MySQLCourseDAO implements CourseDAO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Override
	public boolean createCourse(Course course) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "insert into hust_course values(?,?,?,?,?,?,?,?,?,?,?,?)";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, course.getCourse_id());
			stmt.setString(2, course.getCourse_id_id());
			stmt.setString(3, course.getCourse_num());
			stmt.setString(4, course.getCourse_name());
			stmt.setString(5, course.getCourse_teacher());
			stmt.setString(6, course.getCourse_type());
			stmt.setString(7, course.getCourse_class());
			stmt.setString(8, course.getCourse_enroll());
			stmt.setString(9, course.getCourse_dept());
			stmt.setString(10, course.getCourse_year());
			stmt.setString(11, course.getCourse_semester());
			stmt.setString(12, course.getCourse_teacher_id());
			
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		
		return flag;
	}

	//@Override
	public Course getCourse(Course currentCourse) {
		Course course = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select * from hust_course where course_year = ? "
					+ "and course_semester = ? "
					+ "and course_id_id = ? "
					+ "and course_num = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, currentCourse.getCourse_year());
			stmt.setString(2, currentCourse.getCourse_semester());
			stmt.setString(3, currentCourse.getCourse_id_id());
			stmt.setString(4, currentCourse.getCourse_num());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				course = new Course();
				course.setCourse_id(rs.getInt(1));
				course.setCourse_id_id(rs.getString(2));
				course.setCourse_num(rs.getString(3));
				course.setCourse_name(rs.getString(4));
				course.setCourse_teacher(rs.getString(5));
				course.setCourse_type(rs.getString(6));
				course.setCourse_class(rs.getString(7));
				course.setCourse_enroll(rs.getString(8));
				course.setCourse_dept(rs.getString(9));
				course.setCourse_year(rs.getString(10));
				course.setCourse_semester(rs.getString(11));
				course.setCourse_teacher_id(rs.getString(12));
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return course;
	}

//	//@Override
//	public List<Course> getAllCourse() {
//		List<Course> courseList = new ArrayList<Course>();
//		Connection conn = null;
//		PreparedStatement stmt = null;
//		try {
//			conn = MySQLDAOFactory.getConnection();
//			String sql = "select * from hust_course";
//			stmt = conn.prepareStatement(sql);
//			ResultSet rs = stmt.executeQuery();
//			while(rs.next()) {
//				Course course = new Course();
//				course.setCourse_id(rs.getInt(1));
//				course.setCourse_id_id(rs.getString(2));
//				course.setCourse_num(rs.getString(3));
//				course.setCourse_name(rs.getString(4));
//				course.setCourse_teacher(rs.getString(5));
//				course.setCourse_type(rs.getString(6));
//				course.setCourse_class(rs.getString(7));
//				course.setCourse_enroll(rs.getString(8));
//				course.setCourse_dept(rs.getString(9));
//				course.setCourse_year(rs.getString(10));
//				course.setCourse_semester(rs.getString(11));
//				course.setCourse_teacher_id(rs.getString(12));
//				courseList.add(course);
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//		return courseList;
//	}


	//@Override
	public List<Course_info> getAllCourseWithStatusByCourse_yearCourse_semster(Course course) {
		List<Course_info> courseList = new ArrayList<Course_info>();
		Connection conn = null;
		PreparedStatement stmt= null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "SELECT course_year, course_semester, course_id_id, course_num, course_name, " +
					"course_class, course_teacher, course_dept, COALESCE(archive_status,0), COALESCE(archive_borrower_status,0)" +
					"FROM hust_course LEFT JOIN hust_archive ON hust_course.course_id = hust_archive.archive_course_id "
					+ "where course_year = ? and course_semester = ? "
					+ "order by course_year, course_semester, course_dept, course_id_id, course_num";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, course.getCourse_year());
			stmt.setString(2, course.getCourse_semester());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Course_info course_info = new Course_info();
				course_info.setCourse_year(rs.getString(1));
				course_info.setCourse_semester(rs.getString(2));
				course_info.setCourse_id_id(rs.getString(3));
				course_info.setCourse_num(rs.getString(4));
				course_info.setCourse_name(rs.getString(5));
				course_info.setCourse_class(rs.getString(6));
				course_info.setCourse_teacher(rs.getString(7));
				course_info.setCourse_dept(rs.getString(8));
				switch(rs.getInt(9)) {
					case 0:course_info.setArchive_status(Constants.UNSUBMITTED_REQUEST_STR);break;
					case 1:course_info.setArchive_status(Constants.PENDING_REQUEST_STR);break;
					case 2:course_info.setArchive_status(Constants.REJECTED_REQUEST_STR);break;
					case 3:course_info.setArchive_status(Constants.APPROVED_REQUEST_STR);break;
					default: course_info.setArchive_status(Constants.UNSUBMITTED_REQUEST_STR);break;
				}
				switch(rs.getInt(10)) {
					case 0:course_info.setArchive_borrower_status(Constants.NOTLENDOUT_STR);break;
					case 1:course_info.setArchive_borrower_status(Constants.LENDOUT_STR);break;
				}
				courseList.add(course_info);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return courseList; 
	}

	//@Override
	public boolean deleteCourse(Course course) {
		// TODO Auto-generated method stub
		return false;
	}

	//@Override
	public List<String> getYearList() {
		List<String> yearList = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select course_year "
					+ "from hust_course "
					+ "GROUP BY course_year";
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				String year = rs.getString(1);
				yearList.add(year);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return yearList;
	}

	//@Override
	public boolean deleteAllCourse() {
		boolean flag= false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "truncate table hust_course";
			stmt = conn.prepareStatement(sql);
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return flag;
	}

	//@Override
	public List<Course> getCourse_yearCourse_semester() {
		List<Course> courseList = new ArrayList<Course>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "select course_year, course_semester from hust_course GROUP BY course_year, course_semester";
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Course course = new Course();
				course.setCourse_year(rs.getString(1));
				course.setCourse_semester(rs.getString(2));
				courseList.add(course);
			}
		
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return courseList;
	}

	//@Override
	public List<Course> getAllCourseNameClassTeacherYearSemesterByYearSemester(String year, String semester) {
		List<Course> courseList = new ArrayList<Course>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "SELECT course_id, "
					+ "course_id_id, "
					+ "course_num, "
					+ "course_name, "
					+ "course_class, "
					+ "course_teacher, " 
					+ "course_year, "
					+ "course_semester "
					+ "FROM hust_course "
					+ "WHERE course_year = ? AND "
					+ "course_semester = ? ";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, year);
			stmt.setString(2, semester);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Course course = new Course();
				course.setCourse_id(rs.getInt(1));
				course.setCourse_id_id(rs.getString(2));
				course.setCourse_num(rs.getString(3));
				course.setCourse_name(rs.getString(4));
				course.setCourse_class(rs.getString(5));
				course.setCourse_teacher(rs.getString(6));
				course.setCourse_year(rs.getString(7));
				course.setCourse_semester(rs.getString(8));
				courseList.add(course);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return courseList;
	}
//@Override
	public boolean deleteByYearSemester(String year, String semester) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "DELETE FROM hust_course WHERE course_year = ? AND "
					+ "course_semester = ? ";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, year);
			stmt.setString(2, semester);
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return flag;
	}

	

}
