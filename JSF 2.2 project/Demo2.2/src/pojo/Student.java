package pojo;

import java.io.Serializable;

public class Student implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String stu_id;
	private String stu_class;
	private String stu_name;
	private double stu_grade;
	private	int stu_archive_id;
	public String getStu_id() {
		return stu_id;
	}
	public void setStu_id(String stu_id) {
		this.stu_id = stu_id;
	}
	public String getStu_class() {
		return stu_class;
	}
	public void setStu_class(String stu_class) {
		this.stu_class = stu_class;
	}
	public String getStu_name() {
		return stu_name;
	}
	public void setStu_name(String stu_name) {
		this.stu_name = stu_name;
	}
	public double getStu_grade() {
		return stu_grade;
	}
	public void setStu_grade(double stu_grade) {
		this.stu_grade = stu_grade;
	}
	public int getStu_archive_id() {
		return stu_archive_id;
	}
	public void setStu_archive_id(int stu_archive_id) {
		this.stu_archive_id = stu_archive_id;
	}
	
	
	
}
