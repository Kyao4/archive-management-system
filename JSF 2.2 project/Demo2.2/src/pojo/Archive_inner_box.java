package pojo;

import java.util.ArrayList;
import java.util.List;

public class Archive_inner_box {

	private int ab_id;
	private int ab_num;
	private String 	ab_side;
	private String ab_year;
	private String ab_semester;
	private String ab_enroll;
	
	
	private int aib_id;
	private int aib_num;
	private int aib_length;
	private String aib_dept;
	private String aib_side;
	private int aib_ab_id;
	
	private List<Archive> archiveList = new ArrayList<Archive>();
	
	public int getAb_id() {
		return ab_id;
	}
	public void setAb_id(int ab_id) {
		this.ab_id = ab_id;
	}
	public int getAb_num() {
		return ab_num;
	}
	public void setAb_num(int ab_num) {
		this.ab_num = ab_num;
	}
	public String getAb_side() {
		return ab_side;
	}
	public void setAb_side(String ab_side) {
		this.ab_side = ab_side;
	}
	public String getAb_year() {
		return ab_year;
	}
	public void setAb_year(String ab_year) {
		this.ab_year = ab_year;
	}
	public String getAb_semester() {
		return ab_semester;
	}
	public void setAb_semester(String ab_semester) {
		this.ab_semester = ab_semester;
	}
	public String getAb_enroll() {
		return ab_enroll;
	}
	public void setAb_enroll(String ab_enroll) {
		this.ab_enroll = ab_enroll;
	}
	public int getAib_id() {
		return aib_id;
	}
	public void setAib_id(int aib_id) {
		this.aib_id = aib_id;
	}
	public int getAib_num() {
		return aib_num;
	}
	public void setAib_num(int aib_num) {
		this.aib_num = aib_num;
	}
	public int getAib_length() {
		return aib_length;
	}
	public void setAib_length(int aib_length) {
		this.aib_length = aib_length;
	}
	public String getAib_dept() {
		return aib_dept;
	}
	public void setAib_dept(String aib_dept) {
		this.aib_dept = aib_dept;
	}
	public String getAib_side() {
		return aib_side;
	}
	public void setAib_side(String aib_side) {
		this.aib_side = aib_side;
	}
	public int getAib_ab_id() {
		return aib_ab_id;
	}
	public void setAib_ab_id(int aib_ab_id) {
		this.aib_ab_id = aib_ab_id;
	}
	public List<Archive> getArchiveList() {
		return archiveList;
	}
	public void setArchiveList(List<Archive> archiveList) {
		this.archiveList = archiveList;
	}
	
	
}
