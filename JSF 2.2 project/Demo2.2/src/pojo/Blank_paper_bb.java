package pojo;

public class Blank_paper_bb {

	
	private byte[] blank_paper_bb;
	private String blank_paper_bb_type;
	public byte[] getBlank_paper_bb() {
		return blank_paper_bb;
	}
	public void setBlank_paper_bb(byte[] blank_paper_bb) {
		this.blank_paper_bb = blank_paper_bb;
	}
	public String getBlank_paper_bb_type() {
		return blank_paper_bb_type;
	}
	public void setBlank_paper_bb_type(String blank_paper_bb_type) {
		this.blank_paper_bb_type = blank_paper_bb_type;
	}
	
	
}
