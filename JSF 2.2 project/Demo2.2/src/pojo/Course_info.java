package pojo;

public class Course_info {

	
	private String course_year;
	private String course_semester;
	private String course_id_id;
	private String course_num;
	private String course_name;
	private String course_class;
	private String course_teacher;
	private String course_dept;
	private String archive_status;
	private String archive_borrower_status;
	
	public String getCourse_year() {
		return course_year;
	}
	public void setCourse_year(String course_year) {
		this.course_year = course_year;
	}
	public String getCourse_semester() {
		return course_semester;
	}
	public void setCourse_semester(String course_semester) {
		this.course_semester = course_semester;
	}
	public String getCourse_id_id() {
		return course_id_id;
	}
	public void setCourse_id_id(String course_id_id) {
		this.course_id_id = course_id_id;
	}
	public String getCourse_num() {
		return course_num;
	}
	public void setCourse_num(String course_num) {
		this.course_num = course_num;
	}
	public String getCourse_name() {
		return course_name;
	}
	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}
	public String getCourse_class() {
		return course_class;
	}
	public void setCourse_class(String course_class) {
		this.course_class = course_class;
	}
	public String getCourse_teacher() {
		return course_teacher;
	}
	public void setCourse_teacher(String course_teacher) {
		this.course_teacher = course_teacher;
	}
	public String getCourse_dept() {
		return course_dept;
	}
	public void setCourse_dept(String course_dept) {
		this.course_dept = course_dept;
	}
	public String getArchive_status() {
		return archive_status;
	}
	public void setArchive_status(String archive_status) {
		this.archive_status = archive_status;
	}
	public String getArchive_borrower_status() {
		return archive_borrower_status;
	}
	public void setArchive_borrower_status(String archive_borrower_status) {
		this.archive_borrower_status = archive_borrower_status;
	}
	
	
}
