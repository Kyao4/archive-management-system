package pojo;

public class Blank_paper_a {

	
	private byte[] blank_paper_a;
	private String blank_paper_a_type;
	public byte[] getBlank_paper_a() {
		return blank_paper_a;
	}
	public void setBlank_paper_a(byte[] blank_paper_a) {
		this.blank_paper_a = blank_paper_a;
	}
	public String getBlank_paper_a_type() {
		return blank_paper_a_type;
	}
	public void setBlank_paper_a_type(String blank_paper_a_type) {
		this.blank_paper_a_type = blank_paper_a_type;
	}
	
	
}
