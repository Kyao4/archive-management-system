package pojo;

import java.io.Serializable;

public class Archive_info implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int archive_id;
	private String archive_year;
	private String archive_semester;
	private String archive_course_id_id;
	private String archive_course_num;
	private String archive_course_name;
	private String archive_class;
	private String archive_teacher;
	private String archive_dept;
	private String archive_status;
	private String archive_borrower_status;
	private int archive_borrower_status_int;
	/** 用于保证档案具有高亮效果 */
	private String archive_style;

	public int getArchive_id() {
		return archive_id;
	}

	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}

	public String getArchive_year() {
		return archive_year;
	}

	public void setArchive_year(String archive_year) {
		this.archive_year = archive_year;
	}

	public String getArchive_semester() {
		return archive_semester;
	}

	public void setArchive_semester(String archive_semester) {
		this.archive_semester = archive_semester;
	}

	public String getArchive_course_id_id() {
		return archive_course_id_id;
	}

	public void setArchive_course_id_id(String archive_course_id_id) {
		this.archive_course_id_id = archive_course_id_id;
	}

	public String getArchive_course_num() {
		return archive_course_num;
	}

	public void setArchive_course_num(String archive_course_num) {
		this.archive_course_num = archive_course_num;
	}

	public String getArchive_course_name() {
		return archive_course_name;
	}

	public void setArchive_course_name(String archive_course_name) {
		this.archive_course_name = archive_course_name;
	}

	public String getArchive_class() {
		return archive_class;
	}

	public void setArchive_class(String archive_class) {
		this.archive_class = archive_class;
	}

	public String getArchive_teacher() {
		return archive_teacher;
	}

	public void setArchive_teacher(String archive_teacher) {
		this.archive_teacher = archive_teacher;
	}

	public String getArchive_dept() {
		return archive_dept;
	}

	public void setArchive_dept(String archive_dept) {
		this.archive_dept = archive_dept;
	}

	public String getArchive_status() {
		return archive_status;
	}

	public void setArchive_status(String archive_status) {
		this.archive_status = archive_status;
	}

	public String getArchive_borrower_status() {
		return archive_borrower_status;
	}

	public void setArchive_borrower_status(String archive_borrower_status) {
		this.archive_borrower_status = archive_borrower_status;
	}

	public int getArchive_borrower_status_int() {
		return archive_borrower_status_int;
	}

	public void setArchive_borrower_status_int(int archive_borrower_status_int) {
		this.archive_borrower_status_int = archive_borrower_status_int;
	}
	
	public String getArchive_style() {
		return archive_style;
	}

	public void setArchive_style(String archive_style) {
		this.archive_style = archive_style;
	}

}
