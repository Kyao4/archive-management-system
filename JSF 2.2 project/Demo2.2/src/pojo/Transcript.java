package pojo;

import java.io.Serializable;

public class Transcript implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int transcript_id;
	private byte[] transcript_file;
	private String transcript_type;
	private int transcript_archive_id;
	public int getTranscript_id() {
		return transcript_id;
	}
	public void setTranscript_id(int transcript_id) {
		this.transcript_id = transcript_id;
	}
	public byte[] getTranscript_file() {
		return transcript_file;
	}
	public void setTranscript_file(byte[] transcript_file) {
		this.transcript_file = transcript_file;
	}
	public String getTranscript_type() {
		return transcript_type;
	}
	public void setTranscript_type(String transcript_type) {
		this.transcript_type = transcript_type;
	}
	public int getTranscript_archive_id() {
		return transcript_archive_id;
	}
	public void setTranscript_archive_id(int transcript_archive_id) {
		this.transcript_archive_id = transcript_archive_id;
	}
	
	
	
}
