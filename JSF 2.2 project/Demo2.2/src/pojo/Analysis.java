package pojo;

public class Analysis {

	private byte[] archive_analysis;
	private String archive_analysis_type;
	public byte[] getArchive_analysis() {
		return archive_analysis;
	}
	public void setArchive_analysis(byte[] archive_analysis) {
		this.archive_analysis = archive_analysis;
	}
	public String getArchive_analysis_type() {
		return archive_analysis_type;
	}
	public void setArchive_analysis_type(String archive_analysis_type) {
		this.archive_analysis_type = archive_analysis_type;
	}
	
	
}
