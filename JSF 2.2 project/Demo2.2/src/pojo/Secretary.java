package pojo;

import java.io.Serializable;

public class Secretary implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int secretary_id;
	private String secretary_name;
	private String secretary_password;
	private String secretary_oldPassword;
	private String secretary_newPassword;
	private String secretary_repeatPassword;
	
	public int getSecretary_id() {
		return secretary_id;
	}
	public void setSecretary_id(int secretary_id) {
		this.secretary_id = secretary_id;
	}
	public String getSecretary_name() {
		return secretary_name;
	}
	public void setSecretary_name(String secretary_name) {
		this.secretary_name = secretary_name;
	}
	public String getSecretary_password() {
		return secretary_password;
	}
	public void setSecretary_password(String secretary_password) {
		this.secretary_password = secretary_password;
	}
	public String getSecretary_oldPassword() {
		return secretary_oldPassword;
	}
	public void setSecretary_oldPassword(String secretary_oldPassword) {
		this.secretary_oldPassword = secretary_oldPassword;
	}
	public String getSecretary_newPassword() {
		return secretary_newPassword;
	}
	public void setSecretary_newPassword(String secretary_newPassword) {
		this.secretary_newPassword = secretary_newPassword;
	}
	public String getSecretary_repeatPassword() {
		return secretary_repeatPassword;
	}
	public void setSecretary_repeatPassword(String secretary_repeatPassword) {
		this.secretary_repeatPassword = secretary_repeatPassword;
	}

	
	
}
