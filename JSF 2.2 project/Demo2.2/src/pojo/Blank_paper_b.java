package pojo;

public class Blank_paper_b {

	private byte[] blank_paper_b;
	private String blank_paper_b_type;
	public byte[] getBlank_paper_b() {
		return blank_paper_b;
	}
	public void setBlank_paper_b(byte[] blank_paper_b) {
		this.blank_paper_b = blank_paper_b;
	}
	public String getBlank_paper_b_type() {
		return blank_paper_b_type;
	}
	public void setBlank_paper_b_type(String blank_paper_b_type) {
		this.blank_paper_b_type = blank_paper_b_type;
	}
	
	
}
