package pojo;

import java.io.Serializable;

public class ArchiveCheckInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int archive_id;
	private String archive_year;
	private String archive_semester;
	private String archive_course_id_id;
	private String archive_course_num;
	private String archive_course_name;
	private String archive_teacher;//课程授课教名称
	private String teacher_name;//提交档案教师名称
	private String archive_exam_type;
	private String archive_type;
	private String archive_storage_type;
	private int archive_stu_num;
	private int archive_actual_stu_num;
	private String archive_status;
	
	
	
	public int getArchive_id() {
		return archive_id;
	}
	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}
	public String getArchive_year() {
		return archive_year;
	}
	public void setArchive_year(String archive_year) {
		this.archive_year = archive_year;
	}
	public String getArchive_semester() {
		return archive_semester;
	}
	public void setArchive_semester(String archive_semester) {
		this.archive_semester = archive_semester;
	}
	public String getArchive_course_id_id() {
		return archive_course_id_id;
	}
	public void setArchive_course_id_id(String archive_course_id_id) {
		this.archive_course_id_id = archive_course_id_id;
	}
	public String getArchive_course_num() {
		return archive_course_num;
	}
	public void setArchive_course_num(String archive_course_num) {
		this.archive_course_num = archive_course_num;
	}
	public String getArchive_course_name() {
		return archive_course_name;
	}
	public void setArchive_course_name(String archive_course_name) {
		this.archive_course_name = archive_course_name;
	}
	public String getArchive_teacher() {
		return archive_teacher;
	}
	public void setArchive_teacher(String archive_teacher) {
		this.archive_teacher = archive_teacher;
	}
	public String getTeacher_name() {
		return teacher_name;
	}
	public void setTeacher_name(String teacher_name) {
		this.teacher_name = teacher_name;
	}
	public String getArchive_exam_type() {
		return archive_exam_type;
	}
	public void setArchive_exam_type(String archive_exam_type) {
		this.archive_exam_type = archive_exam_type;
	}
	public String getArchive_type() {
		return archive_type;
	}
	public void setArchive_type(String archive_type) {
		this.archive_type = archive_type;
	}
	public String getArchive_storage_type() {
		return archive_storage_type;
	}
	public void setArchive_storage_type(String archive_storage_type) {
		this.archive_storage_type = archive_storage_type;
	}
	public int getArchive_stu_num() {
		return archive_stu_num;
	}
	public void setArchive_stu_num(int archive_stu_num) {
		this.archive_stu_num = archive_stu_num;
	}
	public int getArchive_actual_stu_num() {
		return archive_actual_stu_num;
	}
	public void setArchive_actual_stu_num(int archive_actual_stu_num) {
		this.archive_actual_stu_num = archive_actual_stu_num;
	}
	public String getArchive_status() {
		return archive_status;
	}
	public void setArchive_status(String archive_status) {
		this.archive_status = archive_status;
	}
	
	
	
	
}
