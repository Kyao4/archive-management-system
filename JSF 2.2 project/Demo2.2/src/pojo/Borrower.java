package pojo;

public class Borrower {

	private int borrower_id;
	private String borrower_name;
	private String borrower_dept;
	private int borrower_archive_id;
	public int getBorrower_id() {
		return borrower_id;
	}
	public void setBorrower_id(int borrower_id) {
		this.borrower_id = borrower_id;
	}
	public String getBorrower_name() {
		return borrower_name;
	}
	public void setBorrower_name(String borrower_name) {
		this.borrower_name = borrower_name;
	}
	public String getBorrower_dept() {
		return borrower_dept;
	}
	public void setBorrower_dept(String borrower_dept) {
		this.borrower_dept = borrower_dept;
	}
	public int getBorrower_archive_id() {
		return borrower_archive_id;
	}
	public void setBorrower_archive_id(int borrower_archive_id) {
		this.borrower_archive_id = borrower_archive_id;
	}
	
	
	
}
