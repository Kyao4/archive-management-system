package pojo;

import java.util.ArrayList;
import java.util.List;

public class Archive_box {

	private int ab_id;
	private int ab_num;
	private String 	ab_side;
	private String ab_year;
	private String ab_semester;
	private String ab_enroll;
	private List<Archive_boxRow> rowList = new ArrayList<Archive_boxRow>();
	
	
	
	public int getAb_id() {
		return ab_id;
	}
	public void setAb_id(int ab_id) {
		this.ab_id = ab_id;
	}
	public int getAb_num() {
		return ab_num;
	}
	public void setAb_num(int ab_num) {
		this.ab_num = ab_num;
	}
	public String getAb_side() {
		return ab_side;
	}
	public void setAb_side(String ab_side) {
		this.ab_side = ab_side;
	}
	public String getAb_year() {
		return ab_year;
	}
	public void setAb_year(String ab_year) {
		this.ab_year = ab_year;
	}
	public String getAb_semester() {
		return ab_semester;
	}
	public void setAb_semester(String ab_semester) {
		this.ab_semester = ab_semester;
	}
	public String getAb_enroll() {
		return ab_enroll;
	}
	public void setAb_enroll(String ab_enroll) {
		this.ab_enroll = ab_enroll;
	}
	public List<Archive_boxRow> getRowList() {
		return rowList;
	}
	public void setRowList(List<Archive_boxRow> rowList) {
		this.rowList = rowList;
	}



}
