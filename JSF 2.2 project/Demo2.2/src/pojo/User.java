package pojo;

import java.io.Serializable;

public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int user_id;
	private String user_name;
	private String user_password;
	
	private String user_oldPassword;
	private String user_newPassword;
	private String user_repeatPassword;
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public String getUser_oldPassword() {
		return user_oldPassword;
	}
	public void setUser_oldPassword(String user_oldPassword) {
		this.user_oldPassword = user_oldPassword;
	}
	public String getUser_newPassword() {
		return user_newPassword;
	}
	public void setUser_newPassword(String user_newPassword) {
		this.user_newPassword = user_newPassword;
	}
	public String getUser_repeatPassword() {
		return user_repeatPassword;
	}
	public void setUser_repeatPassword(String user_repeatPassword) {
		this.user_repeatPassword = user_repeatPassword;
	}
	
	
	
}
