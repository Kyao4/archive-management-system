package pojo;

import java.util.ArrayList;
import java.util.List;

public class Archive_boxRow {

	
	private List<Archive_inner_box> innerBoxList = new ArrayList<Archive_inner_box>();

	public List<Archive_inner_box> getInnerBoxList() {
		return innerBoxList;
	}

	public void setInnerBoxList(List<Archive_inner_box> innerBoxList) {
		this.innerBoxList = innerBoxList;
	} 
	
}
