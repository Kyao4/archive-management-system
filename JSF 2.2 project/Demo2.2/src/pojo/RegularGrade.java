package pojo;

import java.io.Serializable;

public class RegularGrade implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int rg_id;
	private byte[] rg_file;
	private String rg_type;
	private int rg_archive_id;
	public int getRg_id() {
		return rg_id;
	}
	public void setRg_id(int rg_id) {
		this.rg_id = rg_id;
	}
	public byte[] getRg_file() {
		return rg_file;
	}
	public void setRg_file(byte[] rg_file) {
		this.rg_file = rg_file;
	}
	public String getRg_type() {
		return rg_type;
	}
	public void setRg_type(String rg_type) {
		this.rg_type = rg_type;
	}
	public int getRg_archive_id() {
		return rg_archive_id;
	}
	public void setRg_archive_id(int rg_archive_id) {
		this.rg_archive_id = rg_archive_id;
	}
	
	
}
