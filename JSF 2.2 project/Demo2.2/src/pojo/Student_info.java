package pojo;

import java.io.Serializable;

public class Student_info implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String archive_year;
	private String archive_semester;
	private String archive_course_name;
	private String archive_teacher;
	private String stu_name;
	private String stu_id;
	private String stu_class;
	private double stu_grade;
	public String getArchive_year() {
		return archive_year;
	}
	public void setArchive_year(String archive_year) {
		this.archive_year = archive_year;
	}
	public String getArchive_semester() {
		return archive_semester;
	}
	public void setArchive_semester(String archive_semester) {
		this.archive_semester = archive_semester;
	}
	public String getArchive_course_name() {
		return archive_course_name;
	}
	public void setArchive_course_name(String archive_course_name) {
		this.archive_course_name = archive_course_name;
	}
	public String getArchive_teacher() {
		return archive_teacher;
	}
	public void setArchive_teacher(String archive_teacher) {
		this.archive_teacher = archive_teacher;
	}
	public String getStu_name() {
		return stu_name;
	}
	public void setStu_name(String stu_name) {
		this.stu_name = stu_name;
	}
	public String getStu_id() {
		return stu_id;
	}
	public void setStu_id(String stu_id) {
		this.stu_id = stu_id;
	}
	public String getStu_class() {
		return stu_class;
	}
	public void setStu_class(String stu_class) {
		this.stu_class = stu_class;
	}
	public double getStu_grade() {
		return stu_grade;
	}
	public void setStu_grade(double stu_grade) {
		this.stu_grade = stu_grade;
	}
	
}
