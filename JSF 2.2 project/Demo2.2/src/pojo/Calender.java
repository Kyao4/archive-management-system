package pojo;

public class Calender {

	private byte[] archive_calender;
	private String archive_calender_type;
	
	public byte[] getArchive_calender() {
		return archive_calender;
	}
	public void setArchive_calender(byte[] archive_calender) {
		this.archive_calender = archive_calender;
	}
	public String getArchive_calender_type() {
		return archive_calender_type;
	}
	public void setArchive_calender_type(String archive_calender_type) {
		this.archive_calender_type = archive_calender_type;
	}
	
	
}
