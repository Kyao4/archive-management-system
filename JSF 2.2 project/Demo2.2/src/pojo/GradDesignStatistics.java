package pojo;

import java.io.Serializable;

public class GradDesignStatistics implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String gds_school;
	private int stu_num;
	private int topic_property_a;
	private double topic_property_a_percent;
	private int topic_property_b;
	private double topic_property_b_percent;
	private int topic_property_c;
	private double topic_property_c_percent;
	private int topic_property_d;
	private double topic_property_d_percent;
	private int topic_property_e;
	private double topic_property_e_percent;
	private int topic_source_a;
	private double topic_source_a_percent;
	private int topic_source_b;
	private double topic_source_b_percent;
	private int topic_source_c;
	private double topic_source_c_percent;
	private int topic_source_d;
	private double topic_source_d_percent;
	private int topic_source_e;
	private double topic_source_e_percent;
	private int topic_difficulty_a;
	private double topic_difficulty_a_percent;
	private int topic_difficulty_b;
	private double topic_difficulty_b_percent;
	private int topic_difficulty_c;
	private double topic_difficulty_c_percent;
	private int topic_difficulty_d;
	private double topic_difficulty_d_percent;
	private int topic_difficulty_e;
	private double topic_difficulty_e_percent;
	private int grade_a;
	private double grade_a_percent;
	private int grade_b;
	private double grade_b_percent;
	private int grade_c;
	private double grade_c_percent;
	private int grade_d;
	private double grade_d_percent;
	private int grade_e;
	private double grade_e_percent;
	public String getGds_school() {
		return gds_school;
	}
	public void setGds_school(String gds_school) {
		this.gds_school = gds_school;
	}
	public int getStu_num() {
		return stu_num;
	}
	public void setStu_num(int stu_num) {
		this.stu_num = stu_num;
	}
	public int getTopic_property_a() {
		return topic_property_a;
	}
	public void setTopic_property_a(int topic_property_a) {
		this.topic_property_a = topic_property_a;
	}
	public double getTopic_property_a_percent() {
		return topic_property_a_percent;
	}
	public void setTopic_property_a_percent(double topic_property_a_percent) {
		this.topic_property_a_percent = topic_property_a_percent;
	}
	public int getTopic_property_b() {
		return topic_property_b;
	}
	public void setTopic_property_b(int topic_property_b) {
		this.topic_property_b = topic_property_b;
	}
	public double getTopic_property_b_percent() {
		return topic_property_b_percent;
	}
	public void setTopic_property_b_percent(double topic_property_b_percent) {
		this.topic_property_b_percent = topic_property_b_percent;
	}
	public int getTopic_property_c() {
		return topic_property_c;
	}
	public void setTopic_property_c(int topic_property_c) {
		this.topic_property_c = topic_property_c;
	}
	public double getTopic_property_c_percent() {
		return topic_property_c_percent;
	}
	public void setTopic_property_c_percent(double topic_property_c_percent) {
		this.topic_property_c_percent = topic_property_c_percent;
	}
	public int getTopic_property_d() {
		return topic_property_d;
	}
	public void setTopic_property_d(int topic_property_d) {
		this.topic_property_d = topic_property_d;
	}
	public double getTopic_property_d_percent() {
		return topic_property_d_percent;
	}
	public void setTopic_property_d_percent(double topic_property_d_percent) {
		this.topic_property_d_percent = topic_property_d_percent;
	}
	public int getTopic_property_e() {
		return topic_property_e;
	}
	public void setTopic_property_e(int topic_property_e) {
		this.topic_property_e = topic_property_e;
	}
	public double getTopic_property_e_percent() {
		return topic_property_e_percent;
	}
	public void setTopic_property_e_percent(double topic_property_e_percent) {
		this.topic_property_e_percent = topic_property_e_percent;
	}
	public int getTopic_source_a() {
		return topic_source_a;
	}
	public void setTopic_source_a(int topic_source_a) {
		this.topic_source_a = topic_source_a;
	}
	public double getTopic_source_a_percent() {
		return topic_source_a_percent;
	}
	public void setTopic_source_a_percent(double topic_source_a_percent) {
		this.topic_source_a_percent = topic_source_a_percent;
	}
	public int getTopic_source_b() {
		return topic_source_b;
	}
	public void setTopic_source_b(int topic_source_b) {
		this.topic_source_b = topic_source_b;
	}
	public double getTopic_source_b_percent() {
		return topic_source_b_percent;
	}
	public void setTopic_source_b_percent(double topic_source_b_percent) {
		this.topic_source_b_percent = topic_source_b_percent;
	}
	public int getTopic_source_c() {
		return topic_source_c;
	}
	public void setTopic_source_c(int topic_source_c) {
		this.topic_source_c = topic_source_c;
	}
	public double getTopic_source_c_percent() {
		return topic_source_c_percent;
	}
	public void setTopic_source_c_percent(double topic_source_c_percent) {
		this.topic_source_c_percent = topic_source_c_percent;
	}
	public int getTopic_source_d() {
		return topic_source_d;
	}
	public void setTopic_source_d(int topic_source_d) {
		this.topic_source_d = topic_source_d;
	}
	public double getTopic_source_d_percent() {
		return topic_source_d_percent;
	}
	public void setTopic_source_d_percent(double topic_source_d_percent) {
		this.topic_source_d_percent = topic_source_d_percent;
	}
	public int getTopic_source_e() {
		return topic_source_e;
	}
	public void setTopic_source_e(int topic_source_e) {
		this.topic_source_e = topic_source_e;
	}
	public double getTopic_source_e_percent() {
		return topic_source_e_percent;
	}
	public void setTopic_source_e_percent(double topic_source_e_percent) {
		this.topic_source_e_percent = topic_source_e_percent;
	}
	public int getTopic_difficulty_a() {
		return topic_difficulty_a;
	}
	public void setTopic_difficulty_a(int topic_difficulty_a) {
		this.topic_difficulty_a = topic_difficulty_a;
	}
	public double getTopic_difficulty_a_percent() {
		return topic_difficulty_a_percent;
	}
	public void setTopic_difficulty_a_percent(double topic_difficulty_a_percent) {
		this.topic_difficulty_a_percent = topic_difficulty_a_percent;
	}
	public int getTopic_difficulty_b() {
		return topic_difficulty_b;
	}
	public void setTopic_difficulty_b(int topic_difficulty_b) {
		this.topic_difficulty_b = topic_difficulty_b;
	}
	public double getTopic_difficulty_b_percent() {
		return topic_difficulty_b_percent;
	}
	public void setTopic_difficulty_b_percent(double topic_difficulty_b_percent) {
		this.topic_difficulty_b_percent = topic_difficulty_b_percent;
	}
	public int getTopic_difficulty_c() {
		return topic_difficulty_c;
	}
	public void setTopic_difficulty_c(int topic_difficulty_c) {
		this.topic_difficulty_c = topic_difficulty_c;
	}
	public double getTopic_difficulty_c_percent() {
		return topic_difficulty_c_percent;
	}
	public void setTopic_difficulty_c_percent(double topic_difficulty_c_percent) {
		this.topic_difficulty_c_percent = topic_difficulty_c_percent;
	}
	public int getTopic_difficulty_d() {
		return topic_difficulty_d;
	}
	public void setTopic_difficulty_d(int topic_difficulty_d) {
		this.topic_difficulty_d = topic_difficulty_d;
	}
	public double getTopic_difficulty_d_percent() {
		return topic_difficulty_d_percent;
	}
	public void setTopic_difficulty_d_percent(double topic_difficulty_d_percent) {
		this.topic_difficulty_d_percent = topic_difficulty_d_percent;
	}
	public int getTopic_difficulty_e() {
		return topic_difficulty_e;
	}
	public void setTopic_difficulty_e(int topic_difficulty_e) {
		this.topic_difficulty_e = topic_difficulty_e;
	}
	public double getTopic_difficulty_e_percent() {
		return topic_difficulty_e_percent;
	}
	public void setTopic_difficulty_e_percent(double topic_difficulty_e_percent) {
		this.topic_difficulty_e_percent = topic_difficulty_e_percent;
	}
	public int getGrade_a() {
		return grade_a;
	}
	public void setGrade_a(int grade_a) {
		this.grade_a = grade_a;
	}
	public double getGrade_a_percent() {
		return grade_a_percent;
	}
	public void setGrade_a_percent(double grade_a_percent) {
		this.grade_a_percent = grade_a_percent;
	}
	public int getGrade_b() {
		return grade_b;
	}
	public void setGrade_b(int grade_b) {
		this.grade_b = grade_b;
	}
	public double getGrade_b_percent() {
		return grade_b_percent;
	}
	public void setGrade_b_percent(double grade_b_percent) {
		this.grade_b_percent = grade_b_percent;
	}
	public int getGrade_c() {
		return grade_c;
	}
	public void setGrade_c(int grade_c) {
		this.grade_c = grade_c;
	}
	public double getGrade_c_percent() {
		return grade_c_percent;
	}
	public void setGrade_c_percent(double grade_c_percent) {
		this.grade_c_percent = grade_c_percent;
	}
	public int getGrade_d() {
		return grade_d;
	}
	public void setGrade_d(int grade_d) {
		this.grade_d = grade_d;
	}
	public double getGrade_d_percent() {
		return grade_d_percent;
	}
	public void setGrade_d_percent(double grade_d_percent) {
		this.grade_d_percent = grade_d_percent;
	}
	public int getGrade_e() {
		return grade_e;
	}
	public void setGrade_e(int grade_e) {
		this.grade_e = grade_e;
	}
	public double getGrade_e_percent() {
		return grade_e_percent;
	}
	public void setGrade_e_percent(double grade_e_percent) {
		this.grade_e_percent = grade_e_percent;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
}
