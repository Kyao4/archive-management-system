package pojo;

import java.io.Serializable;

public class Teacher implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** 教师号 */
	private String teacher_id;
	/** 教师名称 */
	private String teacher_name;
	/** 所属部门*/
	private String teacher_dept;
	/** 教师密码 */
	private String teacher_password;

	private String teacher_oldPassword;
	private String teacher_newPassword;
	private String teacher_repeatPassword;
	
	public String getTeacher_id() {
		return teacher_id;
	}
	public void setTeacher_id(String teacher_id) {
		this.teacher_id = teacher_id;
	}
	public String getTeacher_name() {
		return teacher_name;
	}
	public void setTeacher_name(String teacher_name) {
		this.teacher_name = teacher_name;
	}
	public String getTeacher_dept() {
		return teacher_dept;
	}
	public void setTeacher_dept(String teacher_dept) {
		this.teacher_dept = teacher_dept;
	}
	public String getTeacher_password() {
		return teacher_password;
	}
	public void setTeacher_password(String teacher_password) {
		this.teacher_password = teacher_password;
	}
	public String getTeacher_oldPassword() {
		return teacher_oldPassword;
	}
	public void setTeacher_oldPassword(String teacher_oldPassword) {
		this.teacher_oldPassword = teacher_oldPassword;
	}
	public String getTeacher_newPassword() {
		return teacher_newPassword;
	}
	public void setTeacher_newPassword(String teacher_newPassword) {
		this.teacher_newPassword = teacher_newPassword;
	}
	public String getTeacher_repeatPassword() {
		return teacher_repeatPassword;
	}
	public void setTeacher_repeatPassword(String teacher_repeatPassword) {
		this.teacher_repeatPassword = teacher_repeatPassword;
	}
	
	

}
