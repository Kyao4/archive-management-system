package pojo;

public class Answer {

	private byte[] archive_answer;
	private String archive_answer_type;
	public byte[] getArchive_answer() {
		return archive_answer;
	}
	public void setArchive_answer(byte[] archive_answer) {
		this.archive_answer = archive_answer;
	}
	public String getArchive_answer_type() {
		return archive_answer_type;
	}
	public void setArchive_answer_type(String archive_answer_type) {
		this.archive_answer_type = archive_answer_type;
	}
	
	
}
