package pojo;

import java.io.Serializable;

public class Archive implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/** 档案编号*/
	private int archive_id;
	/** 学年学期*/
	private String archive_year;
	/** 学年学期*/
	private String archive_semester;
	/**课程名称 */ 
	private String archive_course_name;
	/**课程号*/
	private String archive_course_id_id;
	/**课序号*/
	private String archive_course_num;
	/**任课教师*/
	private String archive_teacher;
	/**授课班级*/
	private String archive_class;
	/**所属部门*/
	private String archive_dept;
	/**录取批次*/
	private String archive_enroll;
	/**厚度*/
	private int archive_thickness;
	/**状态*/
	private int archive_status;
	/**档案类别*/
	private String archive_type;
	/**档案保存方式*/
	private String archive_storage_type;
	/**考核形式*/
	private String archive_exam_type;
	/**成绩单人数*/
	private int archive_stu_num;
	/**实际考试人数*/
	private int archive_actual_stu_num;
	/**教学日历*/
	private byte[] archive_calender;
	/**教学日历内容类型*/
	private String archive_calender_type;

	/**空白试卷A*/
	private byte[] archive_blank_paper_a;
	/**空白试卷A内容类型*/
	private String archive_blank_paper_a_type;
	/**空白试卷B*/
	private byte[] archive_blank_paper_b;
	/**空白试卷B内容类型*/
	private String archive_blank_paper_b_type;
	/**空白试卷A'*/
	private byte[] archive_blank_paper_aa;
	/**空白试卷A'内容类型*/
	private String archive_blank_paper_aa_type;
	/**空白试卷B'*/
	private byte[] archive_blank_paper_bb;
	/**空白试卷B'内容类型*/
	private String archive_blank_paper_bb_type;
	/**考试分析报告单*/
	private byte[] archive_analysis;
	/**考试分析报告单内容类型*/
	private String archive_analysis_type;
	/**标准答案*/
	private byte[] archive_answer;
	/**标准答案内容类型*/
	private String archive_answer_type;
	/**教师号*/
	private String archive_teacher_id;
	/**档案柜号*/
	private int archive_ab_id;
	/**档案柜格子编号*/
	private int archive_aib_id;
	/**课程编号*/
	private int archive_course_id;
	/**借阅人姓名*/
	private String archive_borrower_name;
	/**借阅人部门*/
	private String archive_borrower_dept;

	private String archive_borrower_status;
	
	private int archive_borrower_status_int;
	
	private String archive_note;
	
	private String archive_tooltip;
	
	private String archive_course_name_vertical;
	
	private String archive_style;
	
	//存储文件
	private byte[] rg_file;
	
	private String rg_type;
	
	private byte[] transcript_file;
	
	private String transcript_type;
	
	public int getArchive_id() {
		return archive_id;
	}

	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}

	public String getArchive_year() {
		return archive_year;
	}

	public void setArchive_year(String archive_year) {
		this.archive_year = archive_year;
	}

	public String getArchive_semester() {
		return archive_semester;
	}

	public void setArchive_semester(String archive_semester) {
		this.archive_semester = archive_semester;
	}

	public String getArchive_course_name() {
		return archive_course_name;
	}

	public void setArchive_course_name(String archive_course_name) {
		this.archive_course_name = archive_course_name;
	}

	public String getArchive_course_id_id() {
		return archive_course_id_id;
	}

	public void setArchive_course_id_id(String archive_course_id_id) {
		this.archive_course_id_id = archive_course_id_id;
	}

	public String getArchive_course_num() {
		return archive_course_num;
	}

	public void setArchive_course_num(String archive_course_num) {
		this.archive_course_num = archive_course_num;
	}

	public String getArchive_teacher() {
		return archive_teacher;
	}

	public void setArchive_teacher(String archive_teacher) {
		this.archive_teacher = archive_teacher;
	}

	public String getArchive_class() {
		return archive_class;
	}

	public void setArchive_class(String archive_class) {
		this.archive_class = archive_class;
	}

	public String getArchive_dept() {
		return archive_dept;
	}

	public void setArchive_dept(String archive_dept) {
		this.archive_dept = archive_dept;
	}

	public String getArchive_enroll() {
		return archive_enroll;
	}

	public void setArchive_enroll(String archive_enroll) {
		this.archive_enroll = archive_enroll;
	}

	public int getArchive_thickness() {
		return archive_thickness;
	}

	public void setArchive_thickness(int archive_thickness) {
		this.archive_thickness = archive_thickness;
	}

	public int getArchive_status() {
		return archive_status;
	}

	public void setArchive_status(int archive_status) {
		this.archive_status = archive_status;
	}

	public String getArchive_type() {
		return archive_type;
	}

	public void setArchive_type(String archive_type) {
		this.archive_type = archive_type;
	}

	public String getArchive_storage_type() {
		return archive_storage_type;
	}

	public void setArchive_storage_type(String archive_storage_type) {
		this.archive_storage_type = archive_storage_type;
	}

	public String getArchive_exam_type() {
		return archive_exam_type;
	}

	public void setArchive_exam_type(String archive_exam_type) {
		this.archive_exam_type = archive_exam_type;
	}

	public int getArchive_stu_num() {
		return archive_stu_num;
	}

	public void setArchive_stu_num(int archive_stu_num) {
		this.archive_stu_num = archive_stu_num;
	}

	public int getArchive_actual_stu_num() {
		return archive_actual_stu_num;
	}

	public void setArchive_actual_stu_num(int archive_actual_stu_num) {
		this.archive_actual_stu_num = archive_actual_stu_num;
	}

	public byte[] getArchive_calender() {
		return archive_calender;
	}

	public void setArchive_calender(byte[] archive_calender) {
		this.archive_calender = archive_calender;
	}

	public String getArchive_calender_type() {
		return archive_calender_type;
	}

	public void setArchive_calender_type(String archive_calender_type) {
		this.archive_calender_type = archive_calender_type;
	}

	public byte[] getArchive_blank_paper_a() {
		return archive_blank_paper_a;
	}

	public void setArchive_blank_paper_a(byte[] archive_blank_paper_a) {
		this.archive_blank_paper_a = archive_blank_paper_a;
	}

	public String getArchive_blank_paper_a_type() {
		return archive_blank_paper_a_type;
	}

	public void setArchive_blank_paper_a_type(String archive_blank_paper_a_type) {
		this.archive_blank_paper_a_type = archive_blank_paper_a_type;
	}

	public byte[] getArchive_blank_paper_b() {
		return archive_blank_paper_b;
	}

	public void setArchive_blank_paper_b(byte[] archive_blank_paper_b) {
		this.archive_blank_paper_b = archive_blank_paper_b;
	}

	public String getArchive_blank_paper_b_type() {
		return archive_blank_paper_b_type;
	}

	public void setArchive_blank_paper_b_type(String archive_blank_paper_b_type) {
		this.archive_blank_paper_b_type = archive_blank_paper_b_type;
	}

	public byte[] getArchive_blank_paper_aa() {
		return archive_blank_paper_aa;
	}

	public void setArchive_blank_paper_aa(byte[] archive_blank_paper_aa) {
		this.archive_blank_paper_aa = archive_blank_paper_aa;
	}

	public String getArchive_blank_paper_aa_type() {
		return archive_blank_paper_aa_type;
	}

	public void setArchive_blank_paper_aa_type(String archive_blank_paper_aa_type) {
		this.archive_blank_paper_aa_type = archive_blank_paper_aa_type;
	}

	public byte[] getArchive_blank_paper_bb() {
		return archive_blank_paper_bb;
	}

	public void setArchive_blank_paper_bb(byte[] archive_blank_paper_bb) {
		this.archive_blank_paper_bb = archive_blank_paper_bb;
	}

	public String getArchive_blank_paper_bb_type() {
		return archive_blank_paper_bb_type;
	}

	public void setArchive_blank_paper_bb_type(String archive_blank_paper_bb_type) {
		this.archive_blank_paper_bb_type = archive_blank_paper_bb_type;
	}

	public byte[] getArchive_analysis() {
		return archive_analysis;
	}

	public void setArchive_analysis(byte[] archive_analysis) {
		this.archive_analysis = archive_analysis;
	}

	public String getArchive_analysis_type() {
		return archive_analysis_type;
	}

	public void setArchive_analysis_type(String archive_analysis_type) {
		this.archive_analysis_type = archive_analysis_type;
	}

	public byte[] getArchive_answer() {
		return archive_answer;
	}

	public void setArchive_answer(byte[] archive_answer) {
		this.archive_answer = archive_answer;
	}

	public String getArchive_answer_type() {
		return archive_answer_type;
	}

	public void setArchive_answer_type(String archive_answer_type) {
		this.archive_answer_type = archive_answer_type;
	}

	public String getArchive_teacher_id() {
		return archive_teacher_id;
	}

	public void setArchive_teacher_id(String archive_teacher_id) {
		this.archive_teacher_id = archive_teacher_id;
	}

	public int getArchive_ab_id() {
		return archive_ab_id;
	}

	public void setArchive_ab_id(int archive_ab_id) {
		this.archive_ab_id = archive_ab_id;
	}

	public int getArchive_aib_id() {
		return archive_aib_id;
	}

	public void setArchive_aib_id(int archive_aib_id) {
		this.archive_aib_id = archive_aib_id;
	}

	public int getArchive_course_id() {
		return archive_course_id;
	}

	public void setArchive_course_id(int archive_course_id) {
		this.archive_course_id = archive_course_id;
	}

	public String getArchive_borrower_name() {
		return archive_borrower_name;
	}

	public void setArchive_borrower_name(String archive_borrower_name) {
		this.archive_borrower_name = archive_borrower_name;
	}

	public String getArchive_borrower_dept() {
		return archive_borrower_dept;
	}

	public void setArchive_borrower_dept(String archive_borrower_dept) {
		this.archive_borrower_dept = archive_borrower_dept;
	}

	
	public int getArchive_borrower_status_int() {
		return archive_borrower_status_int;
	}

	public void setArchive_borrower_status_int(int archive_borrower_status_int) {
		this.archive_borrower_status_int = archive_borrower_status_int;
	}

	public String getArchive_borrower_status() {
		return archive_borrower_status;
	}

	public void setArchive_borrower_status(String archive_borrower_status) {
		this.archive_borrower_status = archive_borrower_status;
	}

	public String getArchive_note() {
		return archive_note;
	}

	public void setArchive_note(String archive_note) {
		this.archive_note = archive_note;
	}

	public String getArchive_tooltip() {
		return archive_tooltip;
	}

	public void setArchive_tooltip(String archive_tooltip) {
		this.archive_tooltip = archive_tooltip;
	}

	public String getArchive_course_name_vertical() {
		return archive_course_name_vertical;
	}

	public void setArchive_course_name_vertical(String archive_course_name_vertical) {
		this.archive_course_name_vertical = archive_course_name_vertical;
	}

	public String getArchive_style() {
		return archive_style;
	}

	public void setArchive_style(String archive_style) {
		this.archive_style = archive_style;
	}

	public byte[] getRg_file() {
		return rg_file;
	}

	public String getRg_type() {
		return rg_type;
	}

	public void setRg_file(byte[] rg_file) {
		this.rg_file = rg_file;
	}

	public void setRg_type(String rg_type) {
		this.rg_type = rg_type;
	}

	public byte[] getTranscript_file() {
		return transcript_file;
	}

	public String getTranscript_type() {
		return transcript_type;
	}

	public void setTranscript_file(byte[] transcript_file) {
		this.transcript_file = transcript_file;
	}

	public void setTranscript_type(String transcript_type) {
		this.transcript_type = transcript_type;
	}
	
	
}
