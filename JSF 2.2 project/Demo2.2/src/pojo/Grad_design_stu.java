package pojo;

import java.io.Serializable;

public class Grad_design_stu implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String gds_year;
	private String gds_school;
	private String gds_id;
	private String gds_name;
	private String gds_major;
	private String gds_class;
	private String gds_topic_name;
	private String gds_topic_property;
	private String gds_topic_source;
	private String gds_topic_difficulty;
	private String gds_teacher;
	private String gds_teacher_title;
	private double gds_teacher_grade;
	private String gds_review_teacher;
	private String gds_review_teacher_title;
	private double gds_review_grade;
	private double gds_defense_grade;
	private double gds_grade;
	public String getGds_year() {
		return gds_year;
	}
	public void setGds_year(String gds_year) {
		this.gds_year = gds_year;
	}
	public String getGds_school() {
		return gds_school;
	}
	public void setGds_school(String gds_school) {
		this.gds_school = gds_school;
	}
	public String getGds_id() {
		return gds_id;
	}
	public void setGds_id(String gds_id) {
		this.gds_id = gds_id;
	}
	public String getGds_name() {
		return gds_name;
	}
	public void setGds_name(String gds_name) {
		this.gds_name = gds_name;
	}
	public String getGds_major() {
		return gds_major;
	}
	public void setGds_major(String gds_major) {
		this.gds_major = gds_major;
	}
	public String getGds_class() {
		return gds_class;
	}
	public void setGds_class(String gds_class) {
		this.gds_class = gds_class;
	}
	public String getGds_topic_name() {
		return gds_topic_name;
	}
	public void setGds_topic_name(String gds_topic_name) {
		this.gds_topic_name = gds_topic_name;
	}
	public String getGds_topic_property() {
		return gds_topic_property;
	}
	public void setGds_topic_property(String gds_topic_property) {
		this.gds_topic_property = gds_topic_property;
	}
	public String getGds_topic_source() {
		return gds_topic_source;
	}
	public void setGds_topic_source(String gds_topic_source) {
		this.gds_topic_source = gds_topic_source;
	}
	public String getGds_topic_difficulty() {
		return gds_topic_difficulty;
	}
	public void setGds_topic_difficulty(String gds_topic_difficulty) {
		this.gds_topic_difficulty = gds_topic_difficulty;
	}
	public String getGds_teacher() {
		return gds_teacher;
	}
	public void setGds_teacher(String gds_teacher) {
		this.gds_teacher = gds_teacher;
	}
	public String getGds_teacher_title() {
		return gds_teacher_title;
	}
	public void setGds_teacher_title(String gds_teacher_title) {
		this.gds_teacher_title = gds_teacher_title;
	}
	public double getGds_teacher_grade() {
		return gds_teacher_grade;
	}
	public void setGds_teacher_grade(double gds_teacher_grade) {
		this.gds_teacher_grade = gds_teacher_grade;
	}
	public String getGds_review_teacher() {
		return gds_review_teacher;
	}
	public void setGds_review_teacher(String gds_review_teacher) {
		this.gds_review_teacher = gds_review_teacher;
	}
	public String getGds_review_teacher_title() {
		return gds_review_teacher_title;
	}
	public void setGds_review_teacher_title(String gds_review_teacher_title) {
		this.gds_review_teacher_title = gds_review_teacher_title;
	}
	public double getGds_review_grade() {
		return gds_review_grade;
	}
	public void setGds_review_grade(double gds_review_grade) {
		this.gds_review_grade = gds_review_grade;
	}
	public double getGds_defense_grade() {
		return gds_defense_grade;
	}
	public void setGds_defense_grade(double gds_defense_grade) {
		this.gds_defense_grade = gds_defense_grade;
	}
	public double getGds_grade() {
		return gds_grade;
	}
	public void setGds_grade(double gds_grade) {
		this.gds_grade = gds_grade;
	}
	
	
}
