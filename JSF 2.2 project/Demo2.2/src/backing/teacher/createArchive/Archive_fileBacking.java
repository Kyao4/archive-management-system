package backing.teacher.createArchive;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import model.Constants;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import dao.regular_grade.RegularGradeDAO;
import dao.transcript.TranscriptDAO;
import pojo.Analysis;
import pojo.Answer;
import pojo.Blank_paper_a;
import pojo.Blank_paper_aa;
import pojo.Blank_paper_b;
import pojo.Blank_paper_bb;
import pojo.Calender;
import pojo.RegularGrade;
import pojo.Transcript;
import backing.BaseBacking;

public class Archive_fileBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private TranscriptDAO transcriptdao = dao.getTranscriptDAO();
	private RegularGradeDAO regularGradedao = dao.getRegularGradeDAO();
		
	private byte[] archive_calender = null;
	private List<Transcript> transcriptList = new ArrayList<Transcript>();
	private List<RegularGrade> rgList = new ArrayList<RegularGrade>();
	private byte[] archive_blank_paper_a = null;
	private byte[] archive_blank_paper_b = null;
	private byte[] archive_blank_paper_aa = null;
	private byte[] archive_blank_paper_bb = null;
	private byte[] archive_analysis = null;
	private byte[] archive_answer = null;
	
	private String archive_calender_type = null;
	private String archive_blank_paper_a_type = null;
	private String archive_blank_paper_b_type = null;
	private String archive_blank_paper_aa_type = null;
	private String archive_blank_paper_bb_type = null;
	private String archive_analysis_type = null;
	private String archive_answer_type = null;
	
	private int archive_id;
	private boolean student_flag;
	private boolean edit_flag;
	
	public int getArchive_id() {
		return archive_id;
	}
	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}
	public boolean isStudent_flag() {
		return student_flag;
	}
	public void setStudent_flag(boolean student_flag) {
		this.student_flag = student_flag;
	}
	
	public boolean isEdit_flag() {
		return edit_flag;
	}
	public void setEdit_flag(boolean edit_flag) {
		this.edit_flag = edit_flag;
	}
	
	public void init(ComponentSystemEvent event) {
		Calender calender = archivedao.getCalender(archive_id);
		if(calender != null) {
			archive_calender = calender.getArchive_calender();	
		}
		Blank_paper_a blank_paper_a = archivedao.getBlank_paper_a(archive_id);
		if(blank_paper_a != null) {
			archive_blank_paper_a = blank_paper_a.getBlank_paper_a();	
		}
		Blank_paper_b blank_paper_b = archivedao.getBlank_paper_b(archive_id);
		if(blank_paper_b != null) {
			archive_blank_paper_b = blank_paper_b.getBlank_paper_b();	
		}
		Blank_paper_aa blank_paper_aa = archivedao.getBlank_paper_aa(archive_id);
		if(blank_paper_aa != null) {
			archive_blank_paper_aa = blank_paper_aa.getBlank_paper_aa();	
		}
		Blank_paper_bb blank_paper_bb = archivedao.getBlank_paper_bb(archive_id);
		if(blank_paper_bb != null) {
			archive_blank_paper_bb = blank_paper_bb.getBlank_paper_bb();	
		}
		Analysis analysis = archivedao.getAnalysis(archive_id);
		if(analysis != null) {
			archive_analysis = analysis.getArchive_analysis();	
		}
		Answer answer = archivedao.getAnswer(archive_id);
		if(answer != null) {
			archive_answer = answer.getArchive_answer();	
		}
	}
	
	public String submit() {
//	if(archive_calender == null) {
//		getContext().addMessage("formMsg", new FacesMessage(FacesMessage.SEVERITY_WARN,"警告","您没有上传教学日历"));
//		return null;
//	} 
//	if(transcriptList.size() == 0) {
//		getContext().addMessage("formMsg", new FacesMessage(FacesMessage.SEVERITY_WARN,"警告","您没有上传成绩单"));
//		return null;
//	}
//	if(rgList.size() == 0) {
//		getContext().addMessage("formMsg", new FacesMessage(FacesMessage.SEVERITY_WARN,"警告","您没有上传平时成绩"));
//		return null;
//	}
	
//	if(archive_blank_paper_a == null) {
//		getContext().addMessage("formMsg", new FacesMessage(FacesMessage.SEVERITY_WARN,"警告","您没有上传空白试卷A"));
//		return null;
//	}
//	if(archive_blank_paper_b == null) {
//		getContext().addMessage("formMsg", new FacesMessage(FacesMessage.SEVERITY_WARN,"警告","您没有上传空白试卷B"));
//		return null;
//	}
//
//	if(archive_blank_paper_aa == null) {
//		getContext().addMessage("formMsg", new FacesMessage(FacesMessage.SEVERITY_WARN,"警告","您没有上传空白试卷A'"));
//		return null;
//	}
//	if(archive_blank_paper_bb == null) {
//		getContext().addMessage("formMsg", new FacesMessage(FacesMessage.SEVERITY_WARN,"警告","您没有上传空白试卷B‘"));
//		return null;
//	}
//
//	if(archive_analysis == null) {
//		getContext().addMessage("formMsg", new FacesMessage(FacesMessage.SEVERITY_WARN,"警告","您没有上传考试分析报告"));
//		return null;
//	}
//
//	if(archive_answer == null) {
//		getContext().addMessage("formMsg", new FacesMessage(FacesMessage.SEVERITY_WARN,"警告","您没有上传标准答案"));
//		return null;
//	} 
	
		
		if(transcriptList.size() != 0) {
			transcriptdao.deleteTranscriptById(archive_id);//先清空成绩单
			for(int i = 0; i < transcriptList.size(); i++) {
				Transcript transcript = transcriptList.get(i);
				transcript.setTranscript_archive_id(archive_id);
				boolean flag = transcriptdao.createTrancript(transcript);
				if(!flag) {
					getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "警告" ,"成绩单没有存入档案"));
					transcriptdao.deleteTranscriptById(archive_id);
					return null;
				}
			}
		}
//		getContext().addMessage(null , new FacesMessage("成绩单已经成功存入档案"));
		
		if(rgList.size() != 0) {
			regularGradedao.deleteRegularGradeByArchive_id(archive_id);//先清空平时成绩
			for(int i = 0; i < rgList.size(); i++) {
				RegularGrade rg = rgList.get(i);
				rg.setRg_archive_id(archive_id);
				boolean flag = regularGradedao.createRegularGrade(rg);
				if(!flag) {
					getContext().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_WARN, "警告" ,"平时成绩存入档案不成功"));
					regularGradedao.deleteRegularGradeByArchive_id(archive_id);
					return null;
				}
			}
		}
//		getContext().addMessage(null , new FacesMessage("平时成绩已经成功存入档案"));
	
	
		if(student_flag) {
			return "/teacher/createArchive/student_info?faces-redirect=true&amp;archive_id=" + archive_id + "&amp;edit_flag=" + edit_flag;
		} else {
			archivedao.setStatus(Constants.PENDING_REQUEST, archive_id);
			return "/teacher/createArchive/success?faces-redirect=true" + "&amp;edit_flag=" + edit_flag + "&amp;archive_id=" + archive_id;
		}
	}
	
	
	
	
	
	
	
	
	
	
	public void handleFileCalender(FileUploadEvent event) {
		UploadedFile file = event.getFile();
	 	String fileName = file.getFileName();
	 	String fileType = null;
	 	Pattern p = Pattern.compile(".*(\\.|\\/)(xls|doc|pdf)$");
	 	Matcher m = p.matcher(fileName);
	 	if(m.matches()) {
	 		fileType = m.group(2);
	 	}
	 	archive_calender = file.getContents();
 		archive_calender_type = fileType;
 		
 		boolean flag = archivedao.setCalender(archive_calender, archive_calender_type, archive_id);
 		FacesMessage message;
 		if(flag) {
	 		message = new FacesMessage("上传成功" , fileName + " 已上传");	
	 	} else {
	 		message = new FacesMessage(FacesMessage.SEVERITY_WARN, "上传失败" , fileName + " 未上传");
	 	}
        
        getContext().addMessage(null, message);
    }

	public void handleFileBlank_paper_a(FileUploadEvent event) {
		UploadedFile file = event.getFile();
		String fileName = file.getFileName();
	 	String fileType = null;
	 	Pattern p = Pattern.compile(".*(\\.|\\/)(xls|doc|pdf)$");
	 	Matcher m = p.matcher(fileName);
	 	if(m.matches()) {
	 		fileType = m.group(2);
	 	}
	 	archive_blank_paper_a = file.getContents();
 		archive_blank_paper_a_type = fileType;
 		
 		boolean flag = archivedao.setBlank_paper_a(archive_blank_paper_a, archive_blank_paper_a_type, archive_id);
 		FacesMessage message;
 		if(flag) {
	 		message = new FacesMessage("上传成功" , fileName + " 已上传");	
	 	} else {
	 		message = new FacesMessage(FacesMessage.SEVERITY_WARN, "上传失败" , fileName + " 未上传");
	 	}
        
        getContext().addMessage(null, message);
	}
	
	public void handleFileBlank_paper_b(FileUploadEvent event) {
		UploadedFile file = event.getFile();
		String fileName = file.getFileName();
	 	String fileType = null;
	 	Pattern p = Pattern.compile(".*(\\.|\\/)(xls|doc|pdf)$");
	 	Matcher m = p.matcher(fileName);
	 	if(m.matches()) {
	 		fileType = m.group(2);
	 	}
	 	archive_blank_paper_b = file.getContents();
 		archive_blank_paper_b_type = fileType;
	 	
 		boolean flag = archivedao.setBlank_paper_b(archive_blank_paper_b, 
 				archive_blank_paper_b_type, archive_id);
 		FacesMessage message;
 		if(flag) {
	 		message = new FacesMessage("上传成功" , fileName + " 已上传");	
	 	} else {
	 		message = new FacesMessage(FacesMessage.SEVERITY_WARN, "上传失败" , fileName + " 未上传");
	 	}
        
        getContext().addMessage(null, message);
	}
	
	public void handleFileBlank_paper_aa(FileUploadEvent event) {
		UploadedFile file = event.getFile();
		String fileName = file.getFileName();
	 	String fileType = null;
	 	Pattern p = Pattern.compile(".*(\\.|\\/)(xls|doc|pdf)$");
	 	Matcher m = p.matcher(fileName);
	 	if(m.matches()) {
	 		fileType = m.group(2);
	 	}
	 	archive_blank_paper_aa = file.getContents();
 		archive_blank_paper_aa_type = fileType;
	 	
 		boolean flag = archivedao.setBlank_paper_aa(archive_blank_paper_aa, 
 				archive_blank_paper_aa_type, archive_id);
 		FacesMessage message;
 		if(flag) {
	 		message = new FacesMessage("上传成功" , fileName + " 已上传");	
	 	} else {
	 		message = new FacesMessage(FacesMessage.SEVERITY_WARN, "上传失败" , fileName + " 未上传");
	 	}
        
        getContext().addMessage(null, message);
	}
	
	public void handleFileBlank_paper_bb(FileUploadEvent event) {
		UploadedFile file = event.getFile();
		String fileName = file.getFileName();
	 	String fileType = null;
	 	Pattern p = Pattern.compile(".*(\\.|\\/)(xls|doc|pdf)$");
	 	Matcher m = p.matcher(fileName);
	 	if(m.matches()) {
	 		fileType = m.group(2);
	 	}
	 	archive_blank_paper_bb = file.getContents();
 		archive_blank_paper_bb_type = fileType;
	 	
 		boolean flag = archivedao.setBlank_paper_bb(archive_blank_paper_bb, 
 				archive_blank_paper_bb_type, archive_id);
 		FacesMessage message;
 		if(flag) {
	 		message = new FacesMessage("上传成功" , fileName + " 已上传");	
	 	} else {
	 		message = new FacesMessage(FacesMessage.SEVERITY_WARN, "上传失败" , fileName + " 未上传");
	 	}
        
        getContext().addMessage(null, message);
	}
	
	public void handleFileAnalysis(FileUploadEvent event) {
		UploadedFile file = event.getFile();
		String fileName = file.getFileName();
	 	String fileType = null;
	 	Pattern p = Pattern.compile(".*(\\.|\\/)(xls|doc|pdf)$");
	 	Matcher m = p.matcher(fileName);
	 	if(m.matches()) {
	 		fileType = m.group(2);
	 	}
	 	archive_analysis = file.getContents();
 		archive_analysis_type = fileType;
	 	
 		boolean flag = archivedao.setAnalysis(archive_analysis, 
 				archive_analysis_type, archive_id);
 		FacesMessage message;
 		if(flag) {
	 		message = new FacesMessage("上传成功" , fileName + " 已上传");	
	 	} else {
	 		message = new FacesMessage(FacesMessage.SEVERITY_WARN, "上传失败" , fileName + " 未上传");
	 	}
        
        getContext().addMessage(null, message);
	}
	
	public void handleFileAnswer(FileUploadEvent event) {
		UploadedFile file = event.getFile();
		String fileName = file.getFileName();
	 	String fileType = null;
	 	Pattern p = Pattern.compile(".*(\\.|\\/)(xls|doc|pdf)$");
	 	Matcher m = p.matcher(fileName);
	 	if(m.matches()) {
	 		fileType = m.group(2);
	 	}
	 	archive_answer = file.getContents();
 		archive_answer_type = fileType;
	 	
 		boolean flag = archivedao.setAnswer(archive_answer, 
 				archive_answer_type, archive_id);
 		FacesMessage message;
 		if(flag) {
	 		message = new FacesMessage("上传成功" , fileName + " 已上传");	
	 	} else {
	 		message = new FacesMessage(FacesMessage.SEVERITY_WARN, "上传失败" , fileName + " 未上传");
	 	}
        
        getContext().addMessage(null, message);
	}
	
	
	public void handleFileTranscript(FileUploadEvent event) {
		Transcript transcript = new Transcript();
		
		UploadedFile file = event.getFile();
		String fileName = file.getFileName();
	 	String fileType = null;
	 	Pattern p = Pattern.compile(".*(\\.|\\/)(xls|doc|pdf)$");
	 	Matcher m = p.matcher(fileName);
	 	if(m.matches()) {
	 		fileType = m.group(2);
	 	}
	 	
	 	transcript.setTranscript_file(file.getContents());;
 		transcript.setTranscript_type(fileType);
	 	transcriptList.add(transcript);
	 	
 		 FacesMessage message = new FacesMessage("上传成功" , fileName + "已上传");
        getContext().addMessage(null, message);
	}
	
	public void handleFileRegular_grade(FileUploadEvent event) {
		RegularGrade rg = new RegularGrade();
		
		UploadedFile file = event.getFile();
		String fileName = file.getFileName();
	 	String fileType = null;
	 	Pattern p = Pattern.compile(".*(\\.|\\/)(xls|doc|pdf)$");
	 	Matcher m = p.matcher(fileName);
	 	if(m.matches()) {
	 		fileType = m.group(2);
	 	}
	 	
	 	rg.setRg_file(file.getContents());;
 		rg.setRg_type(fileType);
	 	rgList.add(rg);
	 	
 		FacesMessage message = new FacesMessage("上传成功" , fileName + "已上传");
        getContext().addMessage(null, message);
	}
	
	public void resetTranscript(ActionEvent actionEvent) {
		transcriptList = new ArrayList<Transcript>();
		transcriptdao.deleteTranscriptById(archive_id);
		getContext().addMessage(null, new FacesMessage("提示", "成绩单已清空"));	
		
	}
	
	public void resetRegular_grade(ActionEvent actionEvent) {
		rgList = new ArrayList<RegularGrade>();
		regularGradedao.deleteRegularGradeByArchive_id(archive_id);
		getContext().addMessage(null, new FacesMessage("提示" ,"平时成绩报告单已清空"));	
		
	}
	
}
