package backing.teacher.createArchive;

import javax.faces.event.ComponentSystemEvent;

import pojo.Archive;
import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import backing.BaseBacking;

public class SuccessBacking extends BaseBacking{

	private DAOFactory dao  = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private int archive_id;

	public int getArchive_id() {
		return archive_id;
	}

	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}
	
	public void init(ComponentSystemEvent event) {
		Archive archive = archivedao.getArchiveById(archive_id);
		Archive currentArchive = (Archive) evaluateEl("#{archive}", Archive.class);
		currentArchive.setArchive_id(archive.getArchive_id());
		currentArchive.setArchive_year(archive.getArchive_year());
		currentArchive.setArchive_semester(archive.getArchive_semester());
		currentArchive.setArchive_course_id_id(archive.getArchive_course_id_id());
		currentArchive.setArchive_course_num(archive.getArchive_course_num());
		currentArchive.setArchive_course_name(archive.getArchive_course_name());
		currentArchive.setArchive_teacher(archive.getArchive_teacher());
	}
}
