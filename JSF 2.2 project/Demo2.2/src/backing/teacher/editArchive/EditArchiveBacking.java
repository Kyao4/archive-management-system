package backing.teacher.editArchive;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import model.Constants;
import pojo.Archive;
import pojo.Course;
import dao.DAOFactory;
import dao.MySQLDAOFactory;
import dao.archive.ArchiveDAO;
import dao.course.CourseDAO;
import dao.teacher.TeacherDAO;
import backing.BaseBacking;

public class EditArchiveBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = MySQLDAOFactory.getDAOFactory(MySQLDAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private CourseDAO coursedao = dao.getCourseDAO();
	private TeacherDAO teacherdao = dao.getTeacherDAO();
	private int archive_id;
	private List<String> yearList = new ArrayList<String>();
	public int getArchive_id() {
		return archive_id;
	}



	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}
	
	public List<String> getYearList() {
		return yearList;
	}

	public void setYearList(List<String> yearList) {
		this.yearList = yearList;
	}
	



	public void init(ActionEvent event) {
		Archive currentArchive = (Archive)evaluateEl("#{archive}", Archive.class);
		Archive archive = archivedao.getArchiveById(archive_id);
		currentArchive.setArchive_year(archive.getArchive_year());
		currentArchive.setArchive_semester(archive.getArchive_semester());
		currentArchive.setArchive_course_id_id(archive.getArchive_course_id_id());
		currentArchive.setArchive_course_num(archive.getArchive_course_num());
		currentArchive.setArchive_course_name(archive.getArchive_course_name());
		currentArchive.setArchive_teacher(archive.getArchive_teacher());
		currentArchive.setArchive_class(archive.getArchive_class());
		currentArchive.setArchive_stu_num(archive.getArchive_stu_num());
		currentArchive.setArchive_actual_stu_num(archive.getArchive_actual_stu_num());
		currentArchive.setArchive_exam_type(archive.getArchive_exam_type());
		currentArchive.setArchive_type(archive.getArchive_type());
		currentArchive.setArchive_storage_type(archive.getArchive_storage_type());
		
		String note = archive.getArchive_note();
		if(note != null && !note.equals("")) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "修改意见", note));	
		} 
		
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		
		int currentYear = Integer.parseInt(df.format(new Date()));
		for(int i = -3; i < 4; i++) {
			String currentArchive_year = String.valueOf((currentYear + i - 1)) + "-" + String.valueOf((currentYear + i)); 
			yearList.add(currentArchive_year);
		}
		
		archive.setArchive_year(String.valueOf((currentYear - 2)) + "-" + String.valueOf((currentYear - 1)));
		
	}
	
	public void checkExistence(AjaxBehaviorEvent event) {
		Archive archive = (Archive) evaluateEl("#{archive}", Archive.class);
		Course course = new Course();
		course.setCourse_year(archive.getArchive_year());
		course.setCourse_semester(archive.getArchive_semester());
		course.setCourse_id_id(archive.getArchive_course_id_id());
		course.setCourse_num(archive.getArchive_course_num());
		Course currentCourse = coursedao.getCourse(course);
		
		Archive returnArchive = archivedao.getArchivebyYearSemesterCourse_id_idCourse_num(archive);
		
		if(currentCourse == null) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "没有找到相应的课程", "请检查学年，学期，课程号，课序号"));
			archive.setArchive_course_name(null);
			archive.setArchive_teacher(null);
			archive.setArchive_class(null);
		} else {
			
			if(returnArchive == null) {
				getContext().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, "提示" , "已查询到课程，还未创建此课程档案。 课程名称：" + currentCourse.getCourse_name() + "\n教师名称：" + currentCourse.getCourse_teacher()));
				archive.setArchive_course_name(currentCourse.getCourse_name());
				archive.setArchive_teacher(currentCourse.getCourse_teacher());
				archive.setArchive_class(currentCourse.getCourse_class());
			
			} else {
				getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "提示", "已经创建这个课程的档案，可以修改。课程名称：" + currentCourse.getCourse_name() + "\n教师名称：" + currentCourse.getCourse_teacher()));
				archive.setArchive_course_name(currentCourse.getCourse_name());
				archive.setArchive_teacher(currentCourse.getCourse_teacher());
				archive.setArchive_class(currentCourse.getCourse_class());
			}
			
		}
		
	}
	
	public String submit() {
		
		Archive currentArchive = (Archive) evaluateEl("#{archive}", Archive.class);

		//根据输入的信息看有没有课程
		Course course = new Course();
		course.setCourse_year(currentArchive.getArchive_year());
		course.setCourse_semester(currentArchive.getArchive_semester());
		course.setCourse_id_id(currentArchive.getArchive_course_id_id());
		course.setCourse_num(currentArchive.getArchive_course_num());
		Course currentCourse = coursedao.getCourse(course);
		if(currentCourse == null) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "没有找到相应的课程", "请检查学年，学期，课程号，课序号"));
			return null;
		}
		
		//设置档案的各个条目
		int archive_thickness = (currentArchive.getArchive_actual_stu_num()/10) + 1;
		currentArchive.setArchive_id(archive_id);
		currentArchive.setArchive_thickness(archive_thickness);
		currentArchive.setArchive_teacher_id(teacherdao.getTeacher_idByName((String) getSession().getAttribute("teacher_name")));
		currentArchive.setArchive_status(Constants.REJECTED_REQUEST);
		
		//从课程表中得到的信息
		currentArchive.setArchive_course_id(currentCourse.getCourse_id());
		currentArchive.setArchive_course_name(currentCourse.getCourse_name());
		currentArchive.setArchive_teacher(currentCourse.getCourse_teacher());
		currentArchive.setArchive_class(currentCourse.getCourse_class());
		currentArchive.setArchive_enroll(currentCourse.getCourse_enroll());
		currentArchive.setArchive_dept(currentCourse.getCourse_dept());
		
		boolean flag = archivedao.updateArchiveByArchive(currentArchive);
		
		if(!flag) {
			getContext().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_WARN, "", "更新档案不成功"));
			return null;
		} 
		
		
		if(currentArchive.getArchive_actual_stu_num() > currentArchive.getArchive_stu_num()) {
			return "/teacher/createArchive/archive_file?faces-redirect=true&amp;student_flag=true&amp;archive_id=" + archive_id + "&amp;edit_flag=true";	
		} else {
			return "/teacher/createArchive/archive_file?faces-redirect=true&amp;student_flag=false&amp;archive_id=" + archive_id + "&amp;edit_flag=true";
		}
		
	}
	
}
