package backing.teacher;

import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;

import dao.DAOFactory;
import dao.teacher.TeacherDAO;
import pojo.Teacher;
import backing.BaseBacking;

public class Teacher_passwordBacking extends BaseBacking {

	
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private TeacherDAO teacherdao = dao.getTeacherDAO();
	
	public void submit(ActionEvent event) {
		Teacher currentTeacher = (Teacher) evaluateEl("#{teacher}", Teacher.class);
		String teacher_id = teacherdao.getTeacher_idByName((String)getSession().getAttribute("teacher_name"));
		String oldPassword = currentTeacher.getTeacher_oldPassword();
		String newPassword = currentTeacher.getTeacher_newPassword();
		String repeatPassword = currentTeacher.getTeacher_repeatPassword();
		
 		Teacher teacher = teacherdao.getTeacherByIdAndPassword(teacher_id, oldPassword);
		if( teacher == null) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "错误", "旧密码不正确"));
			return;
		}
		if(!newPassword.equals(repeatPassword)) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "错误", "两次输入的密码不一致"));
			return;
		}
		boolean flag = teacherdao.updatePasswordById(teacher_id, newPassword);
		if(flag) {
			getContext().addMessage(null, new FacesMessage( "更新密码成功", ""));
		} else {
			getContext().addMessage(null, new FacesMessage( "更新密码不成功", ""));
		}
	}
}
