package backing.teacher;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ComponentSystemEvent;

import model.Constants;
import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import dao.teacher.TeacherDAO;
import pojo.Archive;
import pojo.Archive_info;
import backing.BaseBacking;

public class Teacher_editArchiveBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private TeacherDAO teacherdao = dao.getTeacherDAO();
	private List<Archive_info> archiveList;
	private int archive_id;
	
	
	public int getArchive_id() {
		return archive_id;
	}
	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}
	public List<Archive_info> getArchiveList() {
		return archiveList;
	}
	public void setArchiveList(List<Archive_info> archiveList) {
		this.archiveList = archiveList;
	}
	
	public void init(ComponentSystemEvent event) {
		Archive archive = new Archive();
		archive.setArchive_teacher_id(teacherdao.getTeacher_idByName((String)getSession().getAttribute("teacher_name")));
		archive.setArchive_status(Constants.REJECTED_REQUEST);
		archiveList = archivedao.getArchiveByTeacher_idStatus(archive);
	}
	
	public String edit() {
		return "/teacher/editArchive/editArchive?faces-redirect=true&amp;archive_id=" + archive_id;
	}
}
