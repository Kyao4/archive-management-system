package backing.teacher;

import javax.faces.application.FacesMessage;

import dao.DAOFactory;
import dao.teacher.TeacherDAO;
import pojo.Teacher;
import backing.BaseBacking;

public class Teacher_loginBacking extends BaseBacking {

	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private TeacherDAO teacherDAO = dao.getTeacherDAO();
	
	public String submit() {
		Teacher currentTeacher = (Teacher)evaluateEl("#{teacher}", Teacher.class);
		
		Teacher teacher = teacherDAO.getTeacherByIdAndPassword(currentTeacher.getTeacher_id(), currentTeacher.getTeacher_password());
		
		if(teacher == null) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "�û������������", ""));
			return null;
		}
		getContext().getExternalContext().getSession(true);
		getSession().setAttribute("teacher_name", teacher.getTeacher_name());
		getSession().setAttribute("isAuthenticated", true);
		return "/teacher/teacher_createArchive?faces-redirect=true";
	}
	
	public String logout() {
		getSession().invalidate();
		return "/index?faces-redirect=true";
	}
}
