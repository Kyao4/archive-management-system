package backing.teacher;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;

import pojo.Archive;
import pojo.Archive_info;
import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import dao.teacher.TeacherDAO;
import backing.BaseBacking;

public class Teacher_lookupBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private TeacherDAO teacherdao = dao.getTeacherDAO();
	
	private List<Archive_info> archiveList;
	private List<String> yearList = new ArrayList<String>();
	
	private int archive_id;
	
	public int getArchive_id() {
		return archive_id;
	}


	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}


	public List<Archive_info> getArchiveList() {
		return archiveList;
	}


	public void setArchiveList(List<Archive_info> archiveList) {
		this.archiveList = archiveList;
	}
	public List<String> getYearList() {
		return yearList;
	}

	public void setYearList(List<String> yearList) {
		this.yearList = yearList;
	}



	public void init(ActionEvent event) {
		
		
		Archive currentArchive = (Archive) evaluateEl("#{archive}", Archive.class);
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		int currentYear = Integer.parseInt(df.format(new Date()));
		for(int i = -3; i < 4; i++) {
			String currentArchive_year = String.valueOf((currentYear + i - 1)) + "-" + String.valueOf((currentYear + i)); 
			yearList.add(currentArchive_year);
		}
		
		currentArchive.setArchive_year(String.valueOf((currentYear - 2)) + "-" + String.valueOf((currentYear - 1)));
		currentArchive.setArchive_semester("1");
		String teacherName = (String) getSession().getAttribute("teacher_name");
		Archive archive = new Archive();
		archive.setArchive_semester("");
		archive.setArchive_year(String.valueOf((currentYear - 2)) + "-" + String.valueOf((currentYear - 1)));
		archive.setArchive_course_name("");
		archive.setArchive_course_id_id("");
		archive.setArchive_teacher_id(teacherdao.getTeacher_idByName(teacherName));
		archiveList = archivedao.getArchiveByYearSemesterCourse_id_idCourse_numTeacher_id(archive);
		
		
	
	}
	
	public void search(ActionEvent actionEvent) {
		String teacherName = (String) getSession().getAttribute("teacher_name");
		Archive archive = (Archive) evaluateEl("#{archive}", Archive.class);
		archive.setArchive_teacher_id(teacherdao.getTeacher_idByName(teacherName));
		archiveList = archivedao.getArchiveByYearSemesterCourse_id_idCourse_numTeacher_id(archive);
	}
	
	public String checkoutArchive() {
		return "/teacher/teacher_archiveInfo?faces-redirect=true&amp;archive_id=" + archive_id;
	}
}
