package backing.teacher;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.event.ComponentSystemEvent;

import model.Constants;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import pojo.Analysis;
import pojo.Answer;
import pojo.Blank_paper_a;
import pojo.Blank_paper_aa;
import pojo.Blank_paper_b;
import pojo.Blank_paper_bb;
import pojo.Calender;
import pojo.RegularGrade;
import pojo.Student;
import pojo.Transcript;
import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import dao.regular_grade.RegularGradeDAO;
import dao.student.StudentDAO;
import dao.transcript.TranscriptDAO;
import backing.BaseBacking;

public class Teacher_archiveInfoBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private StudentDAO studentdao = dao.getStudentDAO();
	private TranscriptDAO transcriptdao = dao.getTranscriptDAO();
	private RegularGradeDAO rgdao = dao.getRegularGradeDAO();
	private List<StreamedContent> transcriptList = new ArrayList<StreamedContent>();	
	private List<StreamedContent> regularGradeList = new ArrayList<StreamedContent>();
	private List<Student> stuList = new ArrayList<Student>();
	private int archive_id;

	public int getArchive_id() {
		return archive_id;
	}

	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}
	
	private StreamedContent calender;
	private StreamedContent blank_paper_a;
	private StreamedContent blank_paper_b;
	private StreamedContent blank_paper_aa;
	private StreamedContent blank_paper_bb;
	private StreamedContent analysis;
	private StreamedContent answer;
	
	
	public List<StreamedContent> getTranscriptList() {
		return transcriptList;
	}

	public void setTranscriptList(List<StreamedContent> transcriptList) {
		this.transcriptList = transcriptList;
	}

	public List<StreamedContent> getRegularGradeList() {
		return regularGradeList;
	}

	public void setRegularGradeList(List<StreamedContent> regularGradeList) {
		this.regularGradeList = regularGradeList;
	}
	public List<Student> getStuList() {
		return stuList;
	}

	public void setStuList(List<Student> stuList) {
		this.stuList = stuList;
	}

	public StreamedContent getCalender() {
		return calender;
	}

	public void setCalender(StreamedContent calender) {
		this.calender = calender;
	}
	public StreamedContent getBlank_paper_a() {
		return blank_paper_a;
	}

	public void setBlank_paper_a(StreamedContent blank_paper_a) {
		this.blank_paper_a = blank_paper_a;
	}

	public StreamedContent getBlank_paper_b() {
		return blank_paper_b;
	}

	public void setBlank_paper_b(StreamedContent blank_paper_b) {
		this.blank_paper_b = blank_paper_b;
	}

	public StreamedContent getBlank_paper_aa() {
		return blank_paper_aa;
	}

	public void setBlank_paper_aa(StreamedContent blank_paper_aa) {
		this.blank_paper_aa = blank_paper_aa;
	}

	public StreamedContent getBlank_paper_bb() {
		return blank_paper_bb;
	}

	public void setBlank_paper_bb(StreamedContent blank_paper_bb) {
		this.blank_paper_bb = blank_paper_bb;
	}

	public StreamedContent getAnalysis() {
		return analysis;
	}

	public void setAnalysis(StreamedContent analysis) {
		this.analysis = analysis;
	}

	public StreamedContent getAnswer() {
		return answer;
	}

	public void setAnswer(StreamedContent answer) {
		this.answer = answer;
	}

	public void init(ComponentSystemEvent event) {

		
		List<Transcript> transcriptList = transcriptdao.getTranscriptByArchive_id(archive_id);
		if(transcriptList.size() == 0) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "没有" + Constants.TRANSCRIPT));
		} else {
			for(int i = 0; i < transcriptList.size(); i++) {
				InputStream inputStream = new ByteArrayInputStream(transcriptList.get(i).getTranscript_file());
				String mimeType = "";
				String fileName = Constants.TRANSCRIPT + "_" + (i+1) + "." + transcriptList.get(i).getTranscript_type();
				String encodedFileName = fileName;
				try {
					encodedFileName = URLEncoder.encode(fileName, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				switch(transcriptList.get(i).getTranscript_type()) {
					case"pdf": mimeType = Constants.APP_PDF_TYPE;
					case"doc": mimeType = Constants.APP_DOC_TYPE;
					case"xls": mimeType = Constants.APP_XLS_TYPE;
				}
				this.transcriptList.add(new DefaultStreamedContent(inputStream, mimeType, encodedFileName));	
			}
		}
		
		List<RegularGrade> regularGradeList = rgdao.getRegularGradeByArchive_id(archive_id);
		if(regularGradeList.size() == 0) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "没有" + Constants.REGULARGRADE));
		} else {
			for(int i = 0; i < regularGradeList.size(); i++) {
				InputStream inputStream = new ByteArrayInputStream(regularGradeList.get(i).getRg_file());
				String mimeType = "";
				String fileName = Constants.REGULARGRADE + "_" + (i+1) + "." + regularGradeList.get(i).getRg_type();
				String encodedFileName = fileName;
				try {
					encodedFileName = URLEncoder.encode(fileName, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				switch(regularGradeList.get(i).getRg_type()) {
					case"pdf": mimeType = Constants.APP_PDF_TYPE;
					case"doc": mimeType = Constants.APP_DOC_TYPE;
					case"xls": mimeType = Constants.APP_XLS_TYPE;
				}
				this.regularGradeList.add(new DefaultStreamedContent(inputStream, mimeType, encodedFileName));	
			}
		}
		
		
		
		Calender calender = archivedao.getCalender(archive_id);
		if(calender.getArchive_calender() == null) {
			this.calender = null;
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "没有" + Constants.CALENDER));
		} else {
			InputStream inputStream = new ByteArrayInputStream(calender.getArchive_calender());
			String mimeType = "";
			String fileName = Constants.CALENDER + "." + calender.getArchive_calender_type();
			String encodedFileName = fileName;
			try {
				encodedFileName = URLEncoder.encode(fileName, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			switch(calender.getArchive_calender_type()) {
				case"pdf": mimeType = Constants.APP_PDF_TYPE;
				case"doc": mimeType = Constants.APP_DOC_TYPE;
				case"xls": mimeType = Constants.APP_XLS_TYPE;
			}
			this.calender = new DefaultStreamedContent(inputStream, mimeType, encodedFileName);	
		}
		
		Blank_paper_a blank_paper_a = archivedao.getBlank_paper_a(archive_id);
		if(blank_paper_a.getBlank_paper_a() == null) {
			this.blank_paper_a = null;
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "没有" + Constants.BLANK_PAPER_A));
		} else {
			InputStream inputStream = new ByteArrayInputStream(blank_paper_a.getBlank_paper_a());
			String mimeType = "";
			String fileName = Constants.BLANK_PAPER_A + "." + blank_paper_a.getBlank_paper_a_type();
			String encodedFileName = fileName;
			try {
				encodedFileName = URLEncoder.encode(fileName, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			switch(blank_paper_a.getBlank_paper_a_type()) {
				case"pdf": mimeType = Constants.APP_PDF_TYPE;
				case"doc": mimeType = Constants.APP_DOC_TYPE;
				case"xls": mimeType = Constants.APP_XLS_TYPE;
			}
			this.blank_paper_a = new DefaultStreamedContent(inputStream, mimeType, encodedFileName);	
		}
		
		Blank_paper_b blank_paper_b = archivedao.getBlank_paper_b(archive_id);
		if(blank_paper_b.getBlank_paper_b() == null) {
			this.blank_paper_b = null;
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "没有" + Constants.BLANK_PAPER_B));
		} else {
			InputStream inputStream = new ByteArrayInputStream(blank_paper_b.getBlank_paper_b());
			String mimeType = "";
			String fileName = Constants.BLANK_PAPER_B + "." + blank_paper_b.getBlank_paper_b_type();
			String encodedFileName = fileName;
			try {
				encodedFileName = URLEncoder.encode(fileName, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			switch(blank_paper_b.getBlank_paper_b_type()) {
				case"pdf": mimeType = Constants.APP_PDF_TYPE;
				case"doc": mimeType = Constants.APP_DOC_TYPE;
				case"xls": mimeType = Constants.APP_XLS_TYPE;
			}
			this.blank_paper_b = new DefaultStreamedContent(inputStream, mimeType, encodedFileName);	
		}
		
		Blank_paper_aa blank_paper_aa = archivedao.getBlank_paper_aa(archive_id);
		if(blank_paper_aa.getBlank_paper_aa() == null) {
			this.blank_paper_aa = null;
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "没有" + Constants.BLANK_PAPER_AA));
		} else {
			InputStream inputStream = new ByteArrayInputStream(blank_paper_aa.getBlank_paper_aa());
			String mimeType = "";
			String fileName = Constants.BLANK_PAPER_AA + "." + blank_paper_aa.getBlank_paper_aa_type();
			String encodedFileName = fileName;
			try {
				encodedFileName = URLEncoder.encode(fileName, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			switch(blank_paper_aa.getBlank_paper_aa_type()) {
				case"pdf": mimeType = Constants.APP_PDF_TYPE;
				case"doc": mimeType = Constants.APP_DOC_TYPE;
				case"xls": mimeType = Constants.APP_XLS_TYPE;
			}
			this.blank_paper_aa = new DefaultStreamedContent(inputStream, mimeType, encodedFileName);	
		}
		
		Blank_paper_bb blank_paper_bb = archivedao.getBlank_paper_bb(archive_id);
		if(blank_paper_bb.getBlank_paper_bb() == null) {
			this.blank_paper_bb = null;
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "没有" + Constants.BLANK_PAPER_BB));
		} else {
			InputStream inputStream = new ByteArrayInputStream(blank_paper_bb.getBlank_paper_bb());
			String mimeType = "";
			String fileName = Constants.BLANK_PAPER_BB + "." + blank_paper_bb.getBlank_paper_bb_type();
			String encodedFileName = fileName;
			try {
				encodedFileName = URLEncoder.encode(fileName, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			switch(blank_paper_bb.getBlank_paper_bb_type()) {
				case"pdf": mimeType = Constants.APP_PDF_TYPE;
				case"doc": mimeType = Constants.APP_DOC_TYPE;
				case"xls": mimeType = Constants.APP_XLS_TYPE;
			}
			this.blank_paper_bb = new DefaultStreamedContent(inputStream, mimeType, encodedFileName);	
		}
		
		Analysis analysis = archivedao.getAnalysis(archive_id);
		if(analysis.getArchive_analysis() == null) {
			this.analysis = null;
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "没有" + Constants.ANALYSIS));
		} else {
			InputStream inputStream = new ByteArrayInputStream(analysis.getArchive_analysis());
			String mimeType = "";
			String fileName = Constants.ANALYSIS + "." + analysis.getArchive_analysis_type();
			String encodedFileName = fileName;
			try {
				encodedFileName = URLEncoder.encode(fileName, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			switch(analysis.getArchive_analysis_type()) {
				case"pdf": mimeType = Constants.APP_PDF_TYPE;
				case"doc": mimeType = Constants.APP_DOC_TYPE;
				case"xls": mimeType = Constants.APP_XLS_TYPE;
			}
			this.analysis = new DefaultStreamedContent(inputStream, mimeType, encodedFileName);	
		}
		
		Answer answer = archivedao.getAnswer(archive_id);
		if(answer.getArchive_answer() == null) {
			this.answer = null;
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "没有" + Constants.ANSWER));
		} else {
			InputStream inputStream = new ByteArrayInputStream(answer.getArchive_answer());
			String mimeType = "";
			String fileName = Constants.ANSWER + "." + answer.getArchive_answer_type();
			String encodedFileName = fileName;
			try {
				encodedFileName = URLEncoder.encode(fileName, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			switch(answer.getArchive_answer_type()) {
				case"pdf": mimeType = Constants.APP_PDF_TYPE;
				case"doc": mimeType = Constants.APP_DOC_TYPE;
				case"xls": mimeType = Constants.APP_XLS_TYPE;
			}
			this.answer = new DefaultStreamedContent(inputStream, mimeType, encodedFileName);	
		}
		
		stuList = studentdao.getStudentByArchive_id(archive_id);
		if(stuList.size() == 0) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "提示", "没有未录入学生"));
		}
	}
	
}
