package backing.secretary;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ComponentSystemEvent;

import model.Constants;
import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import pojo.ArchiveCheckInfo;
import backing.BaseBacking;

public class Secretary_checkArchiveBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private List<ArchiveCheckInfo> archiveList;
	private ArchiveCheckInfo selectedArchive;
	public List<ArchiveCheckInfo> getArchiveList() {
		return archiveList;
	}

	public void setArchiveList(List<ArchiveCheckInfo> archiveList) {
		this.archiveList = archiveList;
	}
	
	public ArchiveCheckInfo getSelectedArchive() {
		return selectedArchive;
	}

	public void setSelectedArchive(ArchiveCheckInfo selectedArchive) {
		this.selectedArchive = selectedArchive;
	}

	public void init(ComponentSystemEvent event) {
		archiveList = archivedao.getArchiveByStatus(Constants.PENDING_REQUEST);
	}

	public String checkoutArchive() {
		int id = selectedArchive.getArchive_id();
		return "/secretary/checkArchive/archive_info?faces-redirect=true&amp;archive_id=" + id;
	}
	
}
