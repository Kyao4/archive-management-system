package backing.secretary.statistics;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ComponentSystemEvent;

import dao.DAOFactory;
import dao.student.StudentDAO;
import pojo.Archive;
import pojo.Student_info;
import backing.BaseBacking;

public class StudentBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private StudentDAO studentdao = dao.getStudentDAO();
	private List<Student_info> stuList = new ArrayList<Student_info>();
	//@SuppressWarnings("unused")
	private String fileName;
	private String archive_year;
	private String archive_semester;
	
	
	
	public String getArchive_year() {
		return archive_year;
	}



	public void setArchive_year(String archive_year) {
		this.archive_year = archive_year;
	}

	public String getArchive_semester() {
		return archive_semester;
	}



	public void setArchive_semester(String archive_semester) {
		this.archive_semester = archive_semester;
	}



	public String getFileName() {
		String fileName = (String) evaluateEl("#{studentBundle['stu_title']}", String.class);
		try {
			fileName = URLEncoder.encode(fileName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return fileName;
	}



	public List<Student_info> getStuList() {
		return stuList;
	}

	public void setStuList(List<Student_info> stuList) {
		this.stuList = stuList;
	}

	public void init(ComponentSystemEvent event) {
		Archive archive = new Archive();
		archive.setArchive_year(archive_year);
		archive.setArchive_semester(archive_semester);
		stuList = studentdao.getALlStudentGradeInfoByArchive_yearArchive_semester(archive);
	}
}
