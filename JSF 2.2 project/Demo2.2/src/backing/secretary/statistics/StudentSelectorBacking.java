package backing.secretary.statistics;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ComponentSystemEvent;

import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import pojo.Archive;
import backing.BaseBacking;

public class StudentSelectorBacking extends BaseBacking implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private List<Archive> archiveList;
	private Archive currentArchive;
	
	public List<Archive> getArchiveList() {
		return archiveList;
	}

	public void setArchiveList(List<Archive> archiveList) {
		this.archiveList = archiveList;
	}

	public Archive getCurrentArchive() {
		return currentArchive;
	}

	public void setCurrentArchive(Archive currentArchive) {
		this.currentArchive = currentArchive;
	}

	public void init(ComponentSystemEvent event) {
		archiveList = archivedao.getArchive_yearArchive_semester();
	}
	
	public String checkout() {
		return "/secretary/statistics/student?faces-redirect=true&amp;archive_year=" + currentArchive.getArchive_year() +
				"&amp;archive_semester=" + currentArchive.getArchive_semester();
	}
}
