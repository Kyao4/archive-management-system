package backing.secretary.archiveStore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import model.Constants;

import org.primefaces.event.RowEditEvent;

import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import dao.archive_box.Archive_boxDAO;
import dao.archive_inner_box.Archive_inner_boxDAO;
import dao.course.CourseDAO;
import pojo.Archive;
import pojo.Archive_box;
import pojo.Archive_inner_box;
import backing.BaseBacking;

public class Ab_manageBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private Archive_boxDAO archive_boxdao = dao.getArchive_boxDAO();
	private CourseDAO coursedao = dao.getCourseDAO();
	private Archive_inner_boxDAO archive_inner_boxDAO = dao.getArchive_inner_boxDAO();
	private List<Archive_box> boxList;
	private List<String> yearList;
	private int progress;
	
	
	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public List<Archive_box> getBoxList() {
		return boxList;
	}

	public void setBoxList(List<Archive_box> boxList) {
		this.boxList = boxList;
	}
	
	public List<String> getYearList() {
		return yearList;
	}

	public void setYearList(List<String> yearList) {
		this.yearList = yearList;
	}

	public void init(ComponentSystemEvent event) {
		boxList = archive_boxdao.getAllBox();
		yearList = coursedao.getYearList();
	}
	
	public void onRowEdit(RowEditEvent event) {
		Archive_box  currentBox = (Archive_box)event.getObject();
		boolean flag = archive_boxdao.updateArchive_box(currentBox);
		if(flag){
			getContext().addMessage(null, new FacesMessage("提示", "档案柜已更新,档案柜号：" + currentBox.getAb_num()));	
		} else {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN ,"警告", "档案柜未更新,档案柜号：" + currentBox.getAb_num()));
		}
	}
	
	public void onRowCancel(RowEditEvent event) {
		
	}
	
	
	public void storeArchive() {
		for(int boxNum = 1; boxNum <= 24; boxNum++) {
			for(int innerBoxNum = 1; innerBoxNum <= 18; innerBoxNum++) {
				
			}
				
		}
	}
	
	
	/**创建档案柜格子*/
	public void process(ActionEvent event) {
		progress = 0;
		List<Archive_box> boxList = archive_boxdao.getAllBox();
		
		archive_inner_boxDAO.deleteAllArchive_inner_box();
		for(int i = 0; i < boxList.size(); i++) {
			progress = (int)(i/(double)boxList.size()/(double)2 *100) ;
			Archive_box box = boxList.get(i);
			String ab_year = box.getAb_year();
			String ab_semester = box.getAb_semester();
			int aib_ab_id = box.getAb_id();
			String aib_side = box.getAb_side();
			int aib_length = 90;
			if(ab_semester == null||ab_semester.equals("")||ab_year == null||ab_year.equals("")) {
				
					for(int j = 1; j <= 18; j++) {
						int aib_num = j;
						String aib_dept = "";
						switch(j) {
							case 1:;
							case 2:;
							case 3:;
							case 4:aib_dept = Constants.DEPT_COMPUTER;break;
							case 5:;
							case 6:;
							case 7:aib_dept = Constants.DEPT_BASIC;break;
							case 8:;
							case 9:;
							case 10:aib_dept = Constants.DEPT_ELECTRONIC;break;
							case 11:;
							case 12:;
							case 13:aib_dept = Constants.DEPT_COMMUNICATION;break;
							case 14:;
							case 15:;
							case 16:aib_dept = Constants.DEPT_NETWORK;break;
							case 17:;
							case 18:aib_dept = Constants.DEPT_COMPUTATIONCENTER;break;
						}
						Archive_inner_box inner_box = new Archive_inner_box();
						inner_box.setAib_num(aib_num);
						inner_box.setAib_length(aib_length);
						inner_box.setAib_dept(aib_dept);
						inner_box.setAib_side(aib_side);
						inner_box.setAib_ab_id(aib_ab_id);
						boolean flag = archive_inner_boxDAO.createInnerBox(inner_box);
						if(flag) {
							System.out.println("成功创建档案柜格子" + aib_side);
						}
					}
				 
				
				continue;//如果学期为空，就执行上面的代码然后跳过这个档案柜
			}
			switch(ab_semester) {
				case "1":{
					for(int j = 1; j <= 18; j++) {
						int aib_num = j;
						String aib_dept = "";
						switch(j) {
							case 1:;
							case 2:;
							case 3:;
							case 4:aib_dept = Constants.DEPT_COMPUTER;break;
							case 5:;
							case 6:;
							case 7:aib_dept = Constants.DEPT_BASIC;break;
							case 8:;
							case 9:;
							case 10:aib_dept = Constants.DEPT_ELECTRONIC;break;
							case 11:;
							case 12:;
							case 13:aib_dept = Constants.DEPT_COMMUNICATION;break;
							case 14:;
							case 15:;
							case 16:aib_dept = Constants.DEPT_NETWORK;break;
							case 17:;
							case 18:aib_dept = Constants.DEPT_COMPUTATIONCENTER;break;
						}
						Archive_inner_box inner_box = new Archive_inner_box();
						inner_box.setAib_num(aib_num);
						inner_box.setAib_length(aib_length);
						inner_box.setAib_dept(aib_dept);
						inner_box.setAib_side(aib_side);
						inner_box.setAib_ab_id(aib_ab_id);
						boolean flag = archive_inner_boxDAO.createInnerBox(inner_box);
						if(flag) {
							System.out.println("成功创建档案柜格子" + aib_side);
						}
					}
				} break; 
				case "2":{
					for(int j = 1; j <= 18; j++) {
						int aib_num = j;
						String aib_dept = "";
						switch(j) {
							case 1:;
							case 2:;
							case 3:;
							case 4:;
							case 5:;
							case 6:aib_dept = Constants.DEPT_GRADDESIGN;break;
							case 7:;
							case 8:;
							case 9:aib_dept = Constants.DEPT_COMPUTER;break;
							case 10:;
							case 11:;
							case 12:aib_dept = Constants.DEPT_BASIC;break;
							case 13:;
							case 14:;
							case 15:aib_dept = Constants.DEPT_ELECTRONIC;break;
							case 16:aib_dept = Constants.DEPT_COMMUNICATION;break;
							case 17:aib_dept = Constants.DEPT_NETWORK;break;
							case 18:aib_dept = Constants.DEPT_COMPUTATIONCENTER;break;
						}
						Archive_inner_box inner_box = new Archive_inner_box();
						inner_box.setAib_num(aib_num);
						inner_box.setAib_length(aib_length);
						inner_box.setAib_dept(aib_dept);
						inner_box.setAib_side(aib_side);
						inner_box.setAib_ab_id(aib_ab_id);
						boolean flag = archive_inner_boxDAO.createInnerBox(inner_box);
						if(flag) {
							System.out.println("成功创建档案柜格子" + aib_side);
						}
					}
				} break;
				default:break; 
			}
		}
		//TODO 将毕业设计信息放入档案柜
		
		
		for(int i = 0; i < boxList.size(); i++) {
			progress = (int)(i/(double)boxList.size()/(double)2 *100) + 50;
			Archive_box box = boxList.get(i);
			Archive archive = new Archive();
			String ab_year = box.getAb_year();
			String ab_semester = box.getAb_semester();
			String ab_enroll = box.getAb_enroll();
			String aib_dept = "";
			if(ab_semester == null||ab_semester.equals("")||ab_year == null||ab_year.equals("")) {
				continue;
			}
			archive.setArchive_year(ab_year);
			archive.setArchive_semester(ab_semester);
			archive.setArchive_enroll(ab_enroll);
			
			switch(ab_semester) {
				case "1":{
					List<String> deptList = new ArrayList<String>();
					deptList.add(Constants.DEPT_COMPUTER);
					deptList.add(Constants.DEPT_BASIC);
					deptList.add(Constants.DEPT_ELECTRONIC);
					deptList.add(Constants.DEPT_COMMUNICATION);
					deptList.add(Constants.DEPT_NETWORK);
					deptList.add(Constants.DEPT_COMPUTATIONCENTER);
					
					for(int deptNum = 0; deptNum < deptList.size(); deptNum++) {
						aib_dept = deptList.get(deptNum);
						archive.setArchive_dept(aib_dept);
						List<Integer> archive_idList = archivedao.getArchive_idListByArchive_yearArchive_semesterArchive_enrollArchive_deptLendoutstatus(archive);
						List<Integer> aib_idList = archive_inner_boxDAO.getAib_idByAb_yearAb_semesterAb_enrollAib_dept(ab_year, ab_semester, ab_enroll, aib_dept);
						for(int aibNum = 0; aibNum < aib_idList.size(); aibNum++) {
							boolean flag = true;//能不能存进去
							int aib_id = aib_idList.get(aibNum);
							while(archive_idList.size() > 0) {
								int archive_id = archive_idList.get(0);
								if(!archivedao.storeArchive(archive_id, aib_id)) {
									System.out.println("存储档案不成功,档案号： " + archive_idList.get(0));
									flag = false;
									break;
								} else {
									archive_idList.remove(0);
									System.out.println("存储档案成功,档案号： " + archive_idList.get(0));
								}
							}
							if(!flag) {//不能存进去转下一个格子
								continue;
							} else {
								break;
							}
						}
					}
					
				} break;
				
				case "2":{
					archive.setArchive_dept(aib_dept);
					List<String> deptList = new ArrayList<String>();
					deptList.add(Constants.DEPT_GRADDESIGN);
					deptList.add(Constants.DEPT_COMPUTER);
					deptList.add(Constants.DEPT_BASIC);
					deptList.add(Constants.DEPT_ELECTRONIC);
					deptList.add(Constants.DEPT_COMMUNICATION);
					deptList.add(Constants.DEPT_NETWORK);
					deptList.add(Constants.DEPT_COMPUTATIONCENTER);
					
					for(int deptNum = 0; deptNum < deptList.size(); deptNum++) {
						aib_dept = deptList.get(deptNum);
						archive.setArchive_dept(aib_dept);
						List<Integer> archive_idList = archivedao.getArchive_idListByArchive_yearArchive_semesterArchive_enrollArchive_deptLendoutstatus(archive);
						List<Integer> aib_idList = archive_inner_boxDAO.getAib_idByAb_yearAb_semesterAb_enrollAib_dept(ab_year, ab_semester, ab_enroll, aib_dept);
						for(int aibNum = 0; aibNum < aib_idList.size(); aibNum++) {
							boolean flag = true;//能不能存进去
							int aib_id = aib_idList.get(aibNum);
							while(archive_idList.size() > 0) {
								int archive_id = archive_idList.get(0);
								if(!archivedao.storeArchive(archive_id, aib_id)) {
									System.out.println("存储档案失败");
									flag = false;
									break;
								} else {
									archive_idList.remove(0);
									System.out.println("存储档案成功");
								}
							}
							
							if(!flag) {//不能存进去转下一个格子
								continue;
							} else {
								break;
							}
						}
					}
					
				} break;
			}
		
			
		}
		progress = 100;
		getContext().addMessage(null, new FacesMessage("数据处理完成"));
	}
}




