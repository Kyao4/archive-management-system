package backing.secretary;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

import model.Constants;

import org.primefaces.context.RequestContext;

import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import dao.archive_box.Archive_boxDAO;
import dao.archive_inner_box.Archive_inner_boxDAO;
import pojo.Archive;
import pojo.Archive_box;
import pojo.Archive_boxRow;
import pojo.Archive_info;
import pojo.Archive_inner_box;
import pojo.Archive_sideBox;
import backing.BaseBacking;

public class Secretary_archiveStoreBacking extends BaseBacking  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private List<Archive_sideBox> sideBoxList = new ArrayList<Archive_sideBox>();
	private Archive_boxDAO archive_boxdao = dao.getArchive_boxDAO();
	private Archive_inner_boxDAO archive_inner_boxdao = dao.getArchive_inner_boxDAO();
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private	List<Archive_info> archiveList;
	private int ab_id;
	private Archive_box currentBox;
	private int aib_id;
	private Archive_inner_box innerBox;
	private int archive_id;
	private String tooltip;
	private int height;
	private int aib_length;
	private String changeLine;
	private String imageStyleCSS;
	private List<String> yearList = new ArrayList<String>();
	private static final String  currentStyle ="margin-right:1px;" +
			"-moz-opacity:0.2;" +
			"-khtml-opacity:0.2;" +
			"Opacity:0.2;" +
			"Filter:alpha(opacity=50);z-index: 999;" +
			"outline:none;";
	private static final String  commonStyle = "margin-right:1px;";
	
	public List<String> getYearList() {
		return yearList;
	}

	public void setYearList(List<String> yearList) {
		this.yearList = yearList;
	}

	public List<Archive_info> getArchiveList() {
		return archiveList;
	}

	public void setArchiveList(List<Archive_info> archiveList) {
		this.archiveList = archiveList;
	}

	public String getImageStyleCSS() {
		return imageStyleCSS;
	}

	public void setImageStyleCSS(String imageStyleCSS) {
		this.imageStyleCSS = imageStyleCSS;
	}

	public String getChangeLine() {
		this.changeLine = "<br />";
		return this.changeLine;
	}

	public int getAib_length() {
		this.aib_length = 90;
		return this.aib_length;
	}
	
	public int getHeight() {
		this.height = 50;
		return this.height;
	}
	
	public int getAb_id() {
		return ab_id;
	}

	public void setAb_id(int ab_id) {
		this.ab_id = ab_id;
	}

	public int getAib_id() {
		return aib_id;
	}

	public void setAib_id(int aib_id) {
		this.aib_id = aib_id;
	}

	public int getArchive_id() {
		return archive_id;
	}

	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}

	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}
	



	public List<Archive_sideBox> getSideBoxList() {
		return sideBoxList;
	}

	public void setSideBoxList(List<Archive_sideBox> sideBoxList) {
		this.sideBoxList = sideBoxList;
	}

	public Archive_box getCurrentBox() {
		return currentBox;
	}

	public void setCurrentBox(Archive_box currentBox) {
		this.currentBox = currentBox;
	}

	public Archive_inner_box getInnerBox() {
		return innerBox;
	}

	public void setInnerBox(Archive_inner_box innerBox) {
		this.innerBox = innerBox;
	}

	public void init(ComponentSystemEvent event) {
//		Archive archive = new Archive();
//		archive.setArchive_semester("");
//		archive.setArchive_year("");
//		archive.setArchive_course_name("");
//		archive.setArchive_course_id_id("");
//		archive.setArchive_teacher("");
//		archive.setArchive_class("");
//		archiveList = archivedao.getArchiveByYearSemesterCourse_id_idCourse_numTeacherClass(archive);
//		
		Archive currentArchive = (Archive) evaluateEl("#{archive}", Archive.class);
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		
		int currentYear = Integer.parseInt(df.format(new Date()));
		for(int i = -3; i < 4; i++) {
			String currentArchive_year = String.valueOf((currentYear + i - 1)) + "-" + String.valueOf((currentYear + i)); 
			yearList.add(currentArchive_year);
		}
		
		currentArchive.setArchive_year(String.valueOf((currentYear - 2)) + "-" + String.valueOf((currentYear - 1)));
		currentArchive.setArchive_semester("2");
		
		
		
		if(sideBoxList.size() == 0) {
		for(int ab_num = 1; ab_num <= 11; ab_num++) {
			Archive_sideBox sideBox = new Archive_sideBox();
			sideBox.setAb_num(ab_num);
			for(int i = 0; i < 2; i++) {
				String side = "";
				switch(i) {
					case 0: side = "左";break;
					case 1: side = "右";break;
				}
				Archive_box currentBox = archive_boxdao.getArchive_boxByAb_numAb_side(ab_num, side);
				String ab_year = currentBox.getAb_year();
				if(ab_year != null) {
					int strLength = ab_year.length(); 
					int counter = 0;
					int strIndex = 0;
					while(counter ++ < strLength) {
						ab_year = ab_year.substring(0, strIndex+1) + "<br />" + ab_year.substring(strIndex + 1);
						strIndex += 7;
					}
					currentBox.setAb_year(ab_year);	
				}
				String ab_enroll = currentBox.getAb_enroll();
				if(ab_enroll != null && !ab_enroll.equals("")) {
					switch(ab_enroll) {
						case "本三": ab_enroll = ab_enroll.substring(0, 1) + "<br />" + ab_enroll.substring(1); break;
						case "本一、本二":{
							int strLength = ab_enroll.length(); 
							int counter = 0;
							int strIndex = 0;
							while(counter ++ < strLength) {
								ab_enroll = ab_enroll.substring(0, strIndex+1) + "<br />" + ab_enroll.substring(strIndex + 1);
								strIndex += 7;
							}
 							break;
						}
					}
				}
				currentBox.setAb_enroll(ab_enroll);
				
				sideBox.getBoxList().add(currentBox);
			}
			sideBoxList.add(sideBox);
			}
		}
	}
	
	
	public void bringupBox(ActionEvent event) {
		Map<String, String> params = getContext().getExternalContext().getRequestParameterMap();
		int ab_id = Integer.parseInt(params.get("ab_id"));
		currentBox = archive_boxdao.getArchive_boxByAb_id(ab_id);
		ab_id = currentBox.getAb_id();
		for(int rowNum = 1; rowNum <= 6; rowNum++) {
			Archive_boxRow row = new Archive_boxRow();
			for(int x = 1; x <= 3; x++) {
				int aib_num = 0;
				switch(x) {
					case 1: aib_num = rowNum; break;
					case 2: aib_num = rowNum + 6; break;
					case 3: aib_num = rowNum + 12; break;
				}
				
				Archive_inner_box innerBox = archive_inner_boxdao.getInnerBoxInfoByAb_idAib_num(ab_id, aib_num); 
				List<Archive> archiveList = archivedao.getArchiveListByAib_id(innerBox.getAib_id());

				for(int i = 0; i < archiveList.size(); i++) {
					archiveList.get(i).setArchive_style(commonStyle);
				}
				for(int archiveNum = 0; archiveNum < archiveList.size(); archiveNum++) {
					Archive currentArchive = archiveList.get(archiveNum);
					if(currentArchive.getArchive_dept().equals(Constants.DEPT_GRADDESIGN)) {
						currentArchive.setArchive_tooltip("学号：" + currentArchive.getArchive_course_id_id() +  
								"<br />" + "姓名：" + currentArchive.getArchive_course_name());
					} else {
						currentArchive.setArchive_tooltip("课程号：" + currentArchive.getArchive_course_id_id() +  "<br />" +
								" 课序号：" + currentArchive.getArchive_course_num() + "<br />" + " 课程名称: " + currentArchive.getArchive_course_name() + "<br />" +
								"教师名称: " + currentArchive.getArchive_teacher() + "<br />" + "部门：" +  currentArchive.getArchive_dept() + "<br />" + "厚度：" + currentArchive.getArchive_thickness());	
					}
					
					innerBox.getArchiveList().add(currentArchive);	
				}
				
				row.getInnerBoxList().add(innerBox);
			}
			currentBox.getRowList().add(row);
		}
	}
	
	
	public void bringupBoxByQuery(ActionEvent event) {
		//先查询出需要高亮的档案
		
		
		Archive currentArchive = (Archive)evaluateEl("#{archive}", Archive.class);
		
		/**To-Do 此处不正确，需要通过本一本二或者本三才能确定是哪个档案柜*/
		int ab_id = archive_boxdao.getAb_idByYearSemesterEnroll(currentArchive.getArchive_year(), 
				currentArchive.getArchive_semester(), currentArchive.getArchive_enroll());
		archiveList = archivedao.getArchiveByYearSemesterCourse_id_idCourse_numTeacherClassEnrollLendoutStatus(currentArchive);//所有需要高亮显示的档案
		
		
		//显示所有档案柜中的档案，点亮高亮的档案
		currentBox = archive_boxdao.getArchive_boxByAb_id(ab_id);
		ab_id = currentBox.getAb_id();

		for(int rowNum = 1; rowNum <= 6; rowNum++) {
			Archive_boxRow row = new Archive_boxRow();
			for(int x = 1; x <= 3; x++) {
				int aib_num = 0;
				switch(x) {
					case 1: aib_num = rowNum; break;
					case 2: aib_num = rowNum + 6; break;
					case 3: aib_num = rowNum + 12; break;
				}
				
				Archive_inner_box innerBox = archive_inner_boxdao.getInnerBoxInfoByAb_idAib_num(ab_id, aib_num); 
				List<Archive> archiveList = archivedao.getArchiveListByAib_id(innerBox.getAib_id());//一个档案柜中所存的档案
				
				for(int archiveNum = 0; archiveNum < archiveList.size(); archiveNum++) {
					Archive archive = archiveList.get(archiveNum);
					archive.setArchive_style(commonStyle);//提前设置号档案的显示效果CSS
					int archiveListIndex = 0;
					while(archiveListIndex < this.archiveList.size()) {
						
						if(archive.getArchive_id() == this.archiveList.get(archiveListIndex).getArchive_id()) {
							archive.setArchive_style(currentStyle);
							
							break;//找到相应的档案就加量并结束
						} else {
							//一旦有档案编号相等的两个档案就让它高亮，否则什么也不做
						}
						archiveListIndex++;
					}
					
					
					
					if(archive.getArchive_dept().equals(Constants.DEPT_GRADDESIGN)) {
						archive.setArchive_tooltip("学号：" + archive.getArchive_course_id_id() +  
								"<br />" + "姓名：" + archive.getArchive_course_name());
					} else {
						archive.setArchive_tooltip("课程号：" + archive.getArchive_course_id_id() +  "<br />" +
								" 课序号：" + archive.getArchive_course_num() + "<br />" + " 课程名称: " + archive.getArchive_course_name() + "<br />" +
								"教师名称: " + archive.getArchive_teacher() + "<br />" + "部门：" +  archive.getArchive_dept() + "<br />" + "厚度：" + archive.getArchive_thickness());	
					}
					
					innerBox.getArchiveList().add(archive);	
				}
				
				row.getInnerBoxList().add(innerBox);
			}
			currentBox.getRowList().add(row);
			
		}
		if(this.archiveList.size() == 0) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "没有找到对应的档案", "档案未通过审核，或没有更新数据"));
		} else {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "已查询到档案", "档案个数： " + this.archiveList.size()));
		}
	}
	
	
	
	public void bringupInnerBox(ActionEvent event) {
		Map<String, String> params = getContext().getExternalContext().getRequestParameterMap();
		int aib_id = Integer.parseInt(params.get("aib_id"));
		innerBox = archive_inner_boxdao.getInnerBoxInfoByAib_id(aib_id);
		List<Archive> archiveList = archivedao.getArchiveListByAib_id(innerBox.getAib_id());
		for(int archiveNum = 0; archiveNum < archiveList.size(); archiveNum++) {
			Archive currentArchive = archiveList.get(archiveNum);
			String archive_course_name = currentArchive.getArchive_course_name();
			int strLen = archive_course_name.length(); //字符数
			int strIndex = 0; //+1为下一个需要插入换行符的位置
			int counter = 0; //已经在其后加了换行符的字符数
			while(counter++ < strLen) {
				archive_course_name = archive_course_name.substring(0, strIndex + 1) + "<br />" + archive_course_name.substring(strIndex + 1); 
				strIndex += 7;
			}
			currentArchive.setArchive_course_name_vertical(archive_course_name);
			if(currentArchive.getArchive_dept().equals(Constants.DEPT_GRADDESIGN)) {
				currentArchive.setArchive_tooltip("学号：" + currentArchive.getArchive_course_id_id() +  
						"<br />" + "姓名：" + currentArchive.getArchive_course_name());
			} else {
				currentArchive.setArchive_tooltip("课程号：" + currentArchive.getArchive_course_id_id() +  "<br />" +
						" 课序号：" + currentArchive.getArchive_course_num() + "<br />" + " 课程名称: " + currentArchive.getArchive_course_name() + "<br />" +
						"教师名称: " + currentArchive.getArchive_teacher() + "<br />" + "部门：" + currentArchive.getArchive_dept() + "<br />" + "厚度：" + currentArchive.getArchive_thickness() );	
			}
			
			innerBox.getArchiveList().add(currentArchive);	
		}
	}
	
	public String redirect() {
		return "/secretary/archiveStore/archive_box?faces-redirect=true&amp;ab_id=" + ab_id;
	}
	
	public void openDialog(ActionEvent event) {
		Map<String, String> params = getContext().getExternalContext().getRequestParameterMap();
		int archive_id = Integer.parseInt(params.get("archive_id"));
		Map<String,Object> options = new HashMap<String, Object>();
		options.put("modal", true);
		options.put("draggable", false);
		options.put("resizable", false);
		options.put("contentHeight", 600);
		options.put("contentWidth", 600);
		options.put("includeViewParams", true);
		Map<String,List<String>> viewParams = new HashMap<String,List<String>>();
		List<String> values = new ArrayList<String>();
		values.add(Integer.toString(archive_id));
		viewParams.put("archive_id", values);
		RequestContext.getCurrentInstance().openDialog("/secretary/archiveStore/fullPage_archiveInfo" , options, viewParams);
	}
	
	public String onToggle() {
		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("PF('queryButton').enable()");
		return null;
	}
	


	
}
