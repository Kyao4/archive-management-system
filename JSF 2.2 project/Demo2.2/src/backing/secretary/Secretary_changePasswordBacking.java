package backing.secretary;

import javax.faces.application.FacesMessage;

import dao.DAOFactory;
import dao.secretary.SecretaryDAO;
import pojo.Secretary;
import backing.BaseBacking;

public class Secretary_changePasswordBacking extends BaseBacking {

	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private SecretaryDAO secretarydao = dao.getSecretaryDAO();
	
	public void submit() {
		Secretary currentSecretary = (Secretary) evaluateEl("#{secretary}", Secretary.class);
		String userName = (String) getSession().getAttribute("secretary_name");	
		if(!currentSecretary.getSecretary_newPassword().equals(currentSecretary.getSecretary_repeatPassword())) {
			getContext().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, "错误", "两次输入的密码不一致"));
			return;
		}
		Secretary secretary = secretarydao.getSecretaryByNameAndPassword(userName, currentSecretary.getSecretary_oldPassword());
		if(secretary == null) {
			getContext().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, "错误", "密码不正确"));
			return;
		}
		
		boolean flag = secretarydao.updatePasswordByName(userName, currentSecretary.getSecretary_newPassword());
		if(flag) {
			getContext().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_INFO, "成功", "密码更新成功"));
		} else {
			getContext().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_ERROR, "失败", "密码更新失败"));
		}
	}
	
}
