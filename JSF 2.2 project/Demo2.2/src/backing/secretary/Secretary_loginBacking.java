package backing.secretary;

import javax.faces.application.FacesMessage;

import pojo.Secretary;
import dao.DAOFactory;
import dao.secretary.SecretaryDAO;
import backing.BaseBacking;

public class Secretary_loginBacking extends BaseBacking {

	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private	SecretaryDAO secretaryDAO = dao.getSecretaryDAO();
	
	public String submit() {
		Secretary currentSecretary = (Secretary) evaluateEl("#{secretary}", Secretary.class);
		
		Secretary secretary = secretaryDAO.getSecretaryByNameAndPassword(currentSecretary.getSecretary_name(), currentSecretary.getSecretary_password());
		
		if(secretary == null) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "�û������������", ""));
			return null;
		}
		
		currentSecretary.setSecretary_name(secretary.getSecretary_name());
		currentSecretary.setSecretary_password(secretary.getSecretary_password());
		getContext().getExternalContext().getSession(true);
		getSession().setAttribute("secretary_name", secretary.getSecretary_name());
		getSession().setAttribute("isAuthenticated", true);
		return "/secretary/secretary_checkArchive?faces-redirect=true";
	}
	
	public String logout() {
		getSession().invalidate();
		return "/index?faces-redirect=true";
	}
	
}
