package backing.secretary.checkArchive;

import java.io.Serializable;

import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import backing.BaseBacking;

public class Archive_rejectBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private int archive_id;
	private String note;

	public int getArchive_id() {
		return archive_id;
	}

	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	public String addNote() {
		archivedao.updateNote(note, archive_id);
		return "/secretary/secretary_checkArchive?faces-redirect=true";
	}
	
}
