package backing.secretary.archiveLendout;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.event.ComponentSystemEvent;

import model.Constants;
import pojo.Archive;
import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import backing.BaseBacking;

public class Lendout_infoBacking extends BaseBacking implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private int archive_id;
	private boolean checkout_flag;
	private boolean statistics_flag;
	
	public boolean isCheckout_flag() {
		return checkout_flag;
	}

	public void setCheckout_flag(boolean checkout_flag) {
		this.checkout_flag = checkout_flag;
	}

	public boolean isStatistics_flag() {
		return statistics_flag;
	}

	public void setStatistics_flag(boolean statistics_flag) {
		this.statistics_flag = statistics_flag;
	}

	public int getArchive_id() {
		return archive_id;
	}

	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}
	
	public void init(ComponentSystemEvent event) {
		Archive currentArchive = (Archive)evaluateEl("#{archive}", Archive.class);
		Archive archive = archivedao.getArchiveById(archive_id);
		currentArchive.setArchive_id(archive_id);
		currentArchive.setArchive_year(archive.getArchive_year());
		currentArchive.setArchive_semester(archive.getArchive_semester());
		currentArchive.setArchive_course_id_id(archive.getArchive_course_id_id());
		currentArchive.setArchive_course_name(archive.getArchive_course_name());
		currentArchive.setArchive_teacher(archive.getArchive_teacher());
		currentArchive.setArchive_borrower_name(archive.getArchive_borrower_name());
		currentArchive.setArchive_borrower_dept(archive.getArchive_borrower_dept());
	}
	
	public String lendout() {
		//查看是不是已经借出
		Archive archive = archivedao.checkLendoutStatus(archive_id);
		if(archive.getArchive_borrower_status().equals(Constants.LENDOUT_STR)) {
			getContext().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_WARN, "警告", "系统错误,不能重复借阅"));
			return null;
		}
		
		//借出
		archive = (Archive)evaluateEl("#{archive}", Archive.class);
		archive.setArchive_id(archive_id);
		boolean flag = archivedao.lendoutArchive(archive);
		
		
		if(flag) {
			getContext().addMessage(null , new FacesMessage("提示", "借阅成功"));
			return "/secretary/secretary_archiveLendout";
		} else {
			getContext().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_WARN, "警告", "系统错误"));
			return null;
		}
	}
	
	public String returnArchive() {
		boolean flag = archivedao.retrunArchive(archive_id);
		if(flag ) {
			if(statistics_flag != true) {
				return "/secretary/secretary_archiveLendout";	
			} else {
				return "/secretary/archiveLendout/lendout_statistics";
			}
		} else {
			getContext().addMessage(null , new FacesMessage(FacesMessage.SEVERITY_WARN, "警告", "系统错误"));
			return null;
		}
	}
	
}
