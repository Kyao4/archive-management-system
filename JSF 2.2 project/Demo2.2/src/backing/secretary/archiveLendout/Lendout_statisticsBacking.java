package backing.secretary.archiveLendout;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ComponentSystemEvent;

import model.Constants;
import pojo.Archive;
import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import backing.BaseBacking;

public class Lendout_statisticsBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private int archive_id;
	private List<Archive> archiveList;
	
	public int getArchive_id() {
		return archive_id;
	}
	public void setArchive_id(int archive_id) {
		this.archive_id = archive_id;
	}
	public List<Archive> getArchiveList() {
		return archiveList;
	}
	public void setArchiveList(List<Archive> archiveList) {
		this.archiveList = archiveList;
	}
	
	public void init(ComponentSystemEvent event) {
		archiveList = archivedao.getArchiveListByArchive_borrower_status(Constants.LENDOUT);
	}
	
	public String checkout() {
		return "/secretary/archiveLendout/lendout_info?faces-redirect=true&amp;archive_id=" + archive_id + "&amp;statistics_flag=true&amp;checkout_flag=true";
	}
	
}
