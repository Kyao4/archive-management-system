package backing.secretary;

import java.io.Serializable;
import java.util.List;

import javax.faces.event.ComponentSystemEvent;

import dao.DAOFactory;
import dao.course.CourseDAO;
import pojo.Course;
import backing.BaseBacking;

public class Secretary_statisticsSelectorBacking extends BaseBacking implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private CourseDAO coursedao = dao.getCourseDAO();
	private List<Course> courseList;
	private Course currentCourse;
	public List<Course> getCourseList() {
		return courseList;
	}

	public void setCourseList(List<Course> courseList) {
		this.courseList = courseList;
	}

	public Course getCurrentCourse() {
		return currentCourse;
	}

	public void setCurrentCourse(Course currentCourse) {
		this.currentCourse = currentCourse;
	}
	
	
	public void init(ComponentSystemEvent event) {
		courseList = coursedao.getCourse_yearCourse_semester();
		
	}
	
	public String checkout() {
		return "/secretary/secretary_statistics?faces-redirect=true&amp;course_year=" + currentCourse.getCourse_year() + 
				"&amp;course_semester=" + currentCourse.getCourse_semester();
	}
	

}
