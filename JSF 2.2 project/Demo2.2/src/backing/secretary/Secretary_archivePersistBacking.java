package backing.secretary;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.event.ComponentSystemEvent;

import model.Constants;
import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import dao.course.CourseDAO;
import dao.grad_design_stu.GradDesignStuDAO;
import dao.regular_grade.RegularGradeDAO;
import dao.transcript.TranscriptDAO;
import pojo.Archive;
import pojo.Course;
import pojo.Gds_year_major;
import pojo.RegularGrade;
import pojo.Transcript;
import backing.BaseBacking;

public class Secretary_archivePersistBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private CourseDAO coursedao = dao.getCourseDAO();
	private GradDesignStuDAO gdsdao = dao.getGradDesignStuDAO();
	private ArchiveDAO archivedao = dao.getArchiveDAO();
	private TranscriptDAO transcriptdao = dao.getTranscriptDAO();
	private RegularGradeDAO regularGradedao = dao.getRegularGradeDAO();
	private String currentYear;
	private String currentSemester;
	private List<Archive> archiveYearSemesterList = new ArrayList<Archive>();
	private List<Course> courseYearSemesterList = new ArrayList<Course>();
	private List<Gds_year_major> GdsYearList = new ArrayList<Gds_year_major>();
	
	public String getCurrentSemester() {
		return currentSemester;
	}



	public List<Archive> getYearSemesterList() {
		return archiveYearSemesterList;
	}



	public void setCurrentSemester(String currentSemester) {
		this.currentSemester = currentSemester;
	}



	public void setYearSemesterList(List<Archive> yearSemesterList) {
		this.archiveYearSemesterList = yearSemesterList;
	}



	public String getCurrentYear() {
		return currentYear;
	}



	public List<Gds_year_major> getGdsYearList() {
		return GdsYearList;
	}



	public void setGdsYearList(List<Gds_year_major> gdsYearList) {
		GdsYearList = gdsYearList;
	}



	public void setCurrentYear(String currentYear) {
		this.currentYear = currentYear;
	}







	public List<Archive> getArchiveYearSemesterList() {
		return archiveYearSemesterList;
	}



	public List<Course> getCourseYearSemesterList() {
		return courseYearSemesterList;
	}



	public void setArchiveYearSemesterList(List<Archive> archiveYearSemesterList) {
		this.archiveYearSemesterList = archiveYearSemesterList;
	}



	public void setCourseYearSemesterList(List<Course> courseYearSemesterList) {
		this.courseYearSemesterList = courseYearSemesterList;
	}



	public void init(ComponentSystemEvent event) {
		
		archiveYearSemesterList = archivedao.getArchive_yearArchive_semester();
		courseYearSemesterList = coursedao.getCourse_yearCourse_semester();
		GdsYearList = gdsdao.getGds_Year();
//		Archive archive = (Archive)evaluateEl("#{archive}", Archive.class);
//		SimpleDateFormat df = new SimpleDateFormat("yyyy");
//		
//		int currentYear = Integer.parseInt(df.format(new Date()));
//		for(int i = -3; i < 4; i++) {
//			String currentArchive_year = String.valueOf((currentYear + i - 1)) + "-" + String.valueOf((currentYear + i)); 
//			yearList.add(currentArchive_year);
//		}
//		
//		archive.setArchive_year(String.valueOf((currentYear - 2)) + "-" + String.valueOf((currentYear - 1)));
		
		
	}
	
	
//	public static int sChunk = 8192;
	
	public String downloadFile() {
//		Archive archive = (Archive)evaluateEl("#{archive}", Archive.class);
//		String year = archive.getArchive_year();
//		String semester = archive.getArchive_semester();
		String year = this.currentYear;
		String semester = this.currentSemester;

		ZipOutputStream zipout;
		ByteArrayOutputStream out = new ByteArrayOutputStream();//使用bytearray来存储zip文件，以便转换为byte数组
		zipout = new ZipOutputStream(out);
		try {
			//普通文档
			List<Archive> archiveList = archivedao.getArchiveFileByYearSemester(year, semester);
			for(Archive currentArchive : archiveList) {
				ZipEntry entry = null;
				if(currentArchive.getArchive_calender() != null) {
					entry = new ZipEntry(
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "/" +
							
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "_" +
							Constants.CALENDER + "." + 
							currentArchive.getArchive_calender_type());
					zipout.putNextEntry(entry);
					zipout.write(currentArchive.getArchive_calender());
				}
				if(currentArchive.getArchive_blank_paper_a() != null) {
					entry = new ZipEntry(
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "/" +
							
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "_" +
							Constants.BLANK_PAPER_A + "." + 
							currentArchive.getArchive_blank_paper_a_type());
					zipout.putNextEntry(entry);
					zipout.write(currentArchive.getArchive_blank_paper_a());
				}
				if(currentArchive.getArchive_blank_paper_b() != null) {
					entry = new ZipEntry(
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "/" +
							
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "_" +
							Constants.BLANK_PAPER_B + "." + 
							currentArchive.getArchive_blank_paper_b_type());
					zipout.putNextEntry(entry);
					zipout.write(currentArchive.getArchive_blank_paper_b());
				}
				
				if(currentArchive.getArchive_blank_paper_aa() != null) {
					entry = new ZipEntry(
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "/" +
							
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "_" +
							Constants.BLANK_PAPER_AA + "." + 
							currentArchive.getArchive_blank_paper_aa_type());
					zipout.putNextEntry(entry);
					zipout.write(currentArchive.getArchive_blank_paper_aa());
				}
				
				if(currentArchive.getArchive_blank_paper_bb() != null) {
					entry = new ZipEntry(
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "/" +
							
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "_" +
							Constants.BLANK_PAPER_BB + "." + 
							currentArchive.getArchive_blank_paper_bb_type());
					zipout.putNextEntry(entry);
					zipout.write(currentArchive.getArchive_blank_paper_bb());
				}
				
				if(currentArchive.getArchive_analysis() != null) {
					entry = new ZipEntry(
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "/" +
							
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "_" +
							Constants.ANALYSIS + "." + 
							currentArchive.getArchive_analysis_type());
					zipout.putNextEntry(entry);
					zipout.write(currentArchive.getArchive_analysis());
				}
				
				if(currentArchive.getArchive_answer() != null) {
					entry = new ZipEntry(
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "/" +
							
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "_" +
							Constants.ANSWER + "." + 
							currentArchive.getArchive_answer_type());
					zipout.putNextEntry(entry);
					zipout.write(currentArchive.getArchive_answer());
				
				List<RegularGrade> rgList = regularGradedao.getRegularGradeByArchive_id(currentArchive.getArchive_id());
				for(RegularGrade rg : rgList) {
					entry = new ZipEntry(
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "/" +
							
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "_" +
							Constants.REGULARGRADE + "_" +
							rg.getRg_id() + "." +
							rg.getRg_type());
					zipout.putNextEntry(entry);
					zipout.write(rg.getRg_file());
				}
				
				List<Transcript> transcriptList = transcriptdao.getTranscriptByArchive_id(currentArchive.getArchive_id());
				for(Transcript transcript : transcriptList) {
					entry = new ZipEntry(
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "/" +
							
							currentArchive.getArchive_year() + "_" + 
							currentArchive.getArchive_semester() + "_" + 
							currentArchive.getArchive_course_id_id() + "_" +
							currentArchive.getArchive_course_num() + "_" +
							currentArchive.getArchive_course_name() + "_" +
							currentArchive.getArchive_teacher() + "_" +
							Constants.TRANSCRIPT + "_" +
							transcript.getTranscript_id() + "." +
							transcript.getTranscript_type());
					zipout.putNextEntry(entry);
					zipout.write(transcript.getTranscript_file());
				}
				
				
				
				}
			}
			
			
			
//			
//			//平时成绩
//			List<Archive> rgList = archivedao.getRegularGradeByYearSemester(year, semester);
//			
//			
//			
//			
//			for(int i = 0; i < rgList.size(); i++) {
//				Archive currentRg = rgList.get(i);
//					ZipEntry entry = new ZipEntry("平时成绩/" 
//				+ currentRg.getArchive_year() + "_" 
//				+ currentRg.getArchive_semester() + "_" 
//				+ currentRg.getArchive_course_id_id() + "_"
//				+ currentRg.getArchive_course_num() + "_"
//				+ currentRg.getArchive_course_name() + "_"
//				+ currentRg.getArchive_teacher() + "_"
//				+ "平时成绩" + "_"
//				+ i
//				+ "." + currentRg.getRg_type());
//					zipout.putNextEntry(entry);
//					zipout.write(currentRg.getRg_file());
//					
//			}
//			//成绩单
//			List<Archive> transcriptList = archivedao.getTranscriptByYearSemester(year, semester);
//			for(int i = 0; i < transcriptList.size(); i++) {
//				Archive currentTranscript = transcriptList.get(i);
//					ZipEntry entry = new ZipEntry("成绩单/" 
//				+ currentTranscript.getArchive_year() + "_" 
//				+ currentTranscript.getArchive_semester() + "_" 
//				+ currentTranscript.getArchive_course_id_id() + "_"
//				+ currentTranscript.getArchive_course_num() + "_"
//				+ currentTranscript.getArchive_course_name() + "_"
//				+ currentTranscript.getArchive_teacher() + "_"
//				+ "成绩单" + "_"
//				+ i
//				+ "." + currentTranscript.getTranscript_type());
//					zipout.putNextEntry(entry);
//					zipout.write(currentTranscript.getTranscript_file());
//					
//			}
			
			
			
			zipout.close();
		} catch(IOException e) {
			e.printStackTrace();
			System.out.println("couldn't compress file.");
			return null;
		}
		byte[] content = out.toByteArray();
		ExternalContext externalContext = getContext().getExternalContext();
		externalContext.responseReset();
		externalContext.setResponseContentType(Constants.APP_ZIP_TYPE);
//		externalContext.setResponseContentLength(content.length);
		String fileName = this.currentYear + "_" + this.currentSemester + "_" +  "档案备份" + ".zip";
		try {
			fileName = URLEncoder.encode(fileName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" 
		+ fileName + "\"");
		OutputStream output = null;
		try {
		output = externalContext.getResponseOutputStream();
		output.write(content);
		output.flush();
		output.close();
		} catch (IOException ex) {
		 ex.printStackTrace();
		} finally {
		getContext().responseComplete();
		}
		
		
		return null;
	}
	
	
	public void clearArchiveData() {
		boolean archiveFlag = archivedao.deleteByYearSemester(currentYear, currentSemester);
		
		
		
		if(archiveFlag) {
			getContext().addMessage(null, new FacesMessage("清除数据成功",currentYear + "_" + currentSemester));	
		} else {
			getContext().addMessage(null, new FacesMessage("清除数据失败，系统错误",currentYear +  "_" + currentSemester));
		}
 		
	
	}
	
	public void clearCourseData() {
		boolean courseFlag = coursedao.deleteByYearSemester(currentYear, currentSemester);
		if(courseFlag) {
			getContext().addMessage(null, new FacesMessage("清除数据成功",currentYear + "_" + currentSemester));	
		} else {
			getContext().addMessage(null, new FacesMessage("清除数据失败，系统错误",currentYear +  "_" + currentSemester));
		}
	}
	
	public void clearGdsData() {
		boolean gdsFlag = gdsdao.deleteGradDesignByYearSemester(currentYear, "2");
		if(gdsFlag) {
			getContext().addMessage(null, new FacesMessage("清除数据成功",currentYear + "_" + 2));	
		} else {
			getContext().addMessage(null, new FacesMessage("清除数据失败，系统错误",currentYear +  "_" + 2));
		}
	}
	
		
}
