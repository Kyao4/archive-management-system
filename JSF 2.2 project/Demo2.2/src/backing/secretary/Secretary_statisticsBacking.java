package backing.secretary;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ComponentSystemEvent;

import model.Constants;
import pojo.Course;
import pojo.Course_info;
import dao.DAOFactory;
import dao.course.CourseDAO;
import backing.BaseBacking;

public class Secretary_statisticsBacking extends BaseBacking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private CourseDAO coursedao = dao.getCourseDAO();
	private List<Course_info> filteredCourseList =  new ArrayList<Course_info>();
	private List<Course_info> courseList = new ArrayList<Course_info>();
	private List<String> statusList = new ArrayList<String>();
	//@SuppressWarnings("unused")
	private String course_year;
	private String course_semester;
	
	
	public String getCourse_year() {
		return course_year;
	}



	public void setCourse_year(String course_year) {
		this.course_year = course_year;
	}



	public String getCourse_semester() {
		return course_semester;
	}



	public void setCourse_semester(String course_semester) {
		this.course_semester = course_semester;
	}





	public String getFileName() {
		String currentFileName = (String)evaluateEl("#{bundle['application.secretary.secretary_statistics']}", String.class);
		String fileName = currentFileName;
		try {
			fileName = URLEncoder.encode(currentFileName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return fileName;
	}



	public void init(ComponentSystemEvent ComponentSystemEvent) {
		Course course = new Course();
		course.setCourse_year(course_year);
		course.setCourse_semester(course_semester);
		courseList = coursedao.getAllCourseWithStatusByCourse_yearCourse_semster(course);
		filteredCourseList = courseList; 
		statusList.add(Constants.PENDING_REQUEST_STR);
		statusList.add(Constants.APPROVED_REQUEST_STR);
		statusList.add(Constants.REJECTED_REQUEST_STR);
		statusList.add(Constants.UNSUBMITTED_REQUEST_STR);
	}
	
	
	
	public List<Course_info> getFilteredCourseList() {
		return filteredCourseList;
	}



	public void setFilteredCourseList(List<Course_info> filteredCourseList) {
		this.filteredCourseList = filteredCourseList;
	}



	public List<String> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<String> statusList) {
		this.statusList = statusList;
	}

	

	public List<Course_info> getCourseList() {
		return courseList;
	}

	public void setCourseList(List<Course_info> courseList) {
		this.courseList = courseList;
	}




	
}
