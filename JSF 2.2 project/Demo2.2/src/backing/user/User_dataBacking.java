package backing.user;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;

import model.Constants;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import dao.DAOFactory;
import dao.archive.ArchiveDAO;
import dao.course.CourseDAO;
import dao.grad_design_stu.GradDesignStuDAO;
import dao.teacher.TeacherDAO;
import pojo.Archive;
import pojo.Course;
import pojo.Grad_design_stu;
import pojo.Teacher;
import backing.BaseBacking;

public class User_dataBacking extends BaseBacking implements Serializable{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
		private TeacherDAO teacherdao = dao.getTeacherDAO();
		private CourseDAO coursedao = dao.getCourseDAO();
		private GradDesignStuDAO gradDesignStuDAO = dao.getGradDesignStuDAO();
		private ArchiveDAO archiveDAO = dao.getArchiveDAO();
		private InputStream teacherList;
		private InputStream courseList;
		private InputStream gradDesignList;
		private int progress;
		
		public int getProgress() {
			return progress;
		}


		public void setProgress(int progress) {
			this.progress = progress;
		}
		


		public void handleTeacherList(FileUploadEvent event) {
			progress = 0;
			UploadedFile file = event.getFile();
			try {
				teacherList = file.getInputstream();		
				HSSFWorkbook workbook = new HSSFWorkbook(teacherList);//读取文件
				
				System.out.println("sheet数目： " + workbook.getNumberOfSheets());
				
				HSSFSheet sheet = workbook.getSheetAt(0); //选取第一个sheet
				
				//得到总行数
				int rowNum = sheet.getPhysicalNumberOfRows();
				//得到第二行
				HSSFRow row = sheet.getRow(1);

				//从第二行开始循环所有行
				for(int r = 1; r < rowNum; r++) {
					progress = (int) (r/(double)rowNum * 100);
					row = sheet.getRow(r);
					//看有多少列
					int columnNum = row.getPhysicalNumberOfCells();
					
					Teacher teacher = new Teacher();
					
					for(int c = 0; c < columnNum; c++) {
						
						HSSFCell cell = row.getCell(c);
						
						if(cell == null) {
							break;
						} 
						
						//得到列的索引
						int columnIndex = cell.getColumnIndex();
						switch(columnIndex) {
							case 0: {
								String teacher_id = String.valueOf((int)cell.getNumericCellValue());
								System.out.println(teacher_id);	
								teacher.setTeacher_id(teacher_id);
								String str = String.valueOf(teacher_id);
								String teacher_password =  str.substring(str.length()-6, str.length());
								System.out.println(teacher_password);
								teacher.setTeacher_password(teacher_password);
								break;
							}
							
							case 1: {
								System.out.println(cell);
								teacher.setTeacher_name((String)cell.getStringCellValue());
								break;
							}
							
							case 2: {
								System.out.println(cell);
								teacher.setTeacher_dept((String)cell.getStringCellValue());
								break;
							}
							
						}
						
					}
					teacherdao.createTeacher(teacher);
					
				}
				workbook.close();
				
			
			} catch (IOException e) {
				getContext().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_WARN, "读取文件错误", ""));
				e.printStackTrace();
				return;
			} catch (OfficeXmlFileException e) {
				getContext().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_WARN, "上传的文件格式不正确", ""));
				e.printStackTrace();
				return;
			} catch(Exception e) {
				getContext().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_WARN, "处理教师名单不成功", "请核对教师名单格式"));
				e.printStackTrace();
				return;
			}
			getContext().addMessage("msg", new FacesMessage("处理教师名单成功", ""));
			progress = 100;
			
		}
		
		
		public void handleCourseList(FileUploadEvent event) {
			progress = 0;
			UploadedFile file = event.getFile();
			try {
				
				courseList = file.getInputstream();
				//@SuppressWarnings("resource")
				HSSFWorkbook workbook = new HSSFWorkbook(courseList);//读取文件
				System.out.println("sheet数目： " + workbook.getNumberOfSheets());
				
				HSSFSheet sheet = workbook.getSheetAt(0); //选取第一个sheet
				
				int rowNum = sheet.getPhysicalNumberOfRows();
				
				HSSFRow row = sheet.getRow(1);
				String course_dept = null;
				String course_enroll = null;
				String course_year = null;
				int course_semester = 0;
				for(int r = 0; r < rowNum; r++) {
					progress = (int) (r/(double)rowNum * 100);
					
					row = sheet.getRow(r);
					//对标题的操作
					if(r == 0 ){
						String title = row.getCell(0).getStringCellValue();	
						Pattern p = Pattern.compile(".*(本三).*(\\d{4}-\\d{4}).*(春|秋).*|.*(\\d{4}-\\d{4}).*(春|秋).*");
						Matcher m = p.matcher(title);
						if(m.find()) {
							course_enroll = m.group(1);
							if(course_enroll != null) {
								course_enroll="本三";
								course_year = m.group(2);
								if(m.group(3).equals("秋")) {
									course_semester = 1;
								} else {
									course_semester = 2;
								}
							} else {
								course_enroll="本一、本二";
								course_year = m.group(4);
								if(m.group(5).equals("秋")) {
									course_semester = 1;
								} else {
									course_semester = 2;
								}
							}
							
						}
						
						continue;
					}
					
					//对除了标题以外的操作
					
					String firstColumn = row.getCell(0).getStringCellValue();
					//判断是不是部门名
					
					Pattern p = Pattern.compile("^.*(基础部|计算中心|实验中心).*$|^.*(通信|电子|网络|计算机).*$");
					Matcher m = p.matcher(firstColumn);
					
					if(m.find()){
						course_dept = m.group(1);
						if(course_dept == null) {
							course_dept	 = m.group(2) + "教研室";
						} else {
							course_dept = m.group(1);
						}
					}
					
					//看第一列是不是数字 如果是数字则为要读取的数据
					p = Pattern.compile("^[a-zA-Z0-9]*\\*?[a-zA-Z0-9]*$");
					m = p.matcher(firstColumn);
					
					if(!m.find()){
						continue;
					}
					//如果是要读取的一行数据
					Course course = new Course();
					int columnNum = row.getPhysicalNumberOfCells();
					if(columnNum != 6) throw new Exception("文件格式不正确");
					for(int c = 0; c < columnNum; c++) {
						String dataCell = row.getCell(c).getStringCellValue();
						switch(c) {
							case 0: {
								course.setCourse_id_id(dataCell);
								break;
							}
							
							case 1: {
								course.setCourse_num(dataCell);
								break;
							}
							
							case 2: {
								course.setCourse_name(dataCell);
								break;
							}
							
							case 3: {
								course.setCourse_teacher(dataCell);
								course.setCourse_teacher_id(teacherdao.getTeacher_idByName(dataCell));
								break;
							}
						
							case 4: {
								course.setCourse_type(dataCell);
							}
							
							case 5: {
								course.setCourse_class(dataCell);
							}
							
						}
					}
					course.setCourse_enroll(course_enroll);
					course.setCourse_year(course_year);
					course.setCourse_dept(course_dept);
					course.setCourse_semester(String.valueOf(course_semester));
					
					boolean flag = coursedao.createCourse(course);
					if(flag) {
						System.out.println("课程建立成功");
					} 
				}
				
				workbook.close();
			
			} catch (OfficeXmlFileException e) {
				getContext().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_WARN, "处理课程清单不成功，文件格式不正确", ""));
				e.printStackTrace();
				return;
			} catch(Exception e) {
				getContext().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_WARN, "处理课程清单不成功", "请核对文件格式"));
				e.printStackTrace();
				return;
			}
			getContext().addMessage(null, new FacesMessage("处理课程清单成功",""));
			progress = 100;
			
		}
		
		public void handleGradDesignList(FileUploadEvent event) {
			progress = 0;
			UploadedFile file = event.getFile();
			try {
				gradDesignList = file.getInputstream();
				//@SuppressWarnings("resource")
				HSSFWorkbook workBook = new HSSFWorkbook(gradDesignList);//读取文件
				System.out.println("sheet数目： " + workBook.getNumberOfSheets());
				
				HSSFSheet sheet = workBook.getSheetAt(0); //选取第一个sheet
				
				int rowNum = sheet.getPhysicalNumberOfRows();
				
				HSSFRow row = sheet.getRow(2);
				String stu_id = row.getCell(1).getStringCellValue();
				String stu_enroll = "";
				if(stu_id.contains("l")||stu_id.contains("L") ) {
					stu_enroll = "本三";
				} else {
					stu_enroll = "本一、本二";
				}
				
				String stu_class = row.getCell(4).getStringCellValue();
				Pattern p = Pattern.compile("[\u4e00-\u9fa5]*[A-Za-z]*(\\d{2})*");
				Matcher m = p.matcher(stu_class);
				String year1 = "";
				String year2 = "";
				String year3 = "";
				if(m.find()) {
					SimpleDateFormat df = new SimpleDateFormat("yyyy");
					year1 = df.format(new Date()).substring(0,2);
					year2 = m.group(1);
					int year3Int = Integer.parseInt(year2);
					year3Int += 4;
					year2 = Integer.toString(year3Int);
					if(year3Int <= 10) {
						year3 = "0" + Integer.toString(--year3Int);
					} else {
						year3 = Integer.toString(--year3Int);	
					}
					
				}
				String gds_year =  year1 + year3 + "-" + year1 + year2;
				System.out.println(gds_year);
				
				
				for(int r = 2; r < rowNum-3; r++ ) {
					progress = (int)(r/(double)rowNum * 100);
					row = sheet.getRow(r);
					int columnNum = row.getPhysicalNumberOfCells();
					Grad_design_stu stu = new Grad_design_stu();
					Archive archive = new Archive();
					stu.setGds_year(gds_year);
					archive.setArchive_year(gds_year);
					archive.setArchive_enroll(stu_enroll);
					archive.setArchive_semester("2");
					archive.setArchive_status(Constants.APPROVED_REQUEST);
					archive.setArchive_dept(Constants.DEPT_GRADDESIGN);
					archive.setArchive_course_id(1);
					archive.setArchive_thickness(1);
					for(int c = 0; c < columnNum; c++) {
						HSSFCell cell = row.getCell(c);
						switch(c) {
							case 0: stu.setGds_school(cell.getStringCellValue()); break;
							case 1: stu.setGds_id(cell.getStringCellValue()); archive.setArchive_course_id_id(cell.getStringCellValue());break;
							case 2: stu.setGds_name(cell.getStringCellValue()); archive.setArchive_course_name(cell.getStringCellValue()); break;
							case 3: stu.setGds_major(cell.getStringCellValue()); break;
							case 4: stu.setGds_class(cell.getStringCellValue()); break;
							case 5: stu.setGds_topic_name(cell.getStringCellValue()); break;
							case 6: stu.setGds_topic_property(cell.getStringCellValue()); break;
							case 7: stu.setGds_topic_source(cell.getStringCellValue()); break;
							case 8: stu.setGds_topic_difficulty(cell.getStringCellValue()); break;
							case 9: stu.setGds_teacher(cell.getStringCellValue()); break;
							case 10: stu.setGds_teacher_title(cell.getStringCellValue()); break;
							case 11:{
								int cellType = cell.getCellType();
								switch(cellType) {
									case Cell.CELL_TYPE_NUMERIC:stu.setGds_teacher_grade((double)cell.getNumericCellValue()); break;
									case Cell.CELL_TYPE_STRING:{
										if(cell.getStringCellValue().equals("")) {
											stu.setGds_teacher_grade(0);	
										} else {
											stu.setGds_teacher_grade((double)Double.parseDouble(cell.getStringCellValue()));	
										}
										break;
									}
								}
						        break;
							}
							case 12: stu.setGds_review_teacher(row.getCell(c).getStringCellValue()); break;
							case 13: stu.setGds_review_teacher_title(row.getCell(c).getStringCellValue()); break;
							case 14: {
								int cellType = cell.getCellType();
								switch(cellType) {
									case Cell.CELL_TYPE_NUMERIC:stu.setGds_review_grade((double)cell.getNumericCellValue()); break;
									case Cell.CELL_TYPE_STRING:{
										if(cell.getStringCellValue().equals("")) {
											stu.setGds_review_grade(0);	
										} else {
											stu.setGds_review_grade((double)Double.parseDouble(cell.getStringCellValue()));	
										}
										break;
									}
								}
						        break;
							}
							case 15: {
								int cellType = cell.getCellType();
								switch(cellType) {
									case Cell.CELL_TYPE_NUMERIC:stu.setGds_defense_grade((double)cell.getNumericCellValue()); break;
									case Cell.CELL_TYPE_STRING:{
										if(cell.getStringCellValue().equals("")) {
											stu.setGds_defense_grade(0);	
										} else {
											stu.setGds_defense_grade((double)Double.parseDouble(cell.getStringCellValue()));	
										}
										break;
									}
								}
						        break;
							}
							case 16: {
								int cellType = cell.getCellType();
								switch(cellType) {
									case Cell.CELL_TYPE_NUMERIC:stu.setGds_grade((double)cell.getNumericCellValue()); break;
									case Cell.CELL_TYPE_STRING:{
										if(cell.getStringCellValue().equals("")) {
											stu.setGds_grade(0);	
										} else {
											stu.setGds_grade((double)Double.parseDouble(cell.getStringCellValue()));	
										}
										break;
									}
								}
						        break;
							}
						}
					}
					int archive_id = archiveDAO.createArchive(archive);
					boolean flag = gradDesignStuDAO.createStu(stu);
					System.out.println(flag);
					System.out.println(archive_id);
				}
				
				workBook.close();
			} catch (OfficeXmlFileException e) {
				getContext().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_WARN, "处理毕业设计清单不成功，文件格式不正确", ""));
				e.printStackTrace();
				return;
			} catch(Exception e) {
				getContext().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_WARN, "处理毕业设计清单不成功", "请核对文件格式"));
				e.printStackTrace();
				return;
			}
			progress = 100;
			
			
			getContext().addMessage(null, new FacesMessage("处理毕设清单成功",""));
		}

}
