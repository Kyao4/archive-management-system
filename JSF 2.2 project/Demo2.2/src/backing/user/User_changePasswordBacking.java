package backing.user;

import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;

import com.itextpdf.text.pdf.PdfBoolean;
import com.itextpdf.text.pdf.PdfNumber;

import dao.DAOFactory;
import dao.user.UserDAO;
import pojo.User;
import backing.BaseBacking;

public class User_changePasswordBacking extends BaseBacking {
	
	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private UserDAO userDAO = dao.getUserDAO();
	PdfBoolean flag = PdfBoolean.PDFTRUE;
	PdfNumber num = new PdfNumber("100");
	public void submit(ActionEvent evnet) {
		User currentUser = (User) evaluateEl("#{user}", User.class);
		String user_name = (String) evaluateEl("#{user_name}", String.class);
		
		User user = userDAO.getUserByNameAndPassword(user_name, currentUser.getUser_oldPassword());
		
		if(!currentUser.getUser_newPassword().equals(currentUser.getUser_repeatPassword())) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "两次输入的密码不一致", ""));
			return;
		}
		
		if(user == null) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "旧密码不正确", ""));
			return;
		}

		boolean flag = userDAO.updatePasswordByName(user_name, currentUser.getUser_newPassword());
		
		if(!flag) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "系统错误" ,""));
			return;
		}
		
		getContext().addMessage(null, new FacesMessage("密码修改成功", ""));
		return;
	
	}
}
