package backing.user;



import javax.faces.application.FacesMessage;

import pojo.User;
import dao.DAOFactory;
import dao.user.UserDAO;
import backing.BaseBacking;

public class User_loginBacking extends BaseBacking {

	private DAOFactory dao = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
	private UserDAO userdao = dao.getUserDAO();
	
	
	public String submit() {
		User currentUser = (User)evaluateEl("#{user}", User.class);
		
		User user = userdao.getUserByNameAndPassword(currentUser.getUser_name(), currentUser.getUser_password());
		
		if(user == null) {
			getContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "�û������������", ""));
			return null;
		}
		
		currentUser.setUser_id(user.getUser_id());
		currentUser.setUser_name(user.getUser_name());
		currentUser.setUser_password(user.getUser_password());
		getContext().getExternalContext().getSession(true);
		getSession().setAttribute("user_name", user.getUser_name());
		getSession().setAttribute("isAuthenticated", true);
		return "/user/user_data?faces-redirect=true";
		 
	}
	
	
	public String logout() {
		getSession().invalidate();
		return "/index?faces-redirect=true";
	}
}
