package backing;

import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class BaseBacking {
	protected FacesContext getContext() {
		return FacesContext.getCurrentInstance();
	}
	
	protected Map<String,Object> getRequestMap() {
		return getContext().getExternalContext().getRequestMap();
	}
	
	protected HttpSession getSession() {
		return (HttpSession) getContext().getExternalContext().getSession(false);
	}
	
	//@SuppressWarnings("unchecked")
	protected Object evaluateEl(String elExpression, Class beanClazz) {
		return getContext().getApplication().evaluateExpressionGet(getContext(), elExpression, beanClazz);
	}
	
}
