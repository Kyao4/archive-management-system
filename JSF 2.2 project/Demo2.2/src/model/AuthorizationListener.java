package model;

import java.util.regex.Pattern;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

public class AuthorizationListener implements PhaseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Override
	public void afterPhase(PhaseEvent event) {
		FacesContext context = event.getFacesContext();
		String currentPage = context.getViewRoot().getViewId();
		boolean isLoginPage = Pattern.matches(".*index.xhtml|.*login.xhtml", currentPage);
		
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		Object isAuthenticated = session.getAttribute("isAuthenticated");
		
		if (!isLoginPage && isAuthenticated == null) {
			NavigationHandler navigationHandler = context.getApplication().getNavigationHandler();
			navigationHandler.handleNavigation(context, null, "/index");
		}
	}

	//@Override
	public void beforePhase(PhaseEvent arg0) {
		// TODO Auto-generated method stub

	}

	//@Override
	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}

}
