package model;

public interface Constants {
	public static int UNSUBMITTED_REQUEST = 0;
	public static int PENDING_REQUEST = 1;
	public static int REJECTED_REQUEST = 2;
	public static int APPROVED_REQUEST = 3;
	
	public static String UNSUBMITTED_REQUEST_STR = "未提交";
	public static String PENDING_REQUEST_STR = "等待审核";
	public static String REJECTED_REQUEST_STR = "审核未通过";
	public static String APPROVED_REQUEST_STR = "审核已通过";
	
	public static String APP_PDF_TYPE = "application/pdf";
	public static String APP_XLS_TYPE = "application/vnd.ms-excel";
	public static String APP_DOC_TYPE = "application/msword";
	public static String APP_ZIP_TYPE = "application/zip";
	
	
	public static String CALENDER = "教学日历";
	public static String TRANSCRIPT = "成绩单";
	public static String REGULARGRADE = "平时成绩记分册";
	public static String BLANK_PAPER_A = "空白试卷A";
	public static String BLANK_PAPER_B = "空白试卷B";
	public static String BLANK_PAPER_AA = "空白试卷A’";
	public static String BLANK_PAPER_BB = "空白试卷B’";
	public static String ANALYSIS = "考试分析报告单";
	public static String ANSWER = "标准答案";
	
	public static String DEPT_GRADDESIGN = "毕业设计";
	public static String DEPT_BASIC = "基础部";
	public static String DEPT_COMPUTER = "计算机教研室";
	public static String DEPT_ELECTRONIC = "电子教研室";
	public static String DEPT_COMMUNICATION = "通信教研室";
	public static String DEPT_NETWORK = "网络教研室";
	public static String DEPT_COMPUTATIONCENTER = "计算中心";
	
	public static int LENDOUT = 1;
	public static int NOTLENDOUT = 0;
	
	public static String LENDOUT_STR = "已借出";
	public static String NOTLENDOUT_STR = "未借出";
	
	public static String TOPIC_PROPERTY = "课题性质";
	public static String TOPIC_SOURCE = "课题来源";
	public static String TOPIC_DIFFICULTY = "课题难易程度";
	public static String GRADE_DISTRIBUTION = "成绩分布";
}

