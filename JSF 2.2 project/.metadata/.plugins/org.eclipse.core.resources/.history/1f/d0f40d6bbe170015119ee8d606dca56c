package dao.grad_design_stu;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.MySQLDAOFactory;
import pojo.Gds_year_major;
import pojo.GradDesignStatistics;
import pojo.Grad_design_stu;

public class MySQLGradDesignStuDAO implements GradDesignStuDAO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//@Override
	public boolean createStu(Grad_design_stu stu) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "INSERT INTO hust_grad_design_stu VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, stu.getGds_year());
			stmt.setString(2, stu.getGds_school());
			stmt.setString(3, stu.getGds_id());
			stmt.setString(4, stu.getGds_name());
			stmt.setString(5, stu.getGds_major());
			stmt.setString(6, stu.getGds_class());
			stmt.setString(7, stu.getGds_topic_name());
			stmt.setString(8, stu.getGds_topic_property());
			stmt.setString(9, stu.getGds_topic_source());
			stmt.setString(10, stu.getGds_topic_difficulty());
			stmt.setString(11, stu.getGds_teacher());
			stmt.setString(12, stu.getGds_teacher_title());
			stmt.setDouble(13, stu.getGds_teacher_grade());
			stmt.setString(14, stu.getGds_review_teacher());
			stmt.setString(15, stu.getGds_review_teacher_title());
			stmt.setDouble(16, stu.getGds_review_grade());
			stmt.setDouble(17, stu.getGds_defense_grade());
			stmt.setDouble(18, stu.getGds_grade());
			if (stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return flag;
	}

	//@Override
	public List<Gds_year_major> getGds_year_marjorList() {
		List<Gds_year_major> gdsList = new ArrayList<Gds_year_major>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "SELECT DISTINCT gds_year, gds_major FROM hust_grad_design_stu ORDER BY gds_year DESC";
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Gds_year_major gds = new Gds_year_major();
				gds.setGds_year(rs.getString(1));
				gds.setGds_major(rs.getString(2));
				gdsList.add(gds);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return gdsList;
	}

	//@Override
	public GradDesignStatistics getGradDesignStatisticsByYearMajor(
			String gds_year, String gds_major) {
		GradDesignStatistics gdsInfo = new GradDesignStatistics();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "SELECT " + 
				"gds_school, " + 
				"topic_property AS stu_num, " + 
				"topic_property_a, " + 
				"ROUND(topic_property_a/topic_property*100,2) AS topic_property_a_percent, " +  
				"topic_property_b, " +  
				"ROUND(topic_property_b/topic_property*100,2) AS topic_property_b_percent, " +
				"topic_property_c,  " + 
				"ROUND(topic_property_c/topic_property*100,2) AS topic_property_c_percent, " +
				"topic_property_d,  " + 
				"ROUND(topic_property_d/topic_property*100,2) AS topic_property_d_percent, " +
				"topic_property_e, " + 
				"ROUND(topic_property_e/topic_property*100,2) AS topic_property_e_percent, " +
				"topic_source_a, " + 
				"ROUND(topic_source_a/topic_source*100,2) AS topic_source_a_percent, " +
				"topic_source_b, " + 
				"ROUND(topic_source_b/topic_source*100,2) AS topic_source_b_percent, " +
				"topic_source_c, " + 
				"ROUND(topic_source_c/topic_source*100,2) AS topic_source_c_percent, " +
				"topic_source_d, " + 
				"ROUND(topic_source_d/topic_source*100,2) AS topic_source_d_percent, " +
				"topic_source_e, " + 
				"ROUND(topic_source_e/topic_source*100,2) AS topic_source_e_percent, " +
				"topic_difficulty_a, " + 
				"ROUND(topic_difficulty_a/topic_difficulty*100,2) AS topic_difficulty_a_percent, " +
				"topic_difficulty_b, " + 
				"ROUND(topic_difficulty_b/topic_difficulty*100,2) AS topic_difficulty_b_percent, " +
				"topic_difficulty_c, " + 
				"ROUND(topic_difficulty_c/topic_difficulty*100,2) AS topic_difficulty_c_percent, " +
				"topic_difficulty_d, " + 
				"ROUND(topic_difficulty_d/topic_difficulty*100,2) AS topic_difficulty_d_percent, " +
				"topic_difficulty_e, " + 
				"ROUND(topic_difficulty_e/topic_difficulty*100,2) AS topic_difficulty_e_percent, " +
				"grade_a, " + 
				"ROUND(grade_a/grade*100,2) as grade_a_percent, " +
				"grade_b, " + 
				"ROUND(grade_b/grade*100,2) as grade_b_percent, " +
				"grade_c, " + 
				"ROUND(grade_c/grade*100,2) as grade_c_percent, " +
				"grade_d, " + 
				"ROUND(grade_d/grade*100,2) as grade_d_percent, " +
				"grade_e, " + 
				"ROUND(grade_e/grade*100,2) as grade_e_percent " +
				"FROM " + 
				"hust_grad_design_stu, " +
				"( " + 
				"SELECT COUNT(gds_topic_property) as topic_property " +
				"FROM hust_grad_design_stu  " + 
				"WHERE gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_property_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_property) as topic_property_a " +
				"FROM hust_grad_design_stu " +  
				"WHERE gds_topic_property = 'A' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_property_a_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_property) as topic_property_b " +
				"FROM hust_grad_design_stu " +  
				"WHERE gds_topic_property = 'B' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_property_b_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_property) as topic_property_c " +
				"FROM hust_grad_design_stu " +  
				"WHERE gds_topic_property = 'C' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_property_c_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_property) as topic_property_d " +
				"FROM hust_grad_design_stu " +  
				"WHERE gds_topic_property = 'D' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_property_d_table , " + 
				"( " + 
				"SELECT COUNT(gds_topic_property) as topic_property_e " +
				"FROM hust_grad_design_stu " +  
				"WHERE gds_topic_property = 'E' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_property_e_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_source) as topic_source " +  
				"FROM hust_grad_design_stu  " + 
				"WHERE gds_major = ? " + 
				"AND gds_year = ? " + 
				") gds_topic_source_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_source) as topic_source_a " +  
				"FROM hust_grad_design_stu " + 
				"WHERE gds_topic_source = 'A' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") gds_topic_source_a_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_source) as topic_source_b " +  
				"FROM hust_grad_design_stu " + 
				"WHERE gds_topic_source = 'B' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") gds_topic_source_b_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_source) as topic_source_c " +  
				"FROM hust_grad_design_stu " + 
				"WHERE gds_topic_source = 'C' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") gds_topic_source_c_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_source) as topic_source_d " +  
				"FROM hust_grad_design_stu " + 
				"WHERE gds_topic_source = 'D' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") gds_topic_source_d_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_source) as topic_source_e " +  
				"FROM hust_grad_design_stu " + 
				"WHERE gds_topic_source = 'E' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") gds_topic_source_e_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_difficulty) AS topic_difficulty " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_difficulty_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_difficulty) AS topic_difficulty_a " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_topic_difficulty = 'A' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_difficulty_a_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_difficulty) AS topic_difficulty_b " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_topic_difficulty = 'B' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_difficulty_b_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_difficulty) AS topic_difficulty_c " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_topic_difficulty = 'C' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_difficulty_c_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_difficulty) AS topic_difficulty_d " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_topic_difficulty = 'D' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_difficulty_d_table, " + 
				"( " + 
				"SELECT COUNT(gds_topic_difficulty) AS topic_difficulty_e " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_topic_difficulty = 'E' " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") topic_difficulty_e_table, " + 
				"( " + 
				"SELECT COUNT(gds_grade) AS grade " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_major = ? " + 
				"AND gds_year = ? " + 
				") grade_table, " + 
				"( " + 
				"SELECT COUNT(gds_grade) AS grade_a " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_grade >= 90 " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") grade_a_table, " + 
				"( " + 
				"SELECT COUNT(gds_grade) AS grade_b " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_grade >= 80 and gds_grade < 90 " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") grade_b_table, " + 
				"( " + 
				"SELECT COUNT(gds_grade) AS grade_c " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_grade >= 70 and gds_grade < 80 " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") grade_c_table, " + 
				"( " + 
				"SELECT COUNT(gds_grade) AS grade_d " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_grade >= 60 and gds_grade < 70 " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") grade_d_table, " + 
				"( " + 
				"SELECT COUNT(gds_grade) AS grade_e " + 
				"FROM hust_grad_design_stu " + 
				"WHERE gds_grade < 60 " + 
				"AND gds_major = ? " + 
				"AND gds_year = ? " + 
				") grade_e_table " + 
				"LIMIT 0, 1 "; 
				stmt = conn.prepareStatement(sql);
				for(int i = 1; i <= 48; i++) {
					if(i % 2 == 1) {
						stmt.setString(i, gds_major);	
					} else {
						stmt.setString(i, gds_year);
					}
				}
				ResultSet rs = stmt.executeQuery();
				while(rs.next()) {
					gdsInfo.setGds_school(rs.getString(1));
					gdsInfo.setStu_num(rs.getInt(2));
					gdsInfo.setTopic_property_a(rs.getInt(3));
					gdsInfo.setTopic_property_a_percent(rs.getDouble(4));
					gdsInfo.setTopic_property_b(rs.getInt(5));
					gdsInfo.setTopic_property_b_percent(rs.getDouble(6));
					gdsInfo.setTopic_property_c(rs.getInt(7));
					gdsInfo.setTopic_property_c_percent(rs.getDouble(8));
					gdsInfo.setTopic_property_d(rs.getInt(9));
					gdsInfo.setTopic_property_d_percent(rs.getDouble(10));
					gdsInfo.setTopic_property_e(rs.getInt(11));
					gdsInfo.setTopic_property_e_percent(rs.getDouble(12));
					gdsInfo.setTopic_source_a(rs.getInt(13));
					gdsInfo.setTopic_source_a_percent(rs.getDouble(14));
					gdsInfo.setTopic_source_b(rs.getInt(15));
					gdsInfo.setTopic_source_b_percent(rs.getDouble(16));
					gdsInfo.setTopic_source_c(rs.getInt(17));
					gdsInfo.setTopic_source_c_percent(rs.getDouble(18));
					gdsInfo.setTopic_source_d(rs.getInt(19));
					gdsInfo.setTopic_source_d_percent(rs.getDouble(20));
					gdsInfo.setTopic_source_e(rs.getInt(21));
					gdsInfo.setTopic_source_e_percent(rs.getDouble(22));
					gdsInfo.setTopic_difficulty_a(rs.getInt(23));
					gdsInfo.setTopic_difficulty_a_percent(rs.getDouble(24));
					gdsInfo.setTopic_difficulty_b(rs.getInt(25));
					gdsInfo.setTopic_difficulty_b_percent(rs.getDouble(26));
					gdsInfo.setTopic_difficulty_c(rs.getInt(27));
					gdsInfo.setTopic_difficulty_c_percent(rs.getDouble(28));
					gdsInfo.setTopic_difficulty_d(rs.getInt(29));
					gdsInfo.setTopic_difficulty_d_percent(rs.getDouble(30));
					gdsInfo.setTopic_difficulty_e(rs.getInt(31));
					gdsInfo.setTopic_difficulty_e_percent(rs.getDouble(32));
					gdsInfo.setGrade_a(rs.getInt(33));
					gdsInfo.setGrade_a_percent(rs.getDouble(34));
					gdsInfo.setGrade_b(rs.getInt(35));
					gdsInfo.setGrade_b_percent(rs.getDouble(36));
					gdsInfo.setGrade_c(rs.getInt(37));
					gdsInfo.setGrade_c_percent(rs.getDouble(38));
					gdsInfo.setGrade_d(rs.getInt(39));
					gdsInfo.setGrade_d_percent(rs.getDouble(40));
					gdsInfo.setGrade_e(rs.getInt(41));
					gdsInfo.setGrade_e_percent(rs.getDouble(42));
				}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return gdsInfo;
	}

	//@Override
	public boolean deleteGradDesignByYearSemester(String year, String semester) {
		if(semester.equals('1')) {
			return true;
		}
		boolean flag = false;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "DELETE FROM hust_grad_design_stu WHERE gds_year = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, year);
			if(stmt.executeUpdate() > 0) {
				flag = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
//@Override
	public List<Gds_year_major> getGds_Year() {
		List<Gds_year_major> yearList = new ArrayList<Gds_year_major>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "SELECT gds_year FROM hust_grad_design_stu GROUP BY gds_year";
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Gds_year_major year = new Gds_year_major();
				year.setGds_year(rs.getString(1));
				yearList.add(year);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			MySQLDAOFactory.closeStatement(stmt);
			MySQLDAOFactory.closeConnection(conn);
		}
		return yearList;
	}

	@Override
	public List<String> getAllMajor() {
		List<String> majorList = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "SELECT gds_major FROM hust_grad_design_stu GROUP BY gds_major";
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				majorList.add(rs.getString(1));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return majorList;
	}

	@Override
	public List<String> getAllYear() {
		List<String> yearList = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = MySQLDAOFactory.getConnection();
			String sql = "SELECT gds_year FROM hust_grad_design_stu GROUP BY gds_year";
			stmt = conn.prepareStatement(sql);
			
		
		}
		return null;
	}
}
