/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50051
Source Host           : 127.0.0.1:3306
Source Database       : hustarchive

Target Server Type    : MYSQL
Target Server Version : 50051
File Encoding         : 65001

Date: 2015-06-17 16:02:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for hust_archive
-- ----------------------------
DROP TABLE IF EXISTS `hust_archive`;
CREATE TABLE `hust_archive` (
  `archive_id` int(11) NOT NULL auto_increment,
  `archive_year` varchar(50) default NULL,
  `archive_semester` varchar(50) default NULL,
  `archive_course_name` varchar(255) default NULL,
  `archive_course_id_id` varchar(50) default NULL,
  `archive_course_num` varchar(50) default NULL,
  `archive_teacher` varchar(50) default NULL,
  `archive_class` varchar(255) default NULL,
  `archive_dept` varchar(255) default NULL,
  `archive_enroll` varchar(255) default NULL,
  `archive_thickness` int(11) default NULL,
  `archive_status` int(11) default NULL,
  `archive_type` varchar(255) default NULL,
  `archive_storage_type` varchar(255) default NULL,
  `archive_exam_type` varchar(255) default NULL,
  `archive_stu_num` int(11) default NULL,
  `archive_actual_stu_num` int(11) default NULL,
  `archive_calender` longblob,
  `archive_calender_type` varchar(255) default NULL,
  `archive_blank_paper_a` longblob,
  `archive_blank_paper_a_type` varchar(255) default NULL,
  `archive_blank_paper_b` longblob,
  `archive_blank_paper_b_type` varchar(255) default NULL,
  `archive_blank_paper_aa` longblob,
  `archive_blank_paper_aa_type` varchar(255) default NULL,
  `archive_blank_paper_bb` longblob,
  `archive_blank_paper_bb_type` varchar(255) default NULL,
  `archive_analysis` longblob,
  `archive_analysis_type` varchar(255) default NULL,
  `archive_answer` longblob,
  `archive_answer_type` varchar(255) default NULL,
  `archive_teacher_id` varchar(255) default NULL,
  `archive_ab_id` int(11) default NULL,
  `archive_aib_id` int(11) default NULL,
  `archive_course_id` int(11) default NULL,
  `archive_borrower_name` varchar(255) default NULL,
  `archive_borrower_dept` varchar(255) default NULL,
  `archive_borrower_status` int(11) default NULL,
  `archive_note` varchar(255) default NULL,
  PRIMARY KEY  (`archive_id`),
  UNIQUE KEY `archive_year` (`archive_year`,`archive_semester`,`archive_course_id_id`,`archive_course_num`,`archive_teacher`),
  KEY `archive_teacher_id` (`archive_teacher_id`),
  KEY `hust_archive_ibfk_2` (`archive_ab_id`),
  KEY `archive_aib_id` (`archive_aib_id`),
  KEY `archive_course_id` (`archive_course_id`),
  CONSTRAINT `hust_archive_ibfk_1` FOREIGN KEY (`archive_teacher_id`) REFERENCES `hust_teacher` (`teacher_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `hust_archive_ibfk_2` FOREIGN KEY (`archive_ab_id`) REFERENCES `hust_archive_box` (`ab_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `hust_archive_ibfk_3` FOREIGN KEY (`archive_aib_id`) REFERENCES `hust_archive_inner_box` (`aib_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `hust_archive_ibfk_4` FOREIGN KEY (`archive_course_id`) REFERENCES `hust_course` (`course_id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4069 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hust_archive_box
-- ----------------------------
DROP TABLE IF EXISTS `hust_archive_box`;
CREATE TABLE `hust_archive_box` (
  `ab_id` int(11) NOT NULL auto_increment,
  `ab_num` int(11) default NULL,
  `ab_side` varchar(255) default NULL,
  `ab_year` varchar(255) default NULL,
  `ab_semester` varchar(255) default NULL,
  `ab_enroll` varchar(255) default NULL,
  PRIMARY KEY  (`ab_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hust_archive_inner_box
-- ----------------------------
DROP TABLE IF EXISTS `hust_archive_inner_box`;
CREATE TABLE `hust_archive_inner_box` (
  `aib_id` int(11) NOT NULL auto_increment,
  `aib_num` int(11) default NULL,
  `aib_length` int(11) default NULL,
  `aib_dept` varchar(255) default NULL,
  `aib_side` varchar(255) default NULL,
  `aib_ab_id` int(11) default NULL,
  PRIMARY KEY  (`aib_id`),
  KEY `aib_ab_id` (`aib_ab_id`),
  CONSTRAINT `hust_archive_inner_box_ibfk_1` FOREIGN KEY (`aib_ab_id`) REFERENCES `hust_archive_box` (`ab_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=397 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hust_course
-- ----------------------------
DROP TABLE IF EXISTS `hust_course`;
CREATE TABLE `hust_course` (
  `course_id` int(11) NOT NULL auto_increment,
  `course_id_id` varchar(50) default NULL,
  `course_num` varchar(50) default NULL,
  `course_name` varchar(255) default NULL,
  `course_teacher` varchar(50) default NULL,
  `course_type` varchar(255) default NULL,
  `course_class` varchar(255) default NULL,
  `course_enroll` varchar(255) default NULL,
  `course_dept` varchar(255) default NULL,
  `course_year` varchar(50) default NULL,
  `course_semester` varchar(50) default NULL,
  `course_teacher_id` varchar(255) default NULL,
  PRIMARY KEY  (`course_id`),
  UNIQUE KEY `course_id_id` (`course_id_id`,`course_num`,`course_teacher`,`course_year`,`course_semester`),
  KEY `hust_course_ibfk_1` (`course_teacher_id`),
  CONSTRAINT `hust_course_ibfk_1` FOREIGN KEY (`course_teacher_id`) REFERENCES `hust_teacher` (`teacher_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=366 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hust_grad_design_stu
-- ----------------------------
DROP TABLE IF EXISTS `hust_grad_design_stu`;
CREATE TABLE `hust_grad_design_stu` (
  `gds_year` varchar(255) default NULL,
  `gds_school` varchar(255) default NULL,
  `gds_id` varchar(255) NOT NULL,
  `gds_name` varchar(255) default NULL,
  `gds_major` varchar(255) default NULL,
  `gds_class` varchar(255) default NULL,
  `gds_topic_name` varchar(255) default NULL,
  `gds_topic_property` varchar(255) default NULL,
  `gds_topic_source` varchar(255) default NULL,
  `gds_topic_difficulty` varchar(255) default NULL,
  `gds_teacher` varchar(255) default NULL,
  `gds_teacher_title` varchar(255) default NULL,
  `gds_teacher_grade` double(255,0) default NULL,
  `gds_review_teacher` varchar(255) default NULL,
  `gds_review_teacher_title` varchar(255) default NULL,
  `gds_review_grade` double(255,0) default NULL,
  `gds_defense_grade` double(255,0) default NULL,
  `gds_grade` double(255,0) default NULL,
  PRIMARY KEY  (`gds_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hust_regular_grade
-- ----------------------------
DROP TABLE IF EXISTS `hust_regular_grade`;
CREATE TABLE `hust_regular_grade` (
  `rg_id` int(11) NOT NULL auto_increment,
  `rg_file` longblob,
  `rg_type` varchar(255) default NULL,
  `rg_archive_id` int(11) default NULL,
  PRIMARY KEY  (`rg_id`),
  KEY `rg_archive_id` (`rg_archive_id`),
  CONSTRAINT `hust_regular_grade_ibfk_1` FOREIGN KEY (`rg_archive_id`) REFERENCES `hust_archive` (`archive_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hust_secretary
-- ----------------------------
DROP TABLE IF EXISTS `hust_secretary`;
CREATE TABLE `hust_secretary` (
  `secretary_id` int(11) NOT NULL auto_increment,
  `secretary_name` varchar(255) default NULL,
  `secretary_password` varchar(255) default NULL,
  PRIMARY KEY  (`secretary_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hust_student
-- ----------------------------
DROP TABLE IF EXISTS `hust_student`;
CREATE TABLE `hust_student` (
  `stu_id` varchar(255) NOT NULL,
  `stu_class` varchar(255) default NULL,
  `stu_name` varchar(255) default NULL,
  `stu_grade` double(255,0) default NULL,
  `stu_archive_id` int(11) NOT NULL,
  PRIMARY KEY  (`stu_id`,`stu_archive_id`),
  UNIQUE KEY `stu_id` (`stu_id`,`stu_archive_id`),
  KEY `stu_archive_id` (`stu_archive_id`),
  CONSTRAINT `hust_student_ibfk_1` FOREIGN KEY (`stu_archive_id`) REFERENCES `hust_archive` (`archive_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hust_teacher
-- ----------------------------
DROP TABLE IF EXISTS `hust_teacher`;
CREATE TABLE `hust_teacher` (
  `teacher_id` varchar(255) NOT NULL,
  `teacher_name` varchar(255) default NULL,
  `teacher_dept` varchar(255) default NULL,
  `teacher_password` varchar(255) default NULL,
  PRIMARY KEY  (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hust_transcript
-- ----------------------------
DROP TABLE IF EXISTS `hust_transcript`;
CREATE TABLE `hust_transcript` (
  `transcript_id` int(255) NOT NULL auto_increment,
  `transcript_file` longblob,
  `transcript_type` varchar(255) default NULL,
  `transcript_archive_id` int(255) default NULL,
  PRIMARY KEY  (`transcript_id`),
  KEY `transcript_archive_id` (`transcript_archive_id`),
  CONSTRAINT `hust_transcript_ibfk_1` FOREIGN KEY (`transcript_archive_id`) REFERENCES `hust_archive` (`archive_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for hust_user
-- ----------------------------
DROP TABLE IF EXISTS `hust_user`;
CREATE TABLE `hust_user` (
  `user_id` int(11) NOT NULL auto_increment,
  `user_name` varchar(255) default NULL,
  `user_password` varchar(255) default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Procedure structure for storearchive
-- ----------------------------
DROP PROCEDURE IF EXISTS `storearchive`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `storearchive`( p_aib_id INT, p_archive_id INT, OUT result INT)
BEGIN
DECLARE width int DEFAULT 90;
DECLARE totalthickness INT; 
DECLARE archivethickness INT;
DECLARE p_ab_id INT;
SELECT aib_ab_id INTO p_ab_id FROM hust_archive_inner_box where aib_id = p_aib_id;
SELECT COALESCE(SUM(archive_thickness),0) INTO totalthickness FROM hust_archive where archive_aib_id = p_aib_id;
SELECT archive_thickness INTO archivethickness FROM hust_archive WHERE archive_id = p_archive_id;

IF totalthickness + archivethickness < 90 THEN

UPDATE hust_archive SET archive_aib_id = p_aib_id, archive_ab_id = p_ab_id where archive_id = p_archive_id;
set result = 1;

ELSE

set result = 0;

END IF;

END
;;
DELIMITER ;
