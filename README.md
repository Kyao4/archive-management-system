# README #


### What is this repository for? ###

This a archive manage system I developed for information and technology department of Hebei university of science and technology. 

* Database: MySQL
* language: Java, JavaScript, SQL, HTML, CSS
* Framework: JSF, PrimeFaces
* IDE: MyEclipse 2015

### How do I get set up? ###

Install MySQL, tomcat, Java. import SQL file into your database(use Navicat). Put AS.war to you host. Then run you host with this file as address.

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###

* Name: Kai Yao
* E-mail: kyao4@hawk.iit.edu


### How does it look like? ###

![login.png](https://bitbucket.org/repo/8booXa/images/4071479141-login.png)

This is login page username 100000, password admin.

![create archive page1.png](https://bitbucket.org/repo/8booXa/images/1488787606-create%20archive%20page1.png)

You can create archive here.

![create archive page2 upload file.png](https://bitbucket.org/repo/8booXa/images/2016428474-create%20archive%20page2%20upload%20file.png)

Second page for creating archive, uploading files.

![search for archives.png](https://bitbucket.org/repo/8booXa/images/3462312409-search%20for%20archives.png)

search archives.


There are a lot of other features that are not in this readme, such as modify archives, checking archives, generating reports(pdf, xls, doc), download backup etc.